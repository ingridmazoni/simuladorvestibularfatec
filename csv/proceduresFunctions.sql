
use vestibular

CREATE TABLE `vestibular`.`alternativas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alternativa` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8




DELIMITER //
CREATE PROCEDURE vestibular.populaConteudoProgramatico(valor INT)
BEGIN 
	DECLARE disciplina INT DEFAULT 1;
	DECLARE contador INT DEFAULT 0;
	
	
	
	WHILE contador <= 100 DO
	   SET contador = contador + 1;
    
	    IF disciplina > valor THEN
	       SET disciplina = 1;
	       INSERT INTO `vestibular`.`conteudoprogramatico`	(`programaProva`,`disciplina_id`) VALUES (concat('programaProva', cast(contador as char)) , disciplina);
	     ELSE
	        INSERT INTO `vestibular`.`conteudoprogramatico`	(`programaProva`,`disciplina_id`) VALUES (concat('programaProva', cast(contador as char)) ,disciplina);
	    END IF;
	    
	    SET disciplina = disciplina + 1;
 
	END WHILE;
	
END //
DELIMITER ; 

insert into vestibular.alternativas(alternativa) values ('A'),('B'),('C'),('D'),('E');
CALL vestibular.populaConteudoProgramatico(10);


DELIMITER //
CREATE PROCEDURE vestibular.populaQuestoesProva(ano INT , semestre VARCHAR(100))
BEGIN
	DECLARE contador INT DEFAULT 1;
	DECLARE codConteudoProgramatico int;
	DECLARE contador1 INT DEFAULT 1;
    Declare alternativaCorreta VARCHAR(2);
    declare nomeConteudoProgramatico Varchar(100);
    declare quantDisciplinas int default (select count(*) from vestibular.disciplina);
    declare quantQuestoes int;
  
	WHILE contador <= quantDisciplinas DO
			SET nomeConteudoProgramatico =(select disciplina.nome from disciplina where disciplina.id=contador);
			
        
				IF nomeConteudoProgramatico != 'Multidisciplinar' then 
					set quantQuestoes = 5;
				else
					set quantQuestoes = 9;
				
				end if;
    
				WHILE contador1 <= quantQuestoes DO
					SET alternativaCorreta = (SELECT alternativa FROM alternativas ORDER BY RAND() LIMIT 1);
					SET codConteudoProgramatico = convert( (SELECT id FROM conteudoProgramatico WHERE disciplina_id = contador ORDER BY RAND() LIMIT 1) , unsigned integer);
					INSERT INTO `vestibular`.`questaovestibular` (`alternativaA`, `alternativaB`,`alternativaC`,`alternativaD`,`alternativaE`,`anoVestibular`,`pergunta`,`respostaCorreta`,`semestreVestibular`,`conteudoProgramatico_id`,
                    `imagemAlternativaA`,`imagemAlternativaB`, `imagemAlternativaC`,`imagemAlternativaD`, `imagemAlternativaE`,`imagemPergunta`) 
                    VALUES('alternativaA','alternativaB','alternativaC','alternativaD','alternativaE', ano ,'pergunta', alternativaCorreta,semestre, codConteudoProgramatico,
                    'flower.jpg', 'gata bruxa.jpg','gato preto.jpg', 'Hello-kitty-wallpaper-12.jpg', 'tiger.jpg','city.jpg');
					SET contador1 = contador1 + 1;
				END WHILE;
        
				SET contador1 = 1;
				SET contador = contador + 1;
	END WHILE;
END //
DELIMITER ;

	CALL vestibular.populaQuestoesProva(2050,'Primeiro');


DELIMITER //
CREATE PROCEDURE vestibular.populaQuestionario2(anoInicial INT , anoFinal int)
BEGIN
DECLARE contador INT DEFAULT anoInicial;

	WHILE contador <= anoFinal DO

	CALL populaQuestoesProva(contador,'Primeiro');
	CALL populaQuestoesProva(contador,'Segundo');
    
    SET contador = contador + 1;

	END WHILE;

END //
DELIMITER ;


CALL vestibular.populaQuestionario2(2016,2018);

DELIMITER //
CREATE PROCEDURE vestibular.populaCandidato(ano INT , sexo VARCHAR(100))
BEGIN
DECLARE contador INT DEFAULT 1;

	WHILE contador <= 100 DO

		INSERT INTO `vestibular`.`usuario` (`login`,`perfil`, `senha`)VALUES(concat('candidato',contador) ,'CANDIDATO', concat('candidato',contador));
		INSERT INTO `vestibular`.`candidato` (`anoInscricaoEnem`,	`celular`,`dataNascimento`, `emailPrincipal`, `emailSecundario`, `nome`, `notaEnem`, `pontuacaoAcrescidaAfrodescendente`, `pontuacaoAcrescidaEscolaridadePublica`, `sexo`, `telefone`, `usuario_login`)
			VALUES ( ano,'(11)99999-9999','1989-08-13', concat(concat('candidato',contador),'@gmail.com'), concat(concat('candidato',contador),'@gmail.com'),concat('candidato',contador), 680, 1, 1, sexo, '(11)9999-9999', concat('candidato',contador));
    
		SET contador = contador + 1;
    END WHILE;
   
END //
DELIMITER ;

CALL vestibular.populaCandidato(2012,'MASCULINO');
CALL vestibular.populaCandidato(2012,'FEMININO');


DELIMITER //
CREATE PROCEDURE vestibular.escolhaDeCursoCandidato()
BEGIN
DECLARE contador INT DEFAULT 1;

	WHILE contador <= 100 DO
			
    INSERT INTO `vestibular`.`escolhadecursodocandidato` (`opcaoCandidato`, `candidato_id`,`turma_id`)
	VALUES( 'PRIMEIRA_OPCAO_DE_CURSO', contador ,  (SELECT id FROM vestibular.turma ORDER BY RAND() LIMIT 1));
     INSERT INTO `vestibular`.`escolhadecursodocandidato` (`opcaoCandidato`, `candidato_id`,`turma_id`)
	VALUES( 'SEGUNDA_OPCAO_DE_CURSO', contador ,  (SELECT id FROM vestibular.turma ORDER BY RAND() LIMIT 1));        
            
            SET contador = contador + 1;
    END WHILE;
END //
DELIMITER ;

call vestibular.escolhaDeCursoCandidato()


DELIMITER //
CREATE PROCEDURE vestibular.questoesProva(numeroCandidato int)
BEGIN

DECLARE contador2 INT DEFAULT 1;
declare idQuestaoVestibular int;
declare materia varchar(100);

	
		INSERT INTO `vestibular`.`realizacaoprova` (`id`,`dataRealizacao`, `candidato_id`) VALUES(numeroCandidato,curdate(), numeroCandidato);
    
			WHILE contador2 <= 54 DO
            
				
					IF contador2 <= 9 then 
						set materia='Multidisciplinar';
					end if;  
                     
					IF contador2 >=10 and contador2 <= 14 then 
						set materia='Raciocínio Lógico';
					end if; 
                     
					IF contador2 >=15 and contador2 <= 19 then 
					    set materia='História';	
                    end if; 
                    
                    IF contador2 >=20 and contador2 <= 24 then 
						set materia='Química';			
					end if; 
                    
                    IF contador2 >=25 and contador2 <= 29 then 
							set materia='Inglês';			
					end if; 
                    
					IF contador2 >=30 and contador2 <= 34 then 
							set materia='Matemática';
                   end if;
                   
				   IF contador2 >=35 and contador2 <= 39 then 
							set materia='Física';		
				   end if; 
                   
                   IF contador2 >=40 and contador2 <= 44 then 
						set materia='Geografia';			
					end if; 
                    
                   IF contador2 >=45 and contador2 <= 49 then 
						set materia='Biologia';		
				   end if; 
                    IF contador2 >=50 and contador2 <= 54 then 
						set materia='Português';		
				   end if; 
                 
					
                    set idQuestaoVestibular=(select vestibular.questaovestibular.id from vestibular.questaovestibular inner join vestibular.conteudoprogramatico on vestibular.questaovestibular.conteudoProgramatico_id = vestibular.conteudoprogramatico.id 
						inner join vestibular.disciplina on vestibular.conteudoprogramatico.disciplina_id = vestibular.disciplina.id where vestibular.disciplina.nome = materia order by rand() limit 1);
                 
                 
					INSERT INTO `vestibular`.`questoesprova` (`respostaDoCandidato`, `questaoVestibular_id`, `realizacaoProva_id`,`sequenciaDaQuestaoNaProva`)
					VALUES ((SELECT alternativa FROM vestibular.alternativas ORDER BY RAND() LIMIT 1), idQuestaoVestibular , numeroCandidato ,contador2);
							
					SET contador2 = contador2 + 1;
					
			END WHILE; 
END //
DELIMITER ;

call vestibular.questoesProva(13)


DELIMITER //
CREATE PROCEDURE vestibular.populaQuestionarioRespondido()
BEGIN
DECLARE contador INT DEFAULT 1;

	WHILE contador <= 100 DO
		call vestibular.questoesProva(contador);
        SET contador = contador + 1;
    END WHILE;
END //
DELIMITER ;

call vestibular.populaQuestionarioRespondido()

DELIMITER //
CREATE PROCEDURE vestibular.populaNotaCorte(ano INT , semestre varchar(10))
BEGIN
DECLARE contador INT DEFAULT 1;

	WHILE contador <= 391 DO
			INSERT INTO `vestibular`.`notacorte` (`ano`,`semestre`, `notaCorteConvocados`,`notaCorteSuplentes`, `turma_id`) VALUES(ano, semestre ,0.0,0.0, contador);
            SET contador = contador + 1;
    END WHILE;
END //
DELIMITER ;

CALL vestibular.populaNotaCorte(2016,'Primeiro');

load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/cidades.csv'
into table vestibular.cidades
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/fatec2.csv'
into table vestibular.fatec
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;


INSERT INTO vestibular.eixotecnologico VALUES (1,'Ambiente, Saúde e Segurança ');
INSERT INTO vestibular.eixotecnologico VALUES (2,'Controle e Processos Industriais ');  
INSERT INTO vestibular.eixotecnologico VALUES (3,'Gestão e Negócios ');  
INSERT INTO vestibular.eixotecnologico VALUES (4,'Hospitalidade e Lazer ');  
INSERT INTO vestibular.eixotecnologico VALUES (5,'Informação e Comunicação ');  
INSERT INTO vestibular.eixotecnologico VALUES (6,'Infraestrutura ');
INSERT INTO vestibular.eixotecnologico VALUES (7,'Produção Alimentícia ');
INSERT INTO vestibular.eixotecnologico VALUES (8,'Produção Industrial ');       
INSERT INTO vestibular.eixotecnologico VALUES (9,'Recursos Naturais ');  


load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/curso.csv'
into table vestibular.curso
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

INSERT INTO vestibular.disciplina VALUES (1,'Raciocínio Lógico');
INSERT INTO vestibular.disciplina VALUES (2,'Matemática');
INSERT INTO vestibular.disciplina VALUES (3,'Física');
INSERT INTO vestibular.disciplina VALUES (4,'Química');
INSERT INTO vestibular.disciplina VALUES (5,'Biologia');
INSERT INTO vestibular.disciplina VALUES (6,'Português');
INSERT INTO vestibular.disciplina VALUES (7,'Inglês');
INSERT INTO vestibular.disciplina VALUES (8,'História');
INSERT INTO vestibular.disciplina VALUES (9,'Geografia');
INSERT INTO vestibular.disciplina VALUES (10,'Multidisciplinar');

load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/materiasPesoDois.csv'
into table vestibular.curso_disciplina
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;

load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/conteudoProgramatico.csv'
into table vestibular.conteudoprogramatico
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;


load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/turma.csv'
into table vestibular.turma
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;


load data local infile 'C:/Users/IngridMarion/Desktop/simuladorVestibularFatec/simuladorvestibularfatec/csv/notaCorte.csv'
into table vestibular.notacorte
fields terminated by ';'
enclosed by '"'
lines terminated by '\n'
ignore 1 rows;


select * from vestibular.curso inner join vestibular.curso_disciplina where vestibular.curso.id = vestibular.curso_disciplina.Curso_id;

DELIMITER //
CREATE PROCEDURE vestibular.LoginTeste()
BEGIN
DECLARE contador INT DEFAULT 1;

	WHILE contador <= 100 DO
			INSERT INTO `vestibular`.`usuario` (`login`,`perfil`, `senha`) VALUES(concat('Teste',contador), 'TESTE' ,'123456');
            SET contador = contador + 1;
    END WHILE;
END //
DELIMITER ;

CALL vestibular.LoginTeste();


SET FOREIGN_KEY_CHECKS = 1;

create view vestibular.VisualizarTurma as
select vestibular.turma.id,vestibular.turma.periodo, vestibular.fatec.nome AS Fatec,vestibular.curso.nome AS Curso from vestibular.turma 
inner join vestibular.fatec inner join vestibular.curso
on vestibular.turma.fatec_id = vestibular.fatec.id and  vestibular.turma.Curso_id = vestibular.curso.id;


create view vestibular.escolhaCandidato AS
select vestibular.escolhadecursodocandidato.opcaoCandidato,vestibular.escolhadecursodocandidato.turma_id AS IDTurma,candidato.usuario_login,candidato.nome,candidato.id from vestibular.escolhadecursodocandidato
inner join vestibular.candidato
on escolhadecursodocandidato.candidato_id= candidato.id;

create view vestibular.OpcaoCandidato AS
select vestibular.escolhacandidato.nome,vestibular.escolhacandidato.opcaoCandidato,vestibular.visualizarturma.Curso,vestibular.visualizarturma.Fatec,vestibular.visualizarturma.periodo
from vestibular.escolhacandidato inner join vestibular.visualizarturma on 
vestibular.escolhacandidato.IDTurma=vestibular.visualizarturma.id
where vestibular.escolhacandidato.id in (select vestibular.realizacaoprova.candidato_id from vestibular.realizacaoprova)
order by vestibular.visualizarturma.Fatec,vestibular.visualizarturma.Curso,vestibular.visualizarturma.periodo;


create view vestibular.visualizaNotaCorte as 
select visualizarturma.periodo,visualizarturma.Fatec,visualizarturma.Curso,
vestibular.notacorte.ano, vestibular.notacorte.demanda, vestibular.notacorte.notaCorteConvocados,
vestibular.notacorte.notaCorteSuplentes,vestibular.notacorte.numeroInscritos,vestibular.notacorte.semestre
from vestibular.visualizarturma inner join vestibular.notacorte on 
vestibular.notacorte.turma_id= visualizarturma.id
order by vestibular.notacorte.ano,vestibular.notacorte.semestre,visualizarturma.Curso,visualizarturma.periodo asc;

create view vestibular.visualizaMateriaPesoDoisDeCursos as 
select vestibular.curso.nome as Curso, vestibular.disciplina.nome as Disciplina from vestibular.curso inner join vestibular.curso_disciplina
inner join vestibular.disciplina on 
vestibular.curso.id = vestibular.curso_disciplina.Curso_id and vestibular.disciplina.id = vestibular.curso_disciplina.materiasPesoDois_id
order by vestibular.curso.nome;


