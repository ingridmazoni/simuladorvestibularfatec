use vestibular;

INSERT INTO cidades VALUES (1,'Americana');
INSERT INTO cidades VALUES (2,'Araçatuba');
INSERT INTO cidades VALUES (3,' Assis');
INSERT INTO cidades VALUES (4,'Barueri');
INSERT INTO cidades VALUES (5,'Bauru');
INSERT INTO cidades VALUES (6,'Bebedouro ');
INSERT INTO cidades VALUES (7,'Botucatu');
INSERT INTO cidades VALUES (8,'Campinas');
INSERT INTO cidades VALUES (9,'Bragança Paulista ');
INSERT INTO cidades VALUES (10,'Capão Bonito');
INSERT INTO cidades VALUES (11,'Carapicuíba');
INSERT INTO cidades VALUES (12,'Catanduva');
INSERT INTO cidades VALUES (13,'Cotia');
INSERT INTO cidades VALUES (14,'Cruzeiro');
INSERT INTO cidades VALUES (15,'Diadema');
INSERT INTO cidades VALUES (16,'Franca ');
INSERT INTO cidades VALUES (17,'Garça ');
INSERT INTO cidades VALUES (18,'Guaratinguetá');
INSERT INTO cidades VALUES (19,'Guarulhos');
INSERT INTO cidades VALUES (20,'Indaiatuba');
INSERT INTO cidades VALUES (21,'Itapetininga');
INSERT INTO cidades VALUES (22,'Itapira');
INSERT INTO cidades VALUES (23,'Itaquaquecetuba');       
INSERT INTO cidades VALUES (24,'Itatiba');       
INSERT INTO cidades VALUES (25,'Itu ');       
INSERT INTO cidades VALUES (26,'Jaboticabal');       
INSERT INTO cidades VALUES (27,'Jacareí');       
INSERT INTO cidades VALUES (28,'Jales');  
INSERT INTO cidades VALUES (29,'Jaú');    
INSERT INTO cidades VALUES (30,'Jundiaí');
INSERT INTO cidades VALUES (31,'Lins');    
INSERT INTO cidades VALUES (32,'Marília ');    
INSERT INTO cidades VALUES (33,'Mauá');    
INSERT INTO cidades VALUES (34,'Mococa');    
INSERT INTO cidades VALUES (35,'Mogi das Cruzes'); 
INSERT INTO cidades VALUES (36,'Mogi Mirim');
INSERT INTO cidades VALUES (37,'Osasco');
INSERT INTO cidades VALUES (38,'Ourinhos');
INSERT INTO cidades VALUES (39,'Pindamonhangaba');
INSERT INTO cidades VALUES (40,'Piracicaba');
INSERT INTO cidades VALUES (41,'Pompéia');
INSERT INTO cidades VALUES (42,'Praia Grande');
INSERT INTO cidades VALUES (43,'Presidente Prudente');
INSERT INTO cidades VALUES (44,'Ribeirão Preto');
INSERT INTO cidades VALUES (45,'Santana de Parnaíba');
INSERT INTO cidades VALUES (46,'Santo André');
INSERT INTO cidades VALUES (47,'Santos');
INSERT INTO cidades VALUES (48,'São Bernardo do Campo');
INSERT INTO cidades VALUES (49,'São Caetano do Sul');
INSERT INTO cidades VALUES (50,'São Carlos');
INSERT INTO cidades VALUES (51,'São José do Rio Preto');
INSERT INTO cidades VALUES (52,'São José dos Campos');
INSERT INTO cidades VALUES (53,'São Paulo');
INSERT INTO cidades VALUES (54,'São Roque');
INSERT INTO cidades VALUES (55,'São Sebastião');
INSERT INTO cidades VALUES (56,'Sertãozinho');
INSERT INTO cidades VALUES (57,'Sorocaba');
INSERT INTO cidades VALUES (58,'Taquaritinga');   
INSERT INTO cidades VALUES (59,'Tatuí');
INSERT INTO cidades VALUES (60,'Taubaté ');    
   
  
 
INSERT INTO eixotecnologico VALUES (1,'Ambiente, Saúde e Segurança ');
INSERT INTO eixotecnologico VALUES (2,'Controle e Processos Industriais ');  
INSERT INTO eixotecnologico VALUES (3,'Gestão e Negócios ');  
INSERT INTO eixotecnologico VALUES (4,'Hospitalidade e Lazer ');  
INSERT INTO eixotecnologico VALUES (5,'Informação e Comunicação ');  
INSERT INTO eixotecnologico VALUES (6,'Infraestrutura ');
INSERT INTO eixotecnologico VALUES (7,'Produção Alimentícia ');
INSERT INTO eixotecnologico VALUES (8,'Produção Industrial ');       
INSERT INTO eixotecnologico VALUES (9,'Recursos Naturais ');  

INSERT INTO vestibular.disciplina VALUES (1,'Raciocínio Lógico');
INSERT INTO vestibular.disciplina VALUES (2,'Matemática');
INSERT INTO vestibular.disciplina VALUES (3,'Física');
INSERT INTO vestibular.disciplina VALUES (4,'Química');
INSERT INTO vestibular.disciplina VALUES (5,'Biologia');
INSERT INTO vestibular.disciplina VALUES (6,'Português');
INSERT INTO vestibular.disciplina VALUES (7,'Inglês');
INSERT INTO vestibular.disciplina VALUES (8,'História');
INSERT INTO vestibular.disciplina VALUES (9,'Geografia');
INSERT INTO vestibular.disciplina VALUES (10,'Multidisciplinar');


SELECT * FROM eixotecnologico;
SELECT * FROM curso;
SELECT * FROM disciplina;
SELECT * FROM curso_disciplina;
SELECT * FROM fatec;
SELECT * FROM cidades;
SELECT * FROM candidato;

UPDATE candidato SET pontuacaoAcrescidaEscolaridadePublica = 1  WHERE login = 'inanda' ;
     
INSERT INTO fatec VALUES ( 1,'13469-111' ,  'direcao@fatec.edu.br' , 'Emílio de Menezes' , 'SP' , 'Rua' , 'Fatec Americana','s/n ','www.fatec.edu.br ','(19) 3406-5776',1);
INSERT INTO fatec VALUES ( 2, '16052-045' ,  ' secretaria.fatec.aracatuba@gmail.com' , 'Prestes Maia' , 'SP' , 'Av' , 'Fatec Araçatuba - Prof. Fernando Amaral de Almeida Prado','1764','fatecaracatuba.edu.br','(18) 3625-9917',2);
INSERT INTO fatec VALUES ( 3, ' 19802-130' ,  '' , ' Senhor do Bonfim' , 'SP' , 'Rua' , 'Fatec Americana','1226 ','','(18) 3321-5266',1);



    
    
    INSERT INTO curso VALUES (1,'Agroindústria','','','',1);
    INSERT INTO curso VALUES (2,'Agronegócio','','','',1);
    INSERT INTO curso VALUES (3,'Alimentos','','','',1);
    INSERT INTO curso VALUES (4,'Análise e Desenvolvimento de Sistemas','','','',1);
    INSERT INTO curso VALUES (5,'Automação de Escritórios e Secretariado','','','',1);
    INSERT INTO curso VALUES (6,'Automação e Manufatura Digital','','','',1);
    INSERT INTO curso VALUES (7,'Automação Industrial','','','',1);
    INSERT INTO curso VALUES (8,'Banco de Dados','','','',1);
    INSERT INTO curso VALUES (9,' Biocombustíveis','','','',1);
    INSERT INTO curso VALUES (10,'Comércio Exterior','','','',1);
    INSERT INTO curso VALUES (11,'Construção Civil - Edifícios','','','',1);
    INSERT INTO curso VALUES (12,'Construção Civil - Movimento de Terra e Pavimentação','','','',1);
    INSERT INTO curso VALUES (13,' Construção de Edifícios','','','',1);
    INSERT INTO curso VALUES (14,'Construção Naval','','','',1);
    INSERT INTO curso VALUES (15,' Construção de Edifícios','','','',1);
    INSERT INTO curso VALUES (16,'Controle de Obras','','','',1);
    INSERT INTO curso VALUES (17,'Cosméticos','','','',1);
    INSERT INTO curso VALUES (18,'Eletrônica Automotiva','','','',1);
    INSERT INTO curso VALUES (19,'Eletrônica Industrial','','','',1);
    INSERT INTO curso VALUES (20,'Eventos','','','',1);
    INSERT INTO curso VALUES (21,'Fabricação Mecânica','','','',1);
    INSERT INTO curso VALUES (22,'Geoprocessamento','','','',1);
    INSERT INTO curso VALUES (23,'Gestão Ambiental','','','',1);
    INSERT INTO curso VALUES (24,'Gestão Comercial','','','',1);
    INSERT INTO curso VALUES (25,'Gestão da Produção Industrial','','','',1);
    INSERT INTO curso VALUES (26,'Gestão da Tecnologia da Informação','','','',1);
    INSERT INTO curso VALUES (27,'Gestão de Negócios e Inovação','','','',1);
    INSERT INTO curso VALUES (28,'Gestão de Recursos Humanos','','','',1);
    INSERT INTO curso VALUES (29,'Gestão de Serviços','','','',1);
    INSERT INTO curso VALUES (30,'Gestão de Turismo','','','',1);
    INSERT INTO curso VALUES (31,'Gestão Empresarial','','','',1);
    INSERT INTO curso VALUES (32,'Gestão Financeira','','','',1);
    INSERT INTO curso VALUES (33,'Gestão Portuária','','','',1);
    
   
    select if(vestibular.conteudoprogramatico.id = 1,'Teste','Teste2') AS Teste ,to_base64(conteudoprogramatico.programaProva) AS accprogramaProva from vestibular.conteudoprogramatico;

    
    
    
    
  