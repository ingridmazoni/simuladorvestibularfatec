package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import aplicacao.validation.ValidatorTurma;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import dominio.entity.curso.Periodo;
import dominio.entity.curso.TipoCurso;
import dominio.entity.curso.Turma;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.TurmaDao;

@Controller
public class TurmaController {
	
	
	private TurmaDao turmaDao;
	private FatecDao fatecDao;
	private CursoDao cursoDao;
	private ValidatorTurma validatorTurma;
	private Result result;
	
	public TurmaController() {
		
	}

	@Inject
	public TurmaController(
			TurmaDao turmaDao,
			FatecDao fatecDao,
			CursoDao cursoDao,
			ValidatorTurma validatorTurma,
			Result result) {
		this.turmaDao = turmaDao;
		this.fatecDao = fatecDao;
		this.cursoDao = cursoDao;
		this.validatorTurma = validatorTurma;
		this.result = result;
	}
	
	@Get("/turma/novo")
	public void salva(){
		
		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
	
		
		  
		
	}
	
	@Post("/turma/salva")
	public void salva(Turma turma){
		validatorTurma.validaTurma(turma);
		validatorTurma.onErrorRedirectTo(this).salva();
		turmaDao.insere(turma);
		result.include("sucesso", "Turma inserida com sucesso");
		result.redirectTo(this).salva();
		
	}
	

	@Get("/turma/pesquisa")
	public void pesquisa() {
		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
	
	}
	
	
	@Get("/turma")
	public void pesquisa(Turma turma) {
		if(turma.getFatec().getId()==0 && turma.getCurso().getId()==0 && turma.getTipoCurso()==null && turma.getPeriodo()==null){
			result.include("turmaList",turmaDao.pesquisaTodasAsTurmas(turma));
		}
		else{
			result.include("turmaList",turmaDao.pesquisaTurma(turma));
		}
		
		result.redirectTo(this).pesquisa();
		
	
	}
	

	@Get("/turma/{id}")
	public void edita(int id) {
		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("turma",turmaDao.pesquisaTurmaComId(id));
	
	}
	
	@Put("/turma/{turma.id}")
	public void edita(Turma turma) {
		validatorTurma.validaTurma(turma);
		validatorTurma.onErrorRedirectTo(this).edita(turma.getId());
		turmaDao.atualiza(turma);
		result.include("sucesso", "Turma editada com sucesso");
		result.redirectTo(this).salva();
	}
	
	@Delete("/turma/{turma.id}")
	public void apaga(Turma turma) {
		try{
			turmaDao.deleta(turma);
			result.include("sucesso", "A turma foi deletada com sucesso");
			result.redirectTo(this).pesquisa();
			
		}
		catch(PersistenceException erro){
			result.include("insucesso", erro.getMessage());
			result.redirectTo(this).pesquisa();
		}
		
		
	}

}



