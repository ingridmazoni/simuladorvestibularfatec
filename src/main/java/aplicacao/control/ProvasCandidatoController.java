package aplicacao.control;







import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverDirectHTTP;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import servicosTecnicos.UsuarioLogado;
import servicosTecnicos.dao.CandidatoDao;
import servicosTecnicos.dao.QuestaoVestibularDao;
import servicosTecnicos.dao.QuestoesProvaDao;
import servicosTecnicos.dao.RealizacaoProvaDao;
import sun.security.jca.GetInstance;


@Controller
public class ProvasCandidatoController {
	
	private QuestaoVestibularDao questaoVestibularDao;
	private CandidatoDao candidatoDao;
	private RealizacaoProvaDao realizacaoProvaDao;
	private QuestoesProvaDao questoesProvaDao;
	private Candidato c;
	private Result result;
	
	public ProvasCandidatoController() {
		
	}
	
	
	@Inject
	public ProvasCandidatoController(
			QuestaoVestibularDao questaoVestibularDao,
			CandidatoDao candidatoDao,
			RealizacaoProvaDao realizacaoProvaDao,
			QuestoesProvaDao questoesProvaDao,
			RealizacaoProva realizacaoProva,
			UsuarioLogado usuarioLogado,
			Result result) {
		this.questaoVestibularDao = questaoVestibularDao;
		this.candidatoDao = candidatoDao;
		this.realizacaoProvaDao = realizacaoProvaDao;
		this.questoesProvaDao = questoesProvaDao;
		this.result = result;
				
		this.c = candidatoDao.pesquisaCandidatoComLogin(usuarioLogado.getUsuario().getLogin());
		
		
		}
	
	
	@Get("/candidato/provas")
	public void provasCandidato(){
		
		//result.include("realizacaoProvaList",realizacaoProvaDao.pesquisaRealizacaoProva(c));
		result.include("dataRealizacao",realizacaoProvaDao.pesquisaDataProva(c));
		
	}
	
	
	@Post("/candidato/listaProvas")
	public void listaProvas(Integer data, String semestre){
	
			
		result.include("realizacaoProvaList",realizacaoProvaDao.pesquisaProvasCandidato(c, semestre, data));
		
		result.redirectTo(this).provasCandidato();
		
	
		
		
	}


	
	

}
