package aplicacao.control;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import aplicacao.validation.ValidatorNotaCandidato;
import aplicacao.validation.ValidatorNotaCorte;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import dominio.entity.curso.Curso;
import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Periodo;
import dominio.entity.entidadesUtilitarias.QuantidadeAcertosMateria;
import dominio.entity.questaoVestibular.Disciplina;
import dominio.entity.questaoVestibular.Semestre;
import servicosTecnicos.UsuarioLogado;
import servicosTecnicos.dao.CandidatoDao;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.DisciplinaDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.NotaCorteDao;
import servicosTecnicos.dao.QuestaoVestibularDao;
import servicosTecnicos.dao.QuestoesProvaDao;
import servicosTecnicos.dao.RealizacaoProvaDao;


@Controller
public class NotaCandidatoController {
	
	private NotaCorteDao notaCorteDao;
	private FatecDao fatecDao;
	private CursoDao cursoDao;
	private Result result;
	private Candidato c;
	private QuestoesProvaDao questoesProvaDao;
	private DisciplinaDao disciplinaDao;
	private RealizacaoProvaDao realizacaoProvaDao;
	private QuestaoVestibularDao questaoVestibularDao;
	private ValidatorNotaCandidato validatorNotaCandidato;
	private ValidatorNotaCorte validatorNotaCorte;
	private CandidatoDao candidatoDao;
	
	public NotaCandidatoController() {
		
	}
	
		
	@Inject
	public NotaCandidatoController(
			FatecDao fatecDao,
			CursoDao cursoDao,
			NotaCorteDao notaCorteDao,
			CandidatoDao candidatoDao,
			QuestoesProvaDao questoesProvaDao,
			RealizacaoProvaDao realizacaoProvaDao,
			DisciplinaDao disciplinaDao,
			QuestaoVestibularDao questaoVestibularDao,
			UsuarioLogado usuarioLogado,
			ValidatorNotaCandidato validatorNotaCandidato,
			ValidatorNotaCorte validatorNotaCorte,
			Result result) {
		this.notaCorteDao = notaCorteDao;
		this.cursoDao = cursoDao;
		this.fatecDao= fatecDao;
		this.result = result;
		this.questoesProvaDao = questoesProvaDao;
		this.disciplinaDao = disciplinaDao;
		this.realizacaoProvaDao = realizacaoProvaDao;
		this.questaoVestibularDao = questaoVestibularDao;
		this.validatorNotaCandidato = validatorNotaCandidato;
		this.validatorNotaCorte = validatorNotaCorte;
		this.candidatoDao=candidatoDao;
		this.c = candidatoDao.pesquisaCandidatoComLogin(usuarioLogado.getUsuario().getLogin());
		
	}
	
	
	@Get("/candidato/notaProva")
	public void notaCandidato(){
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("periodoList",Periodo.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("semestreList",Semestre.values());
		result.include("realizacaoProvaList",realizacaoProvaDao.pesquisaRealizacaoProva(c));
		result.include("anosList",notaCorteDao.pesquisaAnosVestibular());
		
		
	}
	
	
	@Post("/notaCorteCandidato")
	public void pesquisa(NotaCorte notaCorte) {
		
			validatorNotaCorte.validaNotaCorte2(notaCorte).onErrorRedirectTo(this).notaCandidato();
			result.include("notaCorteList",  notaCorteDao.pesquisaNotaCorte2(notaCorte));
			result.include("notaCorte",notaCorte);
		
	
	}
	
	
	@Post("/candidato/calculoNotaCandidato")
	public void calculaNotaCandidato(RealizacaoProva realizacaoProva,Curso curso,NotaCorte notaCorte,List<QuantidadeAcertosMateria> quantAcertos){
		validatorNotaCandidato.validaNotaCandidato(realizacaoProva, curso).onErrorRedirectTo(this).notaCandidato();
		
		if(realizacaoProva.getId()!=0 || curso.getId()!=0){
			
		
		List<QuantidadeAcertosMateria> qam= new ArrayList<QuantidadeAcertosMateria>();
		List<Disciplina>listaDisciplinas= disciplinaDao.pesquisaDisciplina();
	
		curso = cursoDao.pesquisaCursoComId(curso.getId());
		
	
		Long npc = 0L ;
	   
	   
	   for(Disciplina d:listaDisciplinas){
		   	   
		   Long quantQuestoes = questoesProvaDao.pesquisaQuantidadeQuestoesProva(realizacaoProva, d);
		   
		   	QuantidadeAcertosMateria qa =new QuantidadeAcertosMateria();
		   	qa.setQuantAcertosMateria(quantQuestoes);
		   	qa.setD(d);
		   	qam.add(qa);
		   	
		   	
		   
		  if(d.getId() == curso.getMateriasPesoDois().get(0).getId() || d.getId() == curso.getMateriasPesoDois().get(1).getId() ){
			  npc = npc + (quantQuestoes*2);
		   		
		 }
		  else{
			  npc = npc + quantQuestoes;
		  }
		   
	  }
		
		result.include("npc",npc);
		result.include("listaMaterias",qam);
		result.include("candidato", c);
		result.include("curso",curso);
		result.include("realizacaoProva",realizacaoProva);
		
		}
		
	
		if(notaCorte.getTurma().getFatec().getId()!=0) this.pesquisa(notaCorte);
	
		
		result.forwardTo(this).notaCandidato();
		
		
	}
	
	@Post("/candidato/{realizacaoProva.npc}/{candidato.id}/{realizacaoProva.id}/{curso.id}")
	public void detalhesNotaCandidato(Candidato candidato,RealizacaoProva realizacaoProva,Curso curso){
		
		List<QuantidadeAcertosMateria> qam= new ArrayList<QuantidadeAcertosMateria>();
		List<Disciplina>listaDisciplinas= disciplinaDao.pesquisaDisciplina();
		curso = cursoDao.pesquisaCursoComId(curso.getId());
		
		 for(Disciplina d:listaDisciplinas){
			 
			 Long quantQuestoes = questoesProvaDao.pesquisaQuantidadeQuestoesProva(realizacaoProva, d);
			   
			   	QuantidadeAcertosMateria qa =new QuantidadeAcertosMateria();
			   	qa.setQuantAcertosMateria(quantQuestoes);
			   	qa.setD(d);
			   	qam.add(qa);
		
		
		 }
		
		result.include("curso",curso);
		result.include("listaMaterias",qam);
		result.include("candidato",candidatoDao.pesquisaCandidatoComId(candidato.getId()));
		result.include("npc",realizacaoProva.getNpc());
	}

}
