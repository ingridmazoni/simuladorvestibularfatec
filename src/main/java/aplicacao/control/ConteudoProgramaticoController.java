package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import aplicacao.validation.ValidatorConteudoProgramatico;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import dominio.entity.questaoVestibular.ConteudoProgramatico;
import servicosTecnicos.dao.ConteudoProgramaticoDao;
import servicosTecnicos.dao.DisciplinaDao;

@Controller
public class ConteudoProgramaticoController {
	
	private ConteudoProgramaticoDao conteudoProgramaticoDao;
	private DisciplinaDao disciplinaDao;
	private ValidatorConteudoProgramatico validatorConteudoProgramatico;
	private Result result;
		
	public ConteudoProgramaticoController() {
		
	}

	@Inject
	public ConteudoProgramaticoController(
			ConteudoProgramaticoDao conteudoProgramaticoDao,
			DisciplinaDao disciplinaDao,
			ValidatorConteudoProgramatico validatorConteudoProgramatico,
			Result result) {
		this.conteudoProgramaticoDao = conteudoProgramaticoDao;
		this.disciplinaDao=disciplinaDao;
		this.validatorConteudoProgramatico = validatorConteudoProgramatico;
		this.result = result;
	}
	
	@Get("/conteudoProgramatico/novo")
	public void salva(){
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
		
		  
		
	}
	
	@Post("/conteudoProgramatico/salva")
	public void salva(ConteudoProgramatico conteudoProgramatico){
			validatorConteudoProgramatico.validaConteudoProgramatico(conteudoProgramatico);
			validatorConteudoProgramatico.onErrorRedirectTo(this).salva();
			conteudoProgramaticoDao.insere(conteudoProgramatico);
			result.include("sucesso", "Conteúdo Programático inserido com sucesso");
			result.redirectTo(this).salva();
	}
	

	@Get("/conteudoProgramatico/pesquisa")
	public void pesquisa() {
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
		
	}
	
	
	@Get("/conteudoProgramatico")
	public void pesquisa(ConteudoProgramatico conteudoProgramatico) {
		
		if(conteudoProgramatico.getDisciplina().getId()== 0 && conteudoProgramatico.getProgramaProva()==null){
			result.include("conteudoProgramaticoList", conteudoProgramaticoDao.pesquisaConteudoProgramatico());
		}
		else{
			result.include("conteudoProgramaticoList",conteudoProgramaticoDao.pesquisaConteudoProgramatico(conteudoProgramatico));
		}
			
		result.redirectTo(this).pesquisa();
	}
	

	@Get("/conteudoProgramatico/{id}")
	public void edita(int id) {
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
		result.include("conteudoProgramatico",conteudoProgramaticoDao.pesquisaConteudoProgramaticoComId(id));
	}
	
	@Put("/conteudoProgramatico/{conteudoProgramatico.id}")
	public void edita(ConteudoProgramatico conteudoProgramatico) {
		validatorConteudoProgramatico.validaConteudoProgramatico(conteudoProgramatico);
		validatorConteudoProgramatico.onErrorRedirectTo(this).edita(conteudoProgramatico.getId());
		conteudoProgramaticoDao.atualiza(conteudoProgramatico);
		result.include("sucesso", "Conteúdo Programático alterado com sucesso");
		result.redirectTo(this).salva();
	}
	
	@Delete("/conteudoProgramatico/{conteudoProgramatico.id}")
	public void apaga(ConteudoProgramatico conteudoProgramatico) {
			try{
				conteudoProgramaticoDao.deleta(conteudoProgramatico);
				result.include("sucesso", "O conteúdo programático foi deletado com sucesso");
				result.redirectTo(this).pesquisa();
			}
			catch(PersistenceException erro){
				result.include("insucesso", erro.getMessage());
				result.redirectTo(this).pesquisa();
			}
	}
	


}
