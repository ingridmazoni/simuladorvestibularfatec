package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import aplicacao.validation.ValidatorCurso;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import dominio.entity.curso.Curso;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.DisciplinaDao;
import servicosTecnicos.dao.EixoTecnologicoDao;

@Controller
public class CursoController {
	
	
	private CursoDao cursoDao;
	private EixoTecnologicoDao eixoTecnologicoDao;
	private DisciplinaDao disciplinaDao;
	private ValidatorCurso validatorCurso;
	private Result result;
	
	public CursoController() {
		
	}

	@Inject
	public CursoController(
			CursoDao cursoDao,
			EixoTecnologicoDao eixoTecnologicoDao,
			DisciplinaDao disciplinaDao,
			ValidatorCurso  validatorCurso,
			Result result) {
		this.cursoDao = cursoDao;
		this.eixoTecnologicoDao=eixoTecnologicoDao;
		this.validatorCurso= validatorCurso;
		this.disciplinaDao=disciplinaDao;
		this.result = result;
	}
	
	@Get("/curso/novo")
	public void salva(){
		
		result.include("eixoTecnologicoList",eixoTecnologicoDao.pesquisaEixoTecnologico());
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
		  
		
	}
	
	@Post("/curso/salva")
	public void salva(Curso curso){
		validatorCurso.validaCurso(curso);
		validatorCurso.onErrorRedirectTo(this).salva();
		cursoDao.insere(curso);
		result.include("sucesso", "Curso inserido com sucesso");
		result.redirectTo(this).salva();
		
	}
	

	@Get("/curso/pesquisa")
	public void pesquisa() {
		result.include("eixoTecnologicoList",eixoTecnologicoDao.pesquisaEixoTecnologico());
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
	}
	
	
	@Get("/curso")
	public void pesquisa(Curso curso) {
		
		if(curso.getEixoTecnologico().getId()==0 && curso.getNome()==null){
			result.include("cursoList",cursoDao.pesquisaTodosOsCurso());
		}
		else{
			result.include("cursoList",cursoDao.pesquisaCurso(curso));
		}
		
		
		result.redirectTo(this).pesquisa();
	
	}
	

	@Get("/curso/{id}")
	public void edita(int id) {
		result.include("eixoTecnologicoList",eixoTecnologicoDao.pesquisaEixoTecnologico());
		result.include("disciplinasList",disciplinaDao.pesquisaDisciplina());
		Curso curso = cursoDao.pesquisaCursoComId(id);
		result.include("curso",curso);
		
	}
	
	@Put("/curso/{curso.id}")
	public void edita(Curso curso) {
		validatorCurso.validaCurso(curso);
		validatorCurso.onErrorRedirectTo(this).edita(curso.getId());
		cursoDao.atualiza(curso);
		result.include("sucesso", "Curso alterado com sucesso");
		result.redirectTo(this).salva();
	}
	
	
	@Delete("/curso/{curso.id}")
	public void apaga(Curso curso) {
		try{
			cursoDao.deleta(curso);
			result.include("sucesso", "O curso foi deletado com sucesso");
			result.redirectTo(this).pesquisa();
			
		}
		catch(PersistenceException erro){
			result.include("insucesso", erro.getMessage());
			result.redirectTo(this).pesquisa();
		}
	}

}



