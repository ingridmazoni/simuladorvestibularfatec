package aplicacao.control;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;

import aplicacao.validation.ValidatorFatec;
import aplicacao.validation.ValidatorQuestaoVestibular;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import dominio.entity.ProvasDoCandidato.QuestoesProva;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.curso.Fatec;
import dominio.entity.questaoVestibular.QuestaoVestibular;
import servicosTecnicos.NoCache;
import servicosTecnicos.UsuarioLogado;
import servicosTecnicos.dao.CandidatoDao;
import servicosTecnicos.dao.CidadeDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.QuestaoVestibularDao;
import servicosTecnicos.dao.QuestoesProvaDao;
import servicosTecnicos.dao.RealizacaoProvaDao;

@Controller
@NoCache
public class RealizacaoProvaController {
	
	private QuestaoVestibularDao questaoVestibularDao;
	private RealizacaoProva realizacaoProva;
	private CandidatoDao candidatoDao;
	private RealizacaoProvaDao realizacaoProvaDao;
	private QuestoesProvaDao questoesProvaDao;
	private UsuarioLogado usuarioLogado;
	private Result result;
	private HttpServletRequest request;
	private List<QuestaoVestibular> listaQuestaoVestibular;
	
	
	
	
	public RealizacaoProvaController() {
			
	}
	
	@Inject
	public RealizacaoProvaController(
			QuestaoVestibularDao questaoVestibularDao,
			CandidatoDao candidatoDao,
			RealizacaoProvaDao realizacaoProvaDao,
			QuestoesProvaDao questoesProvaDao,
			RealizacaoProva realizacaoProva,
			UsuarioLogado usuarioLogado,
			HttpServletRequest request,
			Result result) {
		this.questaoVestibularDao = questaoVestibularDao;
		this.candidatoDao = candidatoDao;
		this.realizacaoProvaDao = realizacaoProvaDao;
		this.questoesProvaDao = questoesProvaDao;
		this.realizacaoProva = realizacaoProva;
		this.request=request;
		this.result = result;
		this.usuarioLogado = usuarioLogado;
						
		realizacaoProva.setCandidato(candidatoDao.pesquisaCandidatoComLogin(usuarioLogado.getUsuario().getLogin()));
				
		}
	
	
	@Post("/prova/novo")
	public void prova(int contador){ 
		try{
						
		if(contador == 0) {
			usuarioLogado.getListaQuestoes().clear();
			listaQuestaoVestibular=new ArrayList<QuestaoVestibular>();
			this.carregaQuestoes();
			request.getSession().setAttribute("listaQuestaoVestibular", listaQuestaoVestibular);
			
		}
		
		result.include("contador",++contador);
		List<QuestaoVestibular> qv=(List<QuestaoVestibular>)request.getSession().getAttribute("listaQuestaoVestibular");
		
		result.include("questaovestibular",qv.get(contador-1));
		
		}
		catch(NoResultException erro){
			result.include("insucesso","Não há questões suficientes cadastradas no sistema");
			result.redirectTo(ProvasCandidatoController.class).provasCandidato();
		}
		
	}
	
	
	@Post("/questaovestibular/salva")
	public void salva(QuestoesProva questoesprova, int contador){
			
		if(usuarioLogado.getListaQuestoes().size()<55 && contador<=54){
			questoesprova.setSequenciaDaQuestaoNaProva(contador);
			usuarioLogado.getListaQuestoes().add(questoesprova);
						
			if(contador==54){
				realizacaoProvaDao.insere(realizacaoProva);
				
					for(QuestoesProva qp:usuarioLogado.getListaQuestoes()){
						qp.setRealizacaoProva(realizacaoProva);
						questoesProvaDao.insere(qp);
					}
				result.include("prova","Prova respondida com sucesso");
				result.redirectTo(ProvasCandidatoController.class).provasCandidato();
			}
			else{
				result.forwardTo(this).prova(contador);
			}
				
		}
						
	}
	
	
	@Get("/prova/{questoesProva.realizacaoProva.id}/{questoesProva.sequenciaDaQuestaoNaProva}")
	public void provaRespondida(QuestoesProva questoesProva ){
		QuestoesProva qp= questoesProvaDao.pesquisaQuestoesProva(questoesProva);
		result.include("candidato",qp.getRealizacaoProva().getCandidato());
		result.include("questoesProvaList",questoesProvaDao.pesquisaListaQuestoesProva(questoesProva));
		result.include("questoesProva",qp);
		result.include("questaovestibular",questaoVestibularDao.pesquisaQuestaoVestibularComId(qp.getQuestaoVestibular().getId()));
	}	
	
	
	@Get("/prova/respondida")
	public void getQuestao(QuestoesProva questoesProva){
		result.redirectTo(this).provaRespondida(questoesProva);
	}
	
	
	public void carregaQuestoes(){
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Multidisciplinar", 9));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Raciocínio Lógico", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("História", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Química", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Inglês", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Matemática", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Física", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Geografia", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Biologia", 5));
		listaQuestaoVestibular.addAll(questaoVestibularDao.pesquisaQuestoes("Português", 5));
	}

}
