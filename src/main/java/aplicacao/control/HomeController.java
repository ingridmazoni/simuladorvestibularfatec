package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.entidadesUtilitarias.Usuario;
import servicosTecnicos.UsuarioLogado;
import servicosTecnicos.dao.UsuarioDao;
import servicosTecnicos.daoImplementation.FactoryEntityManager;

@Controller
public class HomeController {
	
	Result result;
	UsuarioDao usuarioDao;
	Validator validator;
	UsuarioLogado userLogado;
	
	
    public HomeController() {
	
	}
    
    @Inject
    public HomeController(Result result , UsuarioDao usuarioDao , Validator validator , UsuarioLogado userLogado) {
    	this.result=result;
    	this.usuarioDao= usuarioDao;
    	this.validator= validator;
    	this.userLogado = userLogado;
	}
    
 
	@Get("/")
	public void formulario(){
		userLogado.desloga();
	}
	
	
	@Post("/login")
	public void login(String login, String senha) {
	Usuario usuario = usuarioDao.validaUsuarioComLoginESenha(login,senha);
	
	
		validator.ensure(usuario != null, new I18nMessage("login e senha ","usuario.ou.senha.invalida"));
		validator.onErrorRedirectTo(this).formulario();
		userLogado.loga(usuario);
		result.redirectTo(DecoratorsController.class).menuPrincipal();
				
	}
	
	
	@Get("/logout")
	public void logout() {
		result.redirectTo(this).formulario();
	}
	
	
	@Get("/populaBanco")
	public void populaBanco() {
		
		EntityManager manager= FactoryEntityManager.getEntityManager();
		manager.close();
		result.redirectTo(this).formulario();
		
	}
	
	
}