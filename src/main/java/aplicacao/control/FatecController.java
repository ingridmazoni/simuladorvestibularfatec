package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import aplicacao.validation.ValidatorFatec;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import dominio.entity.curso.Fatec;
import servicosTecnicos.dao.CidadeDao;
import servicosTecnicos.dao.FatecDao;


@Controller
public class FatecController {
	
	private FatecDao fatecDao;
	private CidadeDao cidadeDao;
	private ValidatorFatec validatorFatec;
	private Result result;
	
	public FatecController() {
		
	}

	@Inject
	public FatecController(
			FatecDao fatecDao,
			CidadeDao cidadeDao,
			ValidatorFatec validatorFatec,
			Result result) {
		this.fatecDao = fatecDao;
		this.cidadeDao=cidadeDao;
		this.validatorFatec= validatorFatec;
		this.result = result;
	}
	
	@Get("/fatec/novo")
	public void salva(){
		
		result.include("cidadesList",cidadeDao.pesquisaCidades());
		  
		
	}
	
	@Post("/fatec/salva")
	public void salva(Fatec fatec){
		validatorFatec.validaFatec(fatec);
		validatorFatec.onErrorRedirectTo(this).salva();
		fatecDao.insere(fatec);
		result.include("sucesso", "Fatec inserida com sucesso");
		result.redirectTo(this).salva();
		
	}
	

	@Get("/fatec/pesquisa")
	public void pesquisa() {
		result.include("cidadesList",cidadeDao.pesquisaCidades());
	}
	
	
	@Get("/fatec")
	public void pesquisa(Fatec fatec) {
		
		if(fatec.getCidade().getId()== 0 && fatec.getNome()==null){
			result.include("fatecList",fatecDao.pesquisaTodosAsFatecs());
		}
		else{
			result.include("fatecList",fatecDao.pesquisaFatec(fatec));
		}
		
		
		result.redirectTo(this).pesquisa();
	}
	

	@Get("/fatec/{id}")
	public void edita(int id) {
		result.include("cidadesList",cidadeDao.pesquisaCidades());
		result.include("fatec",fatecDao.pesquisaFatecComId(id));
	}
	
	@Put("/fatec/{fatec.id}")
	public void edita(Fatec fatec) {
		validatorFatec.validaFatec(fatec);
		validatorFatec.onErrorRedirectTo(this).salva();
		fatecDao.atualiza(fatec);
		result.include("sucesso", "Fatec alterada com sucesso");
		result.redirectTo(this).salva();
	}
	
	@Delete("/fatec/{fatec.id}")
	public void apaga(Fatec fatec) {
		try{
			
			fatecDao.deleta(fatec);
			result.include("sucesso", "O fatec foi deletada com sucesso");
			result.redirectTo(this).pesquisa();
			
		}
		catch(PersistenceException erro){
			result.include("insucesso", erro.getMessage());
			result.redirectTo(this).pesquisa();
		}
	}
	

}
