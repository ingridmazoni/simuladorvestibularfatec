package aplicacao.control;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.NoResultException;

import aplicacao.validation.ValidatorCandidato;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.http.InvalidParameterException;
import dominio.entity.candidato.Candidato;
import dominio.entity.candidato.EscolhaDeCursoDoCandidato;
import dominio.entity.candidato.OpcaoCandidato;
import dominio.entity.candidato.Sexo;
import dominio.entity.curso.Periodo;
import dominio.entity.curso.Turma;
import dominio.entity.entidadesUtilitarias.Perfil;
import servicosTecnicos.dao.CandidatoDao;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.EscolhaDeCursoDoCandidatoDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.TurmaDao;

@Controller
public class CandidatoController {
	
	private CandidatoDao candidatoDao;
	private FatecDao fatecDao;
	private CursoDao cursoDao;
	private ValidatorCandidato validatorCandidato;
	private TurmaDao turmaDao;
	private EscolhaDeCursoDoCandidatoDao escolhaDeCursoDoCandidatoDao;
	private Result result;
	
	public CandidatoController() {
		
	}

	@Inject
	public CandidatoController(
			CandidatoDao candidatoDao,
			ValidatorCandidato validatorCandidato,
			FatecDao fatecDao,
			CursoDao cursoDao,
			EscolhaDeCursoDoCandidatoDao escolhaDeCursoDoCandidatoDao,
			TurmaDao turmaDao,
			Result result) {
		this.candidatoDao = candidatoDao;
		this.validatorCandidato = validatorCandidato;
		this.fatecDao = fatecDao;
		this.cursoDao = cursoDao;
		this.turmaDao = turmaDao;
		this.escolhaDeCursoDoCandidatoDao = escolhaDeCursoDoCandidatoDao;
		this.result = result;
	}
	
	@Get("/candidatoNovo")
	public void salva(){
		
		result.include("sexoList",Sexo.values());
		result.include("opcaoCandidatoList",OpcaoCandidato.values());
		result.include("periodoList",Periodo.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("perfil",Perfil.CANDIDATO);
		
		  
		
	}
	
	@Post("/candidato/salva")
	public void salva(Candidato candidato,List<EscolhaDeCursoDoCandidato> escolhaDeCursoDoCandidato){
		String mensagem= "Candidato inserido com sucesso";
		validatorCandidato.validaCandidato(candidato);
		validatorCandidato.onErrorRedirectTo(this).salva();
				
		/*escolhaDeCursoDoCandidato.get(0).setTurma(turmaDao.pesquisaTurma2(escolhaDeCursoDoCandidato.get(0).getTurma()));
		escolhaDeCursoDoCandidato.get(0).setOpcaoCandidato(OpcaoCandidato.PRIMEIRA_OPCAO_DE_CURSO);
		
		escolhaDeCursoDoCandidato.get(1).setTurma(turmaDao.pesquisaTurma2(escolhaDeCursoDoCandidato.get(1).getTurma()));
		escolhaDeCursoDoCandidato.get(1).setOpcaoCandidato(OpcaoCandidato.SEGUNDA_OPCAO_DE_CURSO);
		*/
		candidatoDao.insere(candidato);
				
	/*	if(escolhaDeCursoDoCandidato.get(0).getTurma()!=null && escolhaDeCursoDoCandidato.get(0).getTurma().getId()!=0){
			escolhaDeCursoDoCandidato.get(0).setCandidato(candidato);
			escolhaDeCursoDoCandidatoDao.insere(escolhaDeCursoDoCandidato.get(0));
		}
		else{
			mensagem=mensagem+ "\r\nPrimeira opção de curso do candidato inválida";
		}
		if( escolhaDeCursoDoCandidato.get(1).getTurma()!=null && escolhaDeCursoDoCandidato.get(1).getTurma().getId()!=0){
			escolhaDeCursoDoCandidato.get(1).setCandidato(candidato);
			escolhaDeCursoDoCandidatoDao.insere(escolhaDeCursoDoCandidato.get(1));
		}
		else{
			mensagem=mensagem+ "\r\nSegunda opção de curso do candidato inválida";
		}
		*/
				
		result.include("sucesso", mensagem);
		result.redirectTo(HomeController.class).formulario();
		
		
		
		
		
	}
	

	@Get("/candidato/pesquisa")
	public void pesquisa() {
		result.include("sexoList",Sexo.values());
		result.include("opcaoCandidatoList",OpcaoCandidato.values());
		result.include("periodoList",Periodo.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("perfil",Perfil.CANDIDATO);
		
		
	}
	
	
	@Get("/candidato")
	public void pesquisa(Candidato candidato) {
		/*candidatoDao.pesquisa(candidato);*/
		
	
	}
	

	@Get("/candidato/{id}")
	public void edita(int id) {
		Candidato cand = candidatoDao.pesquisaCandidatoComId(id);
		result.include("candidato", cand);
		result.include("sexoList",Sexo.values());
		result.include("opcaoCandidatoList",OpcaoCandidato.values());
		result.include("periodoList",Periodo.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("perfil",Perfil.CANDIDATO);
	}
	
	@Put("/candidato/{candidato.id}")
	public void edita(Candidato candidato,List<EscolhaDeCursoDoCandidato> escolhaDeCursoDoCandidato) {
		try{
			String mensagem= "Candidato alterado com sucesso";
			validatorCandidato.validaCandidatoEdicao(candidato);
			validatorCandidato.onErrorRedirectTo(this).edita(candidato.getId());
			
			candidatoDao.atualiza(candidato);
			
			/*escolhaDeCursoDoCandidato.get(0).setTurma(turmaDao.pesquisaTurma2(escolhaDeCursoDoCandidato.get(0).getTurma()));
			escolhaDeCursoDoCandidato.get(0).setOpcaoCandidato(OpcaoCandidato.PRIMEIRA_OPCAO_DE_CURSO);
			
			escolhaDeCursoDoCandidato.get(1).setTurma(turmaDao.pesquisaTurma2(escolhaDeCursoDoCandidato.get(1).getTurma()));
			escolhaDeCursoDoCandidato.get(1).setOpcaoCandidato(OpcaoCandidato.SEGUNDA_OPCAO_DE_CURSO);
					
			if(escolhaDeCursoDoCandidato.get(0).getTurma()!=null && escolhaDeCursoDoCandidato.get(0).getTurma().getId()!=0){
				escolhaDeCursoDoCandidato.get(0).setCandidato(candidato);
				escolhaDeCursoDoCandidatoDao.atualiza(escolhaDeCursoDoCandidato.get(0));
			}
			else{
				mensagem=mensagem+ "\r\nPrimeira opção de curso do candidato inválida";
			}
			if( escolhaDeCursoDoCandidato.get(1).getTurma()!=null && escolhaDeCursoDoCandidato.get(1).getTurma().getId()!=0){
				escolhaDeCursoDoCandidato.get(1).setCandidato(candidato);
				escolhaDeCursoDoCandidatoDao.atualiza(escolhaDeCursoDoCandidato.get(1));
			}
			else{
				mensagem=mensagem+ "\r\nSegunda opção de curso do candidato inválida";
			}*/
							
			result.include("sucesso", mensagem);
			result.redirectTo(this).edita(candidato.getId());
		}
		catch (InvalidParameterException erro) {
			result.include("insucesso", "Valor digitado no campo nota inválido");
			result.redirectTo(this).edita(candidato.getId());
		}
		
		
	}
	
	
	@Get("/candidatoCadastro/{candidato.usuario.login}")
	public void carregaPaginaCandidato(Candidato candidato){
		Candidato c1;
		c1 = candidatoDao.pesquisaCandidatoComLogin(candidato.getUsuario().getLogin());
		result.redirectTo(this).edita(c1.getId());
		
	}

}



