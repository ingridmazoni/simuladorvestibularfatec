package aplicacao.control;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;

import aplicacao.validation.ValidatorQuestaoVestibular;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import dominio.entity.questaoVestibular.QuestaoVestibular;
import dominio.entity.questaoVestibular.RespostaQuestao;
import dominio.entity.questaoVestibular.Semestre;
import servicosTecnicos.NoCache;
import servicosTecnicos.dao.ConteudoProgramaticoDao;
import servicosTecnicos.dao.DisciplinaDao;
import servicosTecnicos.dao.QuestaoVestibularDao;
import sun.text.normalizer.Replaceable;

@Controller
public class QuestaoVestibularController {
	
	
	private QuestaoVestibularDao questaoVestibularDao;
	private DisciplinaDao disciplinaDao;
	private ConteudoProgramaticoDao conteudoProgramaticoDao;
	private ValidatorQuestaoVestibular validatorQuestaoVestibular;
	private Result result;
    private HttpServletRequest request;
    private String pasta;
	
	public QuestaoVestibularController() {
		
	}

	@Inject
	public QuestaoVestibularController(
			QuestaoVestibularDao questaoVestibularDao,
			DisciplinaDao disciplinaDao,
			ConteudoProgramaticoDao conteudoProgramaticoDao,
			ValidatorQuestaoVestibular validatorQuestaoVestibular,
			HttpServletRequest request,
			Result result) {
		this.questaoVestibularDao = questaoVestibularDao;
		this.validatorQuestaoVestibular = validatorQuestaoVestibular;
		this.disciplinaDao=disciplinaDao;
		this.conteudoProgramaticoDao = conteudoProgramaticoDao;
		this.request = request;
		this.result = result;
		
		pasta=request.getRealPath("/")+"images";
	}
	
	@Get("/questaoVestibular/novo")
	public void salva(){
		
		result.include("disciplinaList",disciplinaDao.pesquisaDisciplina());
		result.include("conteudoProgramaticoList",conteudoProgramaticoDao.pesquisaConteudoProgramatico());
		result.include("respostaQuestaoList",RespostaQuestao.values());
		result.include("semestreList",Semestre.values());
		
		  
		
	}
	
	@Post("/questaoVestibular/salva")
	public void salva(QuestaoVestibular questaoVestibular) throws IOException{
	
		System.out.println(pasta);
				
	if(questaoVestibular.getImagemPergunta()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemPergunta());
		questaoVestibular.getArquivoImagemPergunta().writeTo(pergunta);
	
	}	
	if(questaoVestibular.getImagemAlternativaA()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaA());
		questaoVestibular.getArquivoImagemAlternativaA().writeTo(pergunta);
	}
	if(questaoVestibular.getImagemAlternativaB()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaB());
		questaoVestibular.getArquivoImagemAlternativaB().writeTo(pergunta);
	}
	if(questaoVestibular.getImagemAlternativaC()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaC());
		questaoVestibular.getArquivoImagemAlternativaC().writeTo(pergunta);
	}
	if(questaoVestibular.getImagemAlternativaD()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaD());
		questaoVestibular.getArquivoImagemAlternativaD().writeTo(pergunta);
	}
	if(questaoVestibular.getImagemAlternativaE()!=null)	{				
		File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaE());
		questaoVestibular.getArquivoImagemAlternativaE().writeTo(pergunta);
	}
				
		validatorQuestaoVestibular.validaQuestaoVestibular(questaoVestibular);
		validatorQuestaoVestibular.onErrorRedirectTo(this).salva();
		questaoVestibularDao.insere(questaoVestibular);
		result.include("sucesso", "Questao de Vestibular inserida com sucesso");
		result.redirectTo(this).salva();
		
	}
	

	@Get("/questaoVestibular/pesquisa")
	public void pesquisa() {
		result.include("disciplinaList",disciplinaDao.pesquisaDisciplina());
		result.include("conteudoProgramaticoList",conteudoProgramaticoDao.pesquisaConteudoProgramatico());
		result.include("respostaQuestaoList",RespostaQuestao.values());
		result.include("semestreList",Semestre.values());
		
		  
	}
	
	
	@Get("/questaoVestibular")
	public void pesquisa(QuestaoVestibular questaoVestibular) {
		result.include("questaoVestibularList",questaoVestibularDao.pesquisaQuestaoVestibular(questaoVestibular));
		result.redirectTo(this).pesquisa();
	
	}
	

	@Get("/questaoVestibular/{id}")
	public void edita(int id) {
		result.include("questaoVestibular",questaoVestibularDao.pesquisaQuestaoVestibularComId(id));
		result.include("conteudoProgramaticoList",conteudoProgramaticoDao.pesquisaConteudoProgramatico());
		result.include("disciplinaList",disciplinaDao.pesquisaDisciplina());
		result.include("respostaQuestaoList",RespostaQuestao.values());
		result.include("semestreList",Semestre.values());
	}
	
	@Post("/questaoVestibular/{questaoVestibular.id}")
	public void edita(QuestaoVestibular questaoVestibular) throws IOException {
	
		QuestaoVestibular qv = questaoVestibularDao.pesquisaQuestaoVestibularComId(questaoVestibular.getId());
		
		if(questaoVestibular.getImagemPergunta()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemPergunta());
			questaoVestibular.getArquivoImagemPergunta().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemPergunta(qv.getImagemPergunta());
		}
		
		if(questaoVestibular.getImagemAlternativaA()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaA());
			questaoVestibular.getArquivoImagemAlternativaA().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemAlternativaA(qv.getImagemAlternativaA());
		}
		if(questaoVestibular.getImagemAlternativaB()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaB());
			questaoVestibular.getArquivoImagemAlternativaB().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemAlternativaB(qv.getImagemAlternativaB());
		}
		
		if(questaoVestibular.getImagemAlternativaC()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaC());
			questaoVestibular.getArquivoImagemAlternativaC().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemAlternativaC(qv.getImagemAlternativaC());
		}
		
		if(questaoVestibular.getImagemAlternativaD()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaD());
			questaoVestibular.getArquivoImagemAlternativaD().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemAlternativaD(qv.getImagemAlternativaD());
		}
		
		if(questaoVestibular.getImagemAlternativaE()!=null)	{				
			File pergunta = new File(pasta, questaoVestibular.getImagemAlternativaE());
			questaoVestibular.getArquivoImagemAlternativaE().writeTo(pergunta);
		}
		else{
			questaoVestibular.setImagemAlternativaE(qv.getImagemAlternativaE());
		}
		
		validatorQuestaoVestibular.validaQuestaoVestibular(questaoVestibular);
		validatorQuestaoVestibular.onErrorRedirectTo(this).edita(questaoVestibular.getId());
		questaoVestibularDao.atualiza(questaoVestibular);
		result.include("sucesso", "Questão de Vestibular editada com sucesso");
		result.redirectTo(this).salva();
	}
	
	@Delete("/questaoVestibular/{questaoVestibular.id}")
	public void apaga(QuestaoVestibular questaoVestibular) {
				try{
					questaoVestibularDao.deleta(questaoVestibular);
					result.include("sucesso", "A Questao Vestibular foi deletada com sucesso");
					result.redirectTo(this).pesquisa();
					
				}
				catch(PersistenceException erro){
					result.include("insucesso", erro.getMessage());
					result.redirectTo(this).pesquisa();
				}
	}

}



