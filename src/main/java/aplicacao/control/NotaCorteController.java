package aplicacao.control;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import aplicacao.validation.ValidatorNotaCorte;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Periodo;
import dominio.entity.curso.TipoCurso;
import dominio.entity.curso.Turma;
import dominio.entity.questaoVestibular.Semestre;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.NotaCorteDao;
import servicosTecnicos.dao.TurmaDao;

@Controller
public class NotaCorteController {
	
	
	private NotaCorteDao notaCorteDao;
	private TurmaDao turmaDao;
	private FatecDao fatecDao;
	private CursoDao cursoDao;
	private ValidatorNotaCorte validatorNotaCorte;
	private Result result;
	
	public NotaCorteController() {
		
	}

	@Inject
	public NotaCorteController(
			NotaCorteDao notaCorteDao,
			FatecDao fatecDao,
			TurmaDao turmaDao,
			CursoDao cursoDao,
			ValidatorNotaCorte validatorNotaCorte,
			Result result) {
		this.notaCorteDao = notaCorteDao;
		this.cursoDao = cursoDao;
		this.fatecDao= fatecDao;
		this.turmaDao = turmaDao;
		this.validatorNotaCorte = validatorNotaCorte;
		this.result = result;
	}
	
	@Get("/notaCorte/novo")
	public void salva(){

		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("semestreList",Semestre.values());
	
		
	}
	
	@Post("/notaCorte/salva")
	public void salva(NotaCorte notaCorte){
		validatorNotaCorte.validaNotaCorte(notaCorte);
		validatorNotaCorte.onErrorRedirectTo(this).salva();
				
			try{
				Turma t = turmaDao.pesquisaTurma(notaCorte);
				if(t != null){
				notaCorte.setTurma(t);
				notaCorteDao.insere(notaCorte);
				result.include("sucesso", "Nota de Corte inserida com sucesso");
				
				}
			}
			catch(NoResultException erro){
				result.include("insucesso", "Fatec, Curso e Período não correspondem a uma Turma válida");
							
			}
			finally{
				result.redirectTo(this).salva();
			}
			
	}
	

	@Get("/notaCorte/pesquisa")
	public void pesquisa() {
		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("semestreList",Semestre.values());
		
	}
	
	
	@Get("/notaCorte")
	public void pesquisa(NotaCorte notaCorte) {
	
		result.include("notaCorteList",  notaCorteDao.pesquisaNotaCorte(notaCorte));
		result.redirectTo(this).pesquisa();
	
	}
	

	@Get("/notaCorte/{id}")
	public void edita(int id) {
		result.include("notaCorte",notaCorteDao.pesquisaNotaCorteComId(id));
		result.include("periodoList",Periodo.values());
		result.include("tipoCursoList",TipoCurso.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("semestreList",Semestre.values());
	}
	
	@Put("/notaCorte/{notaCorte.id}")
	public void edita(NotaCorte notaCorte) {
		validatorNotaCorte.validaNotaCorte(notaCorte);
		validatorNotaCorte.onErrorRedirectTo(this).edita(notaCorte.getId());
				
		try{
			Turma t = turmaDao.pesquisaTurma(notaCorte);
			
			if(t != null){
			notaCorte.setTurma(t);
			notaCorteDao.atualiza(notaCorte);
			result.include("sucesso", "Nota de Corte editada com sucesso");
			result.redirectTo(this).salva();
			}
		}
		catch(NoResultException erro){
			result.include("insucesso", "Fatec, Curso e Período não correspondem a uma Turma válida");
			result.redirectTo(this).edita(notaCorte.getId());
			
			
		}
		

	}
	
	@Delete("/notaCorte/{notaCorte.id}")
	public void apaga(NotaCorte notaCorte) {
		try{
			notaCorteDao.deleta(notaCorte);
			result.include("sucesso", "A nota de corte foi deletada com sucesso");
			result.redirectTo(this).pesquisa();
			
		}
		catch(PersistenceException erro){
			result.include("insucesso", erro.getMessage());
			result.redirectTo(this).pesquisa();
		}
		
	}

}


