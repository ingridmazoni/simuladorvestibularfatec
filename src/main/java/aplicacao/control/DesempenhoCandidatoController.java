package aplicacao.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;

import aplicacao.validation.ValidatorNotaCorte;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import dominio.entity.candidato.EscolhaDeCursoDoCandidato;
import dominio.entity.curso.Curso;
import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Periodo;
import dominio.entity.curso.TipoCurso;
import dominio.entity.curso.Turma;
import dominio.entity.entidadesUtilitarias.QuantidadeAcertosMateria;
import dominio.entity.entidadesUtilitarias.TipoLista;
import dominio.entity.questaoVestibular.Disciplina;
import dominio.entity.questaoVestibular.Semestre;
import servicosTecnicos.dao.CandidatoDao;
import servicosTecnicos.dao.CursoDao;
import servicosTecnicos.dao.DisciplinaDao;
import servicosTecnicos.dao.EscolhaDeCursoDoCandidatoDao;
import servicosTecnicos.dao.FatecDao;
import servicosTecnicos.dao.NotaCorteDao;
import servicosTecnicos.dao.QuestoesProvaDao;
import servicosTecnicos.dao.RealizacaoProvaDao;
import servicosTecnicos.dao.TurmaDao;

@Controller
public class DesempenhoCandidatoController {
	
	
	private FatecDao fatecDao;
	private CursoDao cursoDao;
	private NotaCorteDao notaCorteDao;
	private EscolhaDeCursoDoCandidatoDao escolhaDeCursoDoCandidatoDao;
	private Result result;
	private QuestoesProvaDao questoesProvaDao;
	private DisciplinaDao disciplinaDao;
	private RealizacaoProva realizacaoProva;
	private RealizacaoProvaDao realizacaoProvaDao;
	private List<Disciplina>listaDisciplinas;	
	private Curso curso;
	private CandidatoDao candidatoDao;
	private HttpServletRequest request;
	
	public DesempenhoCandidatoController(){
		
	}
	
	@Inject
	public DesempenhoCandidatoController(
			FatecDao fatecDao,
			CursoDao cursoDao,
			Candidato candidato,
			NotaCorteDao notaCorteDao,
			EscolhaDeCursoDoCandidatoDao escolhaDeCursoDoCandidatoDao,
			QuestoesProvaDao questoesProvaDao,
			RealizacaoProva realizacaoProva,
			RealizacaoProvaDao realizacaoProvaDao,
			DisciplinaDao disciplinaDao,
			CandidatoDao candidatoDao,
			HttpServletRequest request,
			Result result) {
		this.cursoDao = cursoDao;
		this.fatecDao= fatecDao;
		this.notaCorteDao = notaCorteDao;
		this.escolhaDeCursoDoCandidatoDao = escolhaDeCursoDoCandidatoDao;
		this.questoesProvaDao=questoesProvaDao;
		this.disciplinaDao=disciplinaDao;
		this.realizacaoProva=realizacaoProva;
		this.result = result;
		this.realizacaoProvaDao=realizacaoProvaDao;
		this.candidatoDao= candidatoDao;
		this.request=request;
		listaDisciplinas= disciplinaDao.pesquisaDisciplina();
		
	}
	
	@Get("/desempenhoCandidato/novo")
	public void desempenhoCandidato(){
		
		result.include("periodoList",Periodo.values());
		result.include("fatecList",fatecDao.pesquisaFatec());
		result.include("cursoList",cursoDao.pesquisaCursos());
		result.include("semestreList",Semestre.values());
		result.include("anosList",notaCorteDao.pesquisaAnosVestibular());
				
	}

	@Post("/verificarDesempenho")
	public void verificarDesempenho(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato,NotaCorte notaCorte,TipoLista tipoLista,Date datainicio,Date datafim){
		/*try{*/
			List<Candidato> listaCandidato;
			if(notaCorte!=null){
				notaCorte.setTurma(escolhaDeCursoDoCandidato.getTurma());
				notaCorte= notaCorteDao.pesquisaNotaCorte3(notaCorte);
			}
			
			request.getSession().setAttribute("escolhaDeCursoDoCandidato1",escolhaDeCursoDoCandidato);
			request.getSession().setAttribute("notaCorte1",notaCorte);
			request.getSession().setAttribute("tipoLista1",tipoLista);
			request.getSession().setAttribute("datainicio1",datainicio);
			request.getSession().setAttribute("datafim1",datafim);
		
			
			if(escolhaDeCursoDoCandidato.getOpcaoCandidato().toString().equalsIgnoreCase("OPCAO_ADMINISTRADOR")){
				listaCandidato=realizacaoProvaDao.pesquisaCandidatos();
			}
			else{
				listaCandidato=escolhaDeCursoDoCandidatoDao.listaDeCandidatos(escolhaDeCursoDoCandidato);
			}
			
			if(!listaCandidato.isEmpty()){
					List<RealizacaoProva> listaRealizacaoProva =realizacaoProvaDao.pesquisaRealizaçãoProvaPorCandidato(listaCandidato,datainicio,datafim);
					this.curso = cursoDao.pesquisaCursoComId(escolhaDeCursoDoCandidato.getTurma().getCurso().getId());
			
					for(RealizacaoProva rp:listaRealizacaoProva){
						
						this.calculaNotaCandidato(rp, curso);
					
					}
						SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
						result.include("tipoLista",tipoLista);
						result.include("escolhaDeCursoDoCandidato",escolhaDeCursoDoCandidato);
						result.include("notaCorte",notaCorte!= null ? notaCorte:null);
						result.include("realizacaoProvaList",listaRealizacaoProva);
						result.include("curso",this.curso);
						result.include("datainicio",datainicio != null ? sdf.format(datainicio):null);
						result.include("datafim",datafim != null ? sdf.format(datafim):null);
						result.forwardTo(this).desempenhoCandidato();
						
			}else{
				result.include("insucesso", "Não foi possivel exibir a lista de candidatos");
				result.forwardTo(this).desempenhoCandidato();
			}
				
			/*}
			catch(Exception erro){
				result.include("insucesso", "Não foi possivel exibir a lista de candidatos" + erro.getMessage());
				result.forwardTo(this).desempenhoCandidato();
			
			}*/
	}
	
	
	
	
	public void calculaNotaCandidato(RealizacaoProva realizacaoProva,Curso curso){
		Double npc= 0.0;
		Double P;
		Double NF;
		Double Enem;
		Double N;
		Double A=0.0;
		Double EP=0.0;

		if(realizacaoProva.getId()!=0 || curso.getId()!=0){
			

			npc = npc+ questoesProvaDao.pesquisaQuantidadeTotalQuestoesProva(realizacaoProva);
	   
			npc = npc+questoesProvaDao.pesquisaQuantidadeQuestoesProva(realizacaoProva, curso.getMateriasPesoDois().get(0));
	   
			npc = npc+questoesProvaDao.pesquisaQuantidadeQuestoesProva(realizacaoProva, curso.getMateriasPesoDois().get(1));
		
			try{
				Enem = (realizacaoProva.getCandidato().getNotaEnem()*100)/1000;
			}
			catch(NullPointerException erro){
				Enem = 0.0;
			}
			
			
			P=(100*npc)/64;
			
			if(Enem>P){
				N=(4*P+1*Enem)/5;
			}
			else{
				N=P;
			}
			
			NF=(8*N+2*0)/10;
			
			
			if(realizacaoProva.getCandidato().isPontuacaoAcrescidaAfrodescendente()){
				A= (NF * 3)/100;
			}
			
			if(realizacaoProva.getCandidato().isPontuacaoAcrescidaEscolaridadePublica()){
				EP= (NF *10)/100;
			}
			
			realizacaoProva.setNotaProva(NF+A+EP);
			
			realizacaoProva.setNpc(npc.intValue());
	 		
		}
		 
	}
	
	@Post("/voltarPesquisa")
	public void voltarPesquisa(){
		
		EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato= (EscolhaDeCursoDoCandidato)request.getSession().getAttribute("escolhaDeCursoDoCandidato1");
		NotaCorte notaCorte= (NotaCorte)request.getSession().getAttribute("notaCorte1");
		TipoLista tipoLista= (TipoLista)request.getSession().getAttribute("tipoLista1");
		Date dataInicio= (Date)request.getSession().getAttribute("datainicio1");
		Date dataFim= (Date)request.getSession().getAttribute("datafim1");
		
		this.verificarDesempenho(escolhaDeCursoDoCandidato, notaCorte, tipoLista,
				dataInicio != null ? dataInicio:null,
				dataFim != null ? dataFim:null);
		
	}
}
