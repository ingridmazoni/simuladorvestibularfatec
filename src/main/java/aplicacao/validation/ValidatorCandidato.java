package aplicacao.validation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.candidato.Candidato;
import servicosTecnicos.UsuarioLogado;
import servicosTecnicos.dao.CandidatoDao;

@RequestScoped
public class ValidatorCandidato {
	
	@Inject
	private Validator validador;
	
	@Inject
	private CandidatoDao daoCandidato;
	
	@Inject
	private UsuarioLogado usuarioLogado;

	public ValidatorCandidato() {
		
		
	
	}
	
		
	public Validator validaCandidato(Candidato candidato){
		this.validador.validate(candidato);
		
		if(daoCandidato.pesquisaCandidatoComLogin(candidato.getUsuario().getLogin()) != null){
			this.validador.add(new I18nMessage("login", "ja.existe"));
		}
		
		
		
		return validador;
	}
	
	public Validator validaCandidatoEdicao(Candidato candidato){
		this.validador.validate(candidato);
		
			if(!candidato.getUsuario().getLogin().equalsIgnoreCase(usuarioLogado.getUsuario().getLogin())){

				this.validador.add(new I18nMessage("login", "alteracao"));
				
				
			}
			
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}


}

