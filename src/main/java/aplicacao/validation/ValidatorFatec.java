package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.curso.Fatec;

@RequestScoped
public class ValidatorFatec {
	
	@Inject
	private Validator validador;


	
	public ValidatorFatec() {
		
		
	
	}
	
	
	
	public Validator validaFatec(Fatec fatec){
		this.validador.validate(fatec);
		
		/*if(fatec.getNomeFatec().equalsIgnoreCase("")||fatec.getNomeFatec()==null){
			validador.add(new I18nMessage("Nome de Fatec", "campo.obrigatorio"));
		}
		*/
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}


}


