package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.ProvasDoCandidato.QuestoesProva;

@RequestScoped
public class ValidatorQuestoesProva {
	
	@Inject
	private Validator validador;


	public ValidatorQuestoesProva() {
		
		
	
	}
	
	
	
	public Validator validaQuestoesProva(QuestoesProva questoesProva){
		this.validador.validate(questoesProva);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

