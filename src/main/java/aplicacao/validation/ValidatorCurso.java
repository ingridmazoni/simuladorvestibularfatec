package aplicacao.validation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.curso.Curso;

@RequestScoped
public class ValidatorCurso {
	
	@Inject
	private Validator validador;


	public ValidatorCurso() {
		
		
	
	}
	
	
	
	public Validator validaCurso(Curso curso){
		this.validador.validate(curso);
		
		if(curso.getEixoTecnologico().getId()==0){
			
			this.validador.add(new I18nMessage("eixo tecnológico", "campo.vazio"));
			
		}
		
		if(curso.getMateriasPesoDois()!= null && curso.getMateriasPesoDois().size()!= 2){
			this.validador.add(new I18nMessage("matérias peso dois", "selecione apenas duas matérias para este curso"));
		}
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}


