package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.curso.EixoTecnologico;

@RequestScoped
public class ValidatorEixoTecnologico {
	
	@Inject
	private Validator validador;


	public ValidatorEixoTecnologico() {
		
		
	
	}
	
	
	
	public Validator validaEixoTecnologico(EixoTecnologico eixoTecnologico){
		this.validador.validate(eixoTecnologico);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}


