package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.questaoVestibular.ConteudoProgramatico;

@RequestScoped
public class ValidatorConteudoProgramatico {
	
	@Inject
	private Validator validador;


	public ValidatorConteudoProgramatico() {
		
			
	}
	
	
	
	public Validator validaConteudoProgramatico(ConteudoProgramatico conteudoProgramatico){
		this.validador.validate(conteudoProgramatico);
		
		
		if(conteudoProgramatico.getDisciplina().getId()==0 || conteudoProgramatico.getDisciplina() == null){
			this.validador.add(new I18nMessage("disciplina", "campo.vazio"));
			
		}
			
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}


