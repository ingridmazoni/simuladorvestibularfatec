package aplicacao.validation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.questaoVestibular.QuestaoVestibular;

@RequestScoped
public class ValidatorQuestaoVestibular {
	
	@Inject
	private Validator validador;


	public ValidatorQuestaoVestibular() {
		
		
	
	}
	
	
	
	public Validator validaQuestaoVestibular(QuestaoVestibular questaoVestibular){
		this.validador.validate(questaoVestibular);
		
		if(questaoVestibular.getConteudoProgramatico().getId()==0){
			
			this.validador.add(new I18nMessage("conteudo programático", "campo.vazio"));
			
		}
		
		if(questaoVestibular.getPergunta()==null && questaoVestibular.getImagemPergunta()==null){
			
			this.validador.add(new I18nMessage("Pergunta", "campo.vazio"));
			
		}
		
		if(questaoVestibular.getAlternativaA()==null && questaoVestibular.getImagemAlternativaA()==null){
			
			this.validador.add(new I18nMessage("Alternativa A", "campo.vazio"));
			
		}
		
		if(questaoVestibular.getAlternativaB()==null && questaoVestibular.getImagemAlternativaB()==null){
					
					this.validador.add(new I18nMessage("Alternativa B", "campo.vazio"));
					
		}
		if(questaoVestibular.getAlternativaC()==null && questaoVestibular.getImagemAlternativaC()==null){
			
			this.validador.add(new I18nMessage("Alternativa C", "campo.vazio"));
			
		}
		if(questaoVestibular.getAlternativaD()==null && questaoVestibular.getImagemAlternativaD()==null){
			
			this.validador.add(new I18nMessage("Alternativa D", "campo.vazio"));
			
		}
		if(questaoVestibular.getAlternativaE()==null && questaoVestibular.getImagemAlternativaE()==null){
			
			this.validador.add(new I18nMessage("Alternativa E", "campo.vazio"));
			
		}
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

