package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.candidato.EscolhaDeCursoDoCandidato;

@RequestScoped
public class ValidatorEscolhaDeCursoDoCandidato {
	
	@Inject
	private Validator validador;


	public ValidatorEscolhaDeCursoDoCandidato() {
		
		
	
	}
	
	
	
	public Validator validaEscolhaDeCursoDoCandidato(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato){
		this.validador.validate(escolhaDeCursoDoCandidato);
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

