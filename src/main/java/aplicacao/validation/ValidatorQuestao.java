package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.questionarioSocioeconomico.Questao;

@RequestScoped
public class ValidatorQuestao {
	
	@Inject
	private Validator validador;


	public ValidatorQuestao() {
		
		
	
	}
	
	
	
	public Validator validaQuestao(Questao questao){
		this.validador.validate(questao);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

