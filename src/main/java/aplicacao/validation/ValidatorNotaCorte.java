package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Periodo;

@RequestScoped
public class ValidatorNotaCorte {
	
	@Inject
	private Validator validador;


	public ValidatorNotaCorte() {
		
	
	
	}
	
	
	
	public Validator validaNotaCorte(NotaCorte notaCorte){
		validador.validate(notaCorte);
		
		
		
		return validador;
	}
	
	
	public Validator validaNotaCorte2(NotaCorte notaCorte){
		

		if(notaCorte.getTurma().getFatec().getId()==0){
			this.validador.add(new I18nMessage("Fatec", "busca.notaCorte"));
		}
		
		if(notaCorte.getTurma().getCurso().getId()==0){
			this.validador.add(new I18nMessage("Curso", "busca.notaCorte"));
		}
		
		if(notaCorte.getTurma().getPeriodo()== null){
			this.validador.add(new I18nMessage("Horário", "busca.notaCorte"));
		}
		
		if(notaCorte.getSemestre()== null){
			this.validador.add(new I18nMessage("Semestre", "busca.notaCorte"));
		}
		
		if(notaCorte.getAno()==null){
			this.validador.add(new I18nMessage("Ano", "busca.notaCorte"));
		}
		
		return validador;
		
	}
	
	
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

