package aplicacao.validation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.curso.Curso;
import dominio.entity.curso.Fatec;

@RequestScoped
public class ValidatorNotaCandidato {
	
	@Inject
	private Validator validador;


	
	public ValidatorNotaCandidato() {
		
		
	
	}
	
	
	
	public Validator validaNotaCandidato(RealizacaoProva realizacaoProva,Curso curso){
		validador.validate(realizacaoProva);
		
		if(realizacaoProva.getId()==0){
			this.validador.add(new I18nMessage("Data de Realização da Prova","calculo.notaProva"));
		}
		
		if(curso.getId()==0){
			this.validador.add(new I18nMessage("Curso","calculo.notaProva"));
		}
		
		//if( realizacaoProva.getNotaRedacao().doubleValue()<0 || realizacaoProva.getNotaRedacao().doubleValue()>100){
			//this.validador.add(new I18nMessage("Nota Redação","notaRedacao"));
		//}
		
		
		return this.validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return this.validador.onErrorRedirectTo(controller);
	}


}



