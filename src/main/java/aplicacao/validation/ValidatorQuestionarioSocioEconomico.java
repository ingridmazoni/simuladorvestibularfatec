package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.questionarioSocioeconomico.QuestionarioSocioeconomico;

@RequestScoped
public class ValidatorQuestionarioSocioEconomico {
	
	@Inject
	private Validator validador;


	public ValidatorQuestionarioSocioEconomico() {
		
	}
	
	
	
	public Validator validaQuestionarioSocioEconomico(QuestionarioSocioeconomico questionarioSocioEconomico){
		this.validador.validate(questionarioSocioEconomico);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}


