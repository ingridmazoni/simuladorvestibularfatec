package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;

@RequestScoped
public class ValidatorRealizacaoProva {
	
	@Inject
	private Validator validador;


	public ValidatorRealizacaoProva(Validator validator) {
		
		this.validador=validator;
	
	}
	
	
	
	public Validator validaRealizacaoProva(RealizacaoProva realizacaoProva){
		this.validador.validate(realizacaoProva);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

