package aplicacao.validation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.I18nMessage;
import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.curso.Turma;

@RequestScoped
public class ValidatorTurma {
	
	@Inject
	private Validator validador;


	public ValidatorTurma() {
		
	
	
	}
	
	
	
	public Validator validaTurma(Turma turma){
		
		this.validador.validate(turma);
		
		if(turma.getFatec().getId()==0 || turma.getFatec() == null){
			this.validador.add(new I18nMessage("fatec", "campo.vazio"));
			
		}
		

		if(turma.getCurso().getId()==0 || turma.getCurso() == null){
			this.validador.add(new I18nMessage("curso", "campo.vazio"));
			
		}
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}


