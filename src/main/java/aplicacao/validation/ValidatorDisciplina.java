package aplicacao.validation;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.validator.Validator;
import dominio.entity.questaoVestibular.Disciplina;

@RequestScoped
public class ValidatorDisciplina {
	
	@Inject
	private Validator validador;


	public ValidatorDisciplina() {
		
		
	
	}
	
	
	
	public Validator validaDisciplina(Disciplina disciplina){
		this.validador.validate(disciplina);
		
		
		
		
		
		return validador;
	}
	
	public <T> T onErrorRedirectTo(T controller) {
		return validador.onErrorRedirectTo(controller);
	}

}

