package servicosTecnicos;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Specializes;

import br.com.caelum.vraptor.observer.upload.DefaultMultipartConfig;

@Specializes
@ApplicationScoped
public class CustomMultipartConfig extends DefaultMultipartConfig {

    // alteramos o tamanho total do upload para 50MB
    public long getSizeLimit() {
        return 1024 * 1024 * 1024;
    }

    // alteramos o tamanho do upload de cada arquivo para 20MB
    public long getFileSizeLimit() {
        return 1024 * 1024 * 1024;
    }
}