package servicosTecnicos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import dominio.entity.entidadesUtilitarias.Usuario;
import dominio.entity.ProvasDoCandidato.QuestoesProva;

@SessionScoped
@Named("usuarioLogado")
public class UsuarioLogado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Usuario usuario;
	private ArrayList<QuestoesProva> listaQuestoes;
	
		
	public void loga(Usuario user) {
		usuario = user;
		listaQuestoes =new ArrayList<QuestoesProva>();
	}
	
	
	public boolean isLogado() {
		return usuario != null;
	}
		
	public Usuario getUsuario() {
		return usuario;
		
	}
				
	public void desloga() {
		usuario = null;
		listaQuestoes=null;
	}
	
	public ArrayList<QuestoesProva> getListaQuestoes() {
		return listaQuestoes;
	}


		
}
