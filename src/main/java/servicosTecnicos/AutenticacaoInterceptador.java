package servicosTecnicos;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import aplicacao.control.HomeController;
import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;


@Intercepts
public class AutenticacaoInterceptador {

	private UsuarioLogado usuario;
	private Result result;
	private HttpServletRequest request;
	
	@Inject
	public AutenticacaoInterceptador(UsuarioLogado usuario,Result result,HttpServletRequest request) {
		this.usuario = usuario;
		this.result = result;
		this.request = request;
	}
	@Deprecated AutenticacaoInterceptador() {}
	
	@AroundCall
	public void autentica(SimpleInterceptorStack stack) {
	    System.out.println("Interceptando " + request.getRequestURI());
			if (usuario.isLogado() || request.getRequestURI().equals("/simuladorVestibularFatec/candidatoNovo")|| request.getRequestURI().equals("/simuladorVestibularFatec/candidato/salva")) {
				stack.next();
			
			} else {
				result.redirectTo(HomeController.class).formulario();
			}
	}
	
	
	@Accepts
	public boolean ehRestrito(ControllerMethod method) {
	return !method.getController().getType().equals(HomeController.class);
	}

}