package servicosTecnicos.daoImplementation;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class FactoryEntityManager {
	
	
	public static EntityManager getEntityManager(){
		EntityManagerFactory factory= Persistence.createEntityManagerFactory("default");
		 
		return factory.createEntityManager();
		
	}
	
	public static void main(String args[]){
		
		EntityManager manager= getEntityManager();
		manager.close();
	}

}
