package servicosTecnicos.daoImplementation;



import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import dominio.entity.curso.Fatec;
import servicosTecnicos.dao.RealizacaoProvaDao;
@RequestScoped
public class RealizacaoProvaDaoImplementation implements RealizacaoProvaDao{
	
private final EntityManager manager;
	
	@Inject
	public RealizacaoProvaDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(RealizacaoProva realizacaoProva) {
		manager.persist(realizacaoProva);
		manager.flush();
	}

	public void deleta(RealizacaoProva realizacaoProva) {
		manager.remove(manager.getReference(RealizacaoProva.class, realizacaoProva.getId()));
		
	}

	public void atualiza(RealizacaoProva realizacaoProva) {
		manager.merge(realizacaoProva);
		
	}
	

	public RealizacaoProva pesquisaRealizacaoProvaComId(int id) {
		return manager.find(RealizacaoProva.class, id);
	}

	
public List<RealizacaoProva> pesquisaRealizacaoProva(Candidato candidato) {
		
		return manager.createQuery("SELECT rp FROM RealizacaoProva rp where rp.candidato.id =:candidato", RealizacaoProva.class)
				.setParameter("candidato", candidato.getId())	
				.getResultList();
			
		
	}


public List<Integer> pesquisaDataProva(Candidato candidato) {
	
	return manager.createQuery("SELECT distinct(year(rp.dataRealizacao)) FROM RealizacaoProva rp where rp.candidato.id =:candidato", Integer.class)
			.setParameter("candidato", candidato.getId())	
			.getResultList();
		
	
}

public List<RealizacaoProva> pesquisaProvasCandidato(Candidato candidato,String semestre,Integer data) {
			
	int mesInicio,mesFim;
	
	if(semestre.equals("1")){
		mesInicio = 0;
		mesFim = 6;
	}
	else{
		mesInicio = 7;
		mesFim = 12;
	}
	
	
	return manager.createQuery("SELECT rp FROM RealizacaoProva rp where rp.candidato.id =:candidato and year(rp.dataRealizacao) =:ano and month(rp.dataRealizacao) between :mesInicio and :mesFim", RealizacaoProva.class)
			.setParameter("candidato", candidato.getId())
			.setParameter("ano", data)
			.setParameter("mesInicio", mesInicio)
			.setParameter("mesFim", mesFim)
			.getResultList();
		
	
}

public List<RealizacaoProva> pesquisaRealizaçãoProvaPorCandidato(List<Candidato> candidatoList,Date dataInicio, Date dataFim) {
	return manager.createQuery("SELECT rp FROM RealizacaoProva rp where (rp.candidato IN :candidatoList) and (rp.dataRealizacao between :dataInicio and :dataFim)", RealizacaoProva.class)
			.setParameter("candidatoList", candidatoList)
			.setParameter("dataInicio", dataInicio)
			.setParameter("dataFim", dataFim)
			.getResultList();
}

public List<Candidato> pesquisaCandidatos() {
	return manager.createQuery("SELECT distinct rp.candidato FROM RealizacaoProva rp", Candidato.class)
		.getResultList();
}


}
