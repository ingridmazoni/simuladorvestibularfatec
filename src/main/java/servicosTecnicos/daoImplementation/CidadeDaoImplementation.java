package servicosTecnicos.daoImplementation;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.entidadesUtilitarias.Cidades;
import servicosTecnicos.dao.CidadeDao;

public class CidadeDaoImplementation implements CidadeDao{

@RequestScoped	
private final EntityManager manager;
	
	@Inject
	public CidadeDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public List<Cidades> pesquisaCidades() {
		return manager.createQuery("select c from Cidades c order by c.nome", Cidades.class).getResultList();
	}
	
	

}
