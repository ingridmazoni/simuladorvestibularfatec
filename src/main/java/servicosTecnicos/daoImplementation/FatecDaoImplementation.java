package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import dominio.entity.curso.Curso;
import dominio.entity.curso.Fatec;
import servicosTecnicos.dao.FatecDao;

@RequestScoped
public class FatecDaoImplementation implements FatecDao{
	
private final EntityManager manager;
	
	@Inject
	public FatecDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Fatec fatec) {
		manager.persist(fatec);
		
		
	}

	public void deleta(Fatec fatec) throws PersistenceException {
		manager.remove(manager.getReference(Fatec.class, fatec.getId()));
		
	}

	public void atualiza(Fatec fatec) {
		manager.merge(fatec);
		
	}

	public Fatec pesquisaFatecComId(int id) {
		return manager.find(Fatec.class, id);
	}

	public List<Fatec> pesquisaFatec() {
		return manager.createQuery("select f from Fatec f order by f.nome", Fatec.class).getResultList();
	}
	
	
public List<Fatec> pesquisaFatec(Fatec fatec) {
		
		return manager.createQuery("SELECT f FROM Fatec f WHERE f.cidade.id = :cidade OR f.nome LIKE :nome ORDER BY f.nome", Fatec.class)
			.setParameter("cidade", fatec.getCidade().getId())
			.setParameter("nome", "%" + fatec.getNome() + "%")
			.getResultList();
			
		
	}

	public List<Fatec> pesquisaTodosAsFatecs() {
		return manager.createQuery("SELECT f FROM Fatec f ORDER BY f.nome", Fatec.class)
				.getResultList();
	}

}
