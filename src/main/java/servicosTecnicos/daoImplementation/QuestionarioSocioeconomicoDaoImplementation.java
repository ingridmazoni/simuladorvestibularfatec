package servicosTecnicos.daoImplementation;



import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.questionarioSocioeconomico.QuestionarioSocioeconomico;
import servicosTecnicos.dao.QuestionarioSocioeconomicoDao;
@RequestScoped
public class QuestionarioSocioeconomicoDaoImplementation implements QuestionarioSocioeconomicoDao {
	
private final EntityManager manager;
	
	@Inject
	public QuestionarioSocioeconomicoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(QuestionarioSocioeconomico questionarioSocioeconomico) {
		manager.persist(questionarioSocioeconomico);
		
	}

	public void deleta(QuestionarioSocioeconomico questionarioSocioeconomico) {
		manager.remove(manager.getReference(QuestionarioSocioeconomico.class, questionarioSocioeconomico.getId()));
		
	}

	public void atualiza(QuestionarioSocioeconomico questionarioSocioeconomico) {
		manager.merge(questionarioSocioeconomico);
		
	}
	

	public QuestionarioSocioeconomico pesquisaQuestionarioSocioEconomicoComId(int id) {
		return manager.find(QuestionarioSocioeconomico.class, id);
	}


}
