package servicosTecnicos.daoImplementation;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import dominio.entity.questaoVestibular.ConteudoProgramatico;
import servicosTecnicos.dao.ConteudoProgramaticoDao;

@RequestScoped
public class ConteudoProgramaticoDaoImplementation implements ConteudoProgramaticoDao{
	
private final EntityManager manager;
	
	@Inject
	public ConteudoProgramaticoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(ConteudoProgramatico conteudoProgramatico) {
		manager.persist(conteudoProgramatico);
		
	}

	public void deleta(ConteudoProgramatico conteudoProgramatico) throws PersistenceException{
		manager.remove(manager.getReference(ConteudoProgramatico.class,conteudoProgramatico.getId()));
		
	}

	public void atualiza(ConteudoProgramatico conteudoProgramatico) {
		manager.merge(conteudoProgramatico);
		
	}
	

	public ConteudoProgramatico pesquisaConteudoProgramaticoComId(int id) {
		return manager.find(ConteudoProgramatico.class, id);
	}
	
	public List<ConteudoProgramatico> pesquisaConteudoProgramatico() {
		return manager.createQuery("SELECT c FROM ConteudoProgramatico c ORDER BY c.disciplina.nome,c.programaProva", ConteudoProgramatico.class).getResultList();
	}

	public List<ConteudoProgramatico> pesquisaConteudoProgramatico(ConteudoProgramatico conteudoProgramatico) {
		
		return manager.createQuery("SELECT c FROM ConteudoProgramatico c WHERE c.disciplina.id = :disciplina OR c.programaProva LIKE :cp ORDER BY c.programaProva", ConteudoProgramatico.class)
			.setParameter("disciplina", conteudoProgramatico.getDisciplina().getId())
			.setParameter("cp", "%" + conteudoProgramatico.getProgramaProva() + "%")
			.getResultList();
			
		
	}

}
