package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.curso.EixoTecnologico;
import servicosTecnicos.dao.EixoTecnologicoDao;
@RequestScoped
public class EixoTecnologicoDaoImplementation implements EixoTecnologicoDao{
	
private final EntityManager manager;
	
	@Inject
	public EixoTecnologicoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}
	

	public void insere(EixoTecnologico eixoTecnologico) {
		manager.persist(eixoTecnologico);
		
	}

	public void deleta(EixoTecnologico eixoTecnologico) {
		manager.remove(manager.getReference(EixoTecnologico.class, eixoTecnologico.getId()));
		
	}

	public void atualiza(EixoTecnologico eixoTecnologico) {
		manager.merge(eixoTecnologico);
		
	}
	

	public EixoTecnologico pesquisaEixoTecnologicoComId(int id) {
		return manager.find(EixoTecnologico.class, id);
	}


	public List<EixoTecnologico> pesquisaEixoTecnologico() {
		
		return manager.createQuery("select e from EixoTecnologico e order by e.nome", EixoTecnologico.class).getResultList();
	}


}
