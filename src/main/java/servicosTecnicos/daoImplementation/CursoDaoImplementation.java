package servicosTecnicos.daoImplementation;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import dominio.entity.curso.Curso;
import servicosTecnicos.dao.CursoDao;
@RequestScoped
public class CursoDaoImplementation implements CursoDao{
	
	private final EntityManager manager;
	
	@Inject
	public CursoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Curso curso) {
		manager.persist(curso);
		
	}

	public void deleta(Curso curso) throws PersistenceException {
		manager.remove(manager.getReference(Curso.class, curso.getId()));
		
	}

	public void atualiza(Curso curso) {
		manager.merge(curso);
		
	}
	

	public Curso pesquisaCursoComId(int id) {
		return manager.find(Curso.class, id);
	}

	public List<Curso> pesquisaCursos() {
		return manager.createQuery("select c from Curso c order by c.nome", Curso.class).getResultList();
	}

	public List<Curso> pesquisaCurso(Curso c) {
	
		return manager.createQuery("SELECT c FROM Curso c WHERE c.eixoTecnologico.id = :eixoTecnologico OR c.nome LIKE :nome ORDER BY c.nome", Curso.class)
				.setParameter("eixoTecnologico",c.getEixoTecnologico().getId() )
				.setParameter("nome", "%" + c.getNome() + "%")
				.getResultList();
				
	}

	public List<Curso> pesquisaTodosOsCurso() {
		return manager.createQuery("SELECT c FROM Curso c ORDER BY c.nome", Curso.class)
				.getResultList();
	}


}
