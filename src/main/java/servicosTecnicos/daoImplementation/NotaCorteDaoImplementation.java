package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import dominio.entity.curso.NotaCorte;
import servicosTecnicos.dao.NotaCorteDao;
@RequestScoped
public class NotaCorteDaoImplementation implements NotaCorteDao {
	
private final EntityManager manager;
	
	@Inject
	public NotaCorteDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(NotaCorte notaCorte) {
		manager.persist(notaCorte);
	}

	public void deleta(NotaCorte notaCorte) throws PersistenceException {
		manager.remove(manager.getReference(NotaCorte.class, notaCorte.getId()));
		
	}

	public void atualiza(NotaCorte notaCorte) {
		manager.merge(notaCorte);
		
	}


	public NotaCorte pesquisaNotaCorteComId(int id) {
		return manager.find(NotaCorte.class, id);
	}

	public List<NotaCorte> pesquisaNotaCorte(NotaCorte notaCorte) {
		
		return manager.createQuery("SELECT n FROM NotaCorte n WHERE n.turma.curso.id =:curso OR n.turma.fatec.id =:fatec OR n.turma.periodo =:periodo OR n.ano =:ano OR n.semestre =:semestre", NotaCorte.class)
				.setParameter("curso", notaCorte.getTurma().getCurso().getId())
				.setParameter("fatec",  notaCorte.getTurma().getFatec().getId())
				.setParameter("periodo",  notaCorte.getTurma().getPeriodo())
				.setParameter("ano",  notaCorte.getAno())
				.setParameter("semestre",  notaCorte.getSemestre())
				.getResultList();
	}
	
	
public List<NotaCorte> pesquisaNotaCorte2(NotaCorte notaCorte) {
		
		return manager.createQuery("SELECT n FROM NotaCorte n WHERE n.turma.curso.id =:curso And n.turma.fatec.id =:fatec And n.turma.periodo =:periodo And n.ano =:ano And n.semestre =:semestre", NotaCorte.class)
				.setParameter("curso", notaCorte.getTurma().getCurso().getId())
				.setParameter("fatec",  notaCorte.getTurma().getFatec().getId())
				.setParameter("periodo",  notaCorte.getTurma().getPeriodo())
				.setParameter("ano",  notaCorte.getAno())
				.setParameter("semestre",  notaCorte.getSemestre())
				.getResultList();
	}
	
public List<Integer> pesquisaAnosVestibular() {
	
	return manager.createQuery("SELECT distinct n.ano FROM NotaCorte n order by n.ano", Integer.class).getResultList();
			
}

public NotaCorte pesquisaNotaCorte3(NotaCorte notaCorte) {
	NotaCorte nc;
	try{
		nc= manager.createQuery("SELECT n FROM NotaCorte n WHERE n.turma.curso.id =:curso And n.turma.fatec.id =:fatec And n.turma.periodo =:periodo And n.ano =:ano And n.semestre =:semestre", NotaCorte.class)
		.setParameter("curso", notaCorte.getTurma().getCurso().getId())
		.setParameter("fatec",  notaCorte.getTurma().getFatec().getId())
		.setParameter("periodo",  notaCorte.getTurma().getPeriodo())
		.setParameter("ano",  notaCorte.getAno())
		.setParameter("semestre",  notaCorte.getSemestre())
		.getSingleResult();
	}
	catch(NoResultException erro){
		nc= null;
	}
	
	return nc;
}

}
