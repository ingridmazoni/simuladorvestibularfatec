package servicosTecnicos.daoImplementation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import dominio.entity.entidadesUtilitarias.Usuario;
import servicosTecnicos.dao.UsuarioDao;

@RequestScoped
public class UsuarioDaoImplementation implements UsuarioDao{
	
	private final EntityManager manager;
	
	@Inject
	public UsuarioDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}


	public Usuario validaUsuarioComLoginESenha(String login, String senha) {
		Usuario usuario;
		try{
			usuario= manager.createQuery("SELECT u FROM Usuario u WHERE u.login =:login AND u.senha =:senha", Usuario.class)
					.setParameter("login", login )
					.setParameter("senha",senha)
					.getSingleResult();
		}
		catch(NoResultException erro){
			usuario = null;
		}
		return usuario;
	}

	

}
