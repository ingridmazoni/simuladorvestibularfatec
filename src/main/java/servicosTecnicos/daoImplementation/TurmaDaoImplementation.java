package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import dominio.entity.curso.Fatec;
import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Turma;
import servicosTecnicos.dao.TurmaDao;

@RequestScoped
public class TurmaDaoImplementation implements TurmaDao {
	
private final EntityManager manager;
	
	@Inject
	public TurmaDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Turma turma) {
		manager.persist(turma);
		
	}

	public void deleta(Turma turma) throws PersistenceException{
	manager.remove(manager.getReference(Turma.class, turma.getId()));
		
	}

	public void atualiza(Turma turma) {
		manager.merge(turma);
		
	}


	public Turma pesquisaTurmaComId(int id) {
		return manager.find(Turma.class, id);
	}

	
	public Turma pesquisaTurma(NotaCorte notaCorte) {
			
			return manager.createQuery("SELECT t FROM Turma t WHERE t.curso.id =:curso AND t.fatec.id =:fatec AND t.periodo =:periodo", Turma.class)
				.setParameter("curso", notaCorte.getTurma().getCurso().getId())
				.setParameter("fatec",  notaCorte.getTurma().getFatec().getId())
				.setParameter("periodo",  notaCorte.getTurma().getPeriodo())
				.getSingleResult();
				
			
		}

		public List<Turma> pesquisaTurma(Turma turma) {
			return manager.createQuery("SELECT t FROM Turma t WHERE t.curso.id =:curso OR t.fatec.id =:fatec OR t.periodo =:periodo", Turma.class)
					.setParameter("curso", turma.getCurso().getId())
					.setParameter("fatec",  turma.getFatec().getId())
					.setParameter("periodo",  turma.getPeriodo())
					.getResultList();
		}

		public List<Turma> pesquisaTodasAsTurmas(Turma turma) {
			return manager.createQuery("SELECT t FROM Turma t", Turma.class)
					.getResultList();
		}
		
		
		public Turma pesquisaTurma2(Turma turma) {
			Turma t;
			try{
				t= manager.createQuery("SELECT t FROM Turma t WHERE t.curso.id =:curso AND t.fatec.id =:fatec AND t.periodo =:periodo", Turma.class)
						.setParameter("curso", turma.getCurso().getId())
						.setParameter("fatec",  turma.getFatec().getId())
						.setParameter("periodo",  turma.getPeriodo())
						.getSingleResult();
			}
			catch(NoResultException erro){
				t=null;
			}
					
			return t;
		}

}
