package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.ProvasDoCandidato.QuestoesProva;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import dominio.entity.questaoVestibular.Disciplina;
import servicosTecnicos.dao.QuestoesProvaDao;
@RequestScoped
public class QuestoesProvaDaoImplementation implements QuestoesProvaDao {
	
private final EntityManager manager;
	
	@Inject
	public QuestoesProvaDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(QuestoesProva questoesProva) {
		manager.persist(questoesProva);
		
	}
		
	public void deleta(QuestoesProva questoesProva) {
		manager.remove(manager.getReference(QuestoesProva.class, questoesProva.getId()));
		
	}

	public void atualiza(QuestoesProva questoesProva) {
		manager.merge(questoesProva);
		
	}
	
	
	public QuestoesProva pesquisaQuestoesProva(QuestoesProva questoesProva) {
		
		return manager.createQuery("select qp from QuestoesProva qp where qp.realizacaoProva.id =:id and qp.sequenciaDaQuestaoNaProva =:sequencia", QuestoesProva.class)
				.setParameter("id", questoesProva.getRealizacaoProva().getId())
				.setParameter("sequencia",  questoesProva.getSequenciaDaQuestaoNaProva())
				.getSingleResult();
				
	}

	public QuestoesProva pesquisaQuestoesProvaComId(int id) {
		return manager.find(QuestoesProva.class, id);
	}
	

	public List<QuestoesProva> pesquisaListaQuestoesProva(QuestoesProva questoesProva) {
		
		return manager.createQuery("select qp from QuestoesProva qp where qp.realizacaoProva.id =:id", QuestoesProva.class)
		.setParameter("id", questoesProva.getRealizacaoProva().getId())		
		.getResultList();
	}
	
	
public Long pesquisaQuantidadeQuestoesProva(RealizacaoProva rp, Disciplina d) {
		
		return manager.createQuery("select count(qp) from QuestoesProva qp where qp.realizacaoProva.id =:id "
				+ "and qp.questaoVestibular.conteudoProgramatico.disciplina.id =:disciplina and "
				+ "qp.respostaDoCandidato = qp.questaoVestibular.respostaCorreta", Long.class)
		.setParameter("id", rp.getId())
		.setParameter("disciplina", d.getId())		
		.getSingleResult();
	}


public List<QuestoesProva> pesquisaProvaPorCandidato(Candidato candidato) {
	
	return manager.createQuery("SELECT qp FROM QuestoesProva qp where qp.realizacaoProva.candidato=:candidato", QuestoesProva.class)
			.setParameter("candidato", candidato.getId())	
			.getResultList();
		
	
}


public Long pesquisaQuantidadeTotalQuestoesProva(RealizacaoProva rp) {
	
	return manager.createQuery("select count(qp) from QuestoesProva qp where qp.realizacaoProva.id =:id "
			+ " and qp.respostaDoCandidato = qp.questaoVestibular.respostaCorreta", Long.class)
	.setParameter("id", rp.getId())
	.getSingleResult();
}

}
