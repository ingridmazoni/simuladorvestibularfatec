package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import dominio.entity.questaoVestibular.QuestaoVestibular;
import servicosTecnicos.dao.QuestaoVestibularDao;
@RequestScoped
public class QuestaoVestibularDaoImplementation implements QuestaoVestibularDao{
	
private final EntityManager manager;
	
	@Inject
	public QuestaoVestibularDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(QuestaoVestibular questaoVestibular) {
		manager.persist(questaoVestibular);
		
	}

	public void deleta(QuestaoVestibular questaoVestibular) throws PersistenceException {
		manager.remove(manager.getReference(QuestaoVestibular.class, questaoVestibular.getId()));
		
	}

	public void atualiza(QuestaoVestibular questaoVestibular) {
		manager.merge(questaoVestibular);
		
	}
	

	public QuestaoVestibular pesquisaQuestaoVestibularComId(int id) {
		return manager.find(QuestaoVestibular.class, id);
	}

	public List<QuestaoVestibular> pesquisaQuestaoVestibular(QuestaoVestibular questaoVestibular) {
		
		return manager.createQuery("SELECT q FROM QuestaoVestibular q WHERE q.conteudoProgramatico.id =:conteudoProgramatico OR q.conteudoProgramatico.disciplina.id =:disciplina OR q.anoVestibular =:ano OR q.semestreVestibular =:semestre", QuestaoVestibular.class)
				.setParameter("conteudoProgramatico", questaoVestibular.getConteudoProgramatico().getId() )
				.setParameter("disciplina", questaoVestibular.getConteudoProgramatico().getDisciplina().getId())
				.setParameter("ano", questaoVestibular.getAnoVestibular())
				.setParameter("semestre", questaoVestibular.getSemestreVestibular())
				.getResultList();
				
	}

public QuestaoVestibular pesquisaQuestao(String disciplina) {
		
		return manager.createQuery("SELECT q FROM QuestaoVestibular q WHERE q.conteudoProgramatico.disciplina.nome =:disciplina order by rand()", QuestaoVestibular.class)
				.setParameter("disciplina",disciplina)
				.setMaxResults(1).getSingleResult();
			
				
				
	}


public List<Integer> pesquisaAnosVestibular() {
	
	return manager.createQuery("SELECT distinct q.anoVestibular FROM QuestaoVestibular q order by q.anoVestibular", Integer.class).getResultList();
			
}


public List<QuestaoVestibular> pesquisaQuestoes(String disciplina,int numeroResultados) {
	
	return manager.createQuery("SELECT distinct q FROM QuestaoVestibular q WHERE q.conteudoProgramatico.disciplina.nome =:disciplina order by rand()", QuestaoVestibular.class)
			.setParameter("disciplina",disciplina)
			.setMaxResults(numeroResultados).getResultList();
		
			
			
}


}
