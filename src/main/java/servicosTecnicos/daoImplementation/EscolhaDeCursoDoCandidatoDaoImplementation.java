package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import dominio.entity.candidato.Candidato;
import dominio.entity.candidato.EscolhaDeCursoDoCandidato;
import dominio.entity.curso.NotaCorte;
import servicosTecnicos.dao.EscolhaDeCursoDoCandidatoDao;
@RequestScoped
public class EscolhaDeCursoDoCandidatoDaoImplementation implements EscolhaDeCursoDoCandidatoDao{
	
private final EntityManager manager;
	
	@Inject
	public EscolhaDeCursoDoCandidatoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato) {
		manager.persist(escolhaDeCursoDoCandidato);
		
	}

	public void deleta(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato) {
		manager.remove(manager.getReference(EscolhaDeCursoDoCandidato.class, escolhaDeCursoDoCandidato.getId()));
		
	}

	public void atualiza(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato) {
		manager.merge(escolhaDeCursoDoCandidato);
		
	}
		
	public List<Candidato> listaDeCandidatos(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato){
		List<Candidato> lc=null;
					
		lc = manager.createQuery("SELECT distinct cc.candidato FROM EscolhaDeCursoDoCandidato cc WHERE cc.turma.curso.id =:curso And cc.turma.fatec.id =:fatec And cc.turma.periodo =:periodo And cc.opcaoCandidato =:escolhaDoCandidato", Candidato.class)
				.setParameter("curso", escolhaDeCursoDoCandidato.getTurma().getCurso().getId())
				.setParameter("fatec",  escolhaDeCursoDoCandidato.getTurma().getFatec().getId())
				.setParameter("periodo",  escolhaDeCursoDoCandidato.getTurma().getPeriodo())
				.setParameter("escolhaDoCandidato",  escolhaDeCursoDoCandidato.getOpcaoCandidato())
				.getResultList();
		
		return lc;
	}
	
	public List<Candidato> listaDeCandidatosOpcaoAdmin(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato){
		List<Candidato> lc=null;
		
		lc = manager.createQuery("SELECT distinct cc.candidato FROM EscolhaDeCursoDoCandidato cc WHERE cc.turma.curso.id =:curso And cc.turma.fatec.id =:fatec And cc.turma.periodo =:periodo", Candidato.class)
				.setParameter("curso", escolhaDeCursoDoCandidato.getTurma().getCurso().getId())
				.setParameter("fatec",  escolhaDeCursoDoCandidato.getTurma().getFatec().getId())
				.setParameter("periodo",  escolhaDeCursoDoCandidato.getTurma().getPeriodo())
				.getResultList();
		
		return lc;
	}

}
