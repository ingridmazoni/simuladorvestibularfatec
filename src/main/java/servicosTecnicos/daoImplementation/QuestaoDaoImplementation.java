package servicosTecnicos.daoImplementation;



import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.questionarioSocioeconomico.Questao;
import servicosTecnicos.dao.QuestaoDao;
@RequestScoped
public class QuestaoDaoImplementation implements QuestaoDao {
	
private final EntityManager manager;
	
	@Inject
	public QuestaoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Questao questao) {
		manager.persist(questao);
		
	}

	public void deleta(Questao questao) {
		manager.remove(manager.getReference(Questao.class, questao.getId()));
		
	}

	public void atualiza(Questao questao) {
		manager.merge(questao);
		
	}
	

	public Questao pesquisaQuestaoComId(int id) {
		return manager.find(Questao.class, id);
	}


}
