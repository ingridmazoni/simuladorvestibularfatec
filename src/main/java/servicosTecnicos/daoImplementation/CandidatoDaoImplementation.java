package servicosTecnicos.daoImplementation;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import dominio.entity.candidato.Candidato;
import dominio.entity.curso.Fatec;
import servicosTecnicos.dao.CandidatoDao;

@RequestScoped
public class CandidatoDaoImplementation implements CandidatoDao {
	
	private final EntityManager manager;
	
	@Inject
	public CandidatoDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Candidato candidato) {
		manager.persist(candidato);
		
	}

	public void deleta(Candidato candidato) {
		manager.remove(manager.getReference(Candidato.class, candidato.getId()));
		
	}

	public void atualiza(Candidato candidato) {
		manager.merge(candidato);
		
	}
	

	public Candidato pesquisaCandidatoComId(int id) {
		return manager.find(Candidato.class, id);
	}

	
	public Candidato pesquisaCandidatoComLogin(String login) {
		try{
			return manager.createQuery("SELECT c FROM Candidato c WHERE c.usuario.login = :login", Candidato.class)
					.setParameter("login", login).getSingleResult();
		}
		catch(NoResultException erro){
			return null;
		}
		
	}


}
