package servicosTecnicos.daoImplementation;



import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import dominio.entity.questaoVestibular.Disciplina;
import servicosTecnicos.dao.DisciplinaDao;
@RequestScoped
public class DisciplinaDaoImplementation implements DisciplinaDao {
	
private final EntityManager manager;
	
	@Inject
	public DisciplinaDaoImplementation(EntityManager manager) {
		this.manager=manager;
	}

	public void insere(Disciplina disciplina) {
		manager.persist(disciplina);
		
	}

	public void deleta(Disciplina disciplina) {
		manager.remove(manager.getReference(Disciplina.class, disciplina.getId()));
		
	}

	public void atualiza(Disciplina disciplina) {
		manager.merge(disciplina);
		
	}
	

	public Disciplina pesquisaDisciplinaComId(int id) {
		return manager.find(Disciplina.class, id);
	}

	public List<Disciplina> pesquisaDisciplina() {
		
		return manager.createQuery("select d from Disciplina d order by d.nome", Disciplina.class).getResultList();
	}


}
