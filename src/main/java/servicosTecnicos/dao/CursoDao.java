package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.curso.Curso;

public interface CursoDao {
	
	public void insere(Curso curso);
	public void deleta(Curso curso) throws PersistenceException;
	public void atualiza(Curso curso);
	public Curso pesquisaCursoComId(int id);
	public List<Curso> pesquisaCursos();
	public List<Curso> pesquisaCurso(Curso c);
	public List<Curso> pesquisaTodosOsCurso();
	

}
