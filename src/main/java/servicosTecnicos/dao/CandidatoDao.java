package servicosTecnicos.dao;

import dominio.entity.candidato.Candidato;

public interface CandidatoDao {
	public void insere(Candidato candidato);
	public void deleta(Candidato candidato);
	public void atualiza(Candidato candidato);
	public Candidato pesquisaCandidatoComId(int id);
	public Candidato pesquisaCandidatoComLogin(String login);
	
}
