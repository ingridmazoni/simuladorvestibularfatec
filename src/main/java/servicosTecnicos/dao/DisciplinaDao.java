package servicosTecnicos.dao;

import java.util.List;

import dominio.entity.questaoVestibular.Disciplina;

public interface DisciplinaDao {
	
	public void insere(Disciplina disciplina);
	public void deleta(Disciplina disciplina);
	public void atualiza(Disciplina disciplina);
	public Disciplina pesquisaDisciplinaComId(int id);
	public List<Disciplina> pesquisaDisciplina();
}
