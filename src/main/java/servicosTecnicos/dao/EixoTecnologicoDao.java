package servicosTecnicos.dao;

import java.util.List;

import dominio.entity.curso.EixoTecnologico;

public interface EixoTecnologicoDao {
	
	public void insere(EixoTecnologico eixoTecnologico);
	public void deleta(EixoTecnologico eixoTecnologico);
	public void atualiza(EixoTecnologico eixoTecnologico);
	public EixoTecnologico pesquisaEixoTecnologicoComId(int id);
	public List<EixoTecnologico> pesquisaEixoTecnologico();

}
