package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.curso.Fatec;


public interface FatecDao {
	
	public void insere(Fatec fatec);
	public void deleta(Fatec fatec)throws PersistenceException;
	public void atualiza(Fatec fatec);
	public Fatec pesquisaFatecComId(int id);
	public List<Fatec> pesquisaFatec();
	public List<Fatec> pesquisaFatec(Fatec fatec);
	public List<Fatec> pesquisaTodosAsFatecs();
	

}
