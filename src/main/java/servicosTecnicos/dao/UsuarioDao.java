package servicosTecnicos.dao;

import dominio.entity.entidadesUtilitarias.Usuario;

public interface UsuarioDao {
	
	public Usuario validaUsuarioComLoginESenha(String login,String senha);

}
