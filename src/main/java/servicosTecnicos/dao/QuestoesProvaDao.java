package servicosTecnicos.dao;

import java.util.List;

import dominio.entity.ProvasDoCandidato.QuestoesProva;
import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;
import dominio.entity.questaoVestibular.Disciplina;

public interface QuestoesProvaDao {
	
	public void insere(QuestoesProva questoesProva);
	public void deleta(QuestoesProva questoesProva);
	public void atualiza(QuestoesProva questoesProva);
	public QuestoesProva pesquisaQuestoesProvaComId(int id);
	public QuestoesProva pesquisaQuestoesProva(QuestoesProva questoesProva);
	public List<QuestoesProva> pesquisaListaQuestoesProva(QuestoesProva questoesProva);
	public Long pesquisaQuantidadeQuestoesProva(RealizacaoProva rp, Disciplina d);
	public List<QuestoesProva> pesquisaProvaPorCandidato(Candidato candidato);
	public Long pesquisaQuantidadeTotalQuestoesProva(RealizacaoProva rp);
	

}
