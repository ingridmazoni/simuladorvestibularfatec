package servicosTecnicos.dao;


import dominio.entity.questionarioSocioeconomico.QuestionarioSocioeconomico;

public interface QuestionarioSocioeconomicoDao {
	
	public void insere(QuestionarioSocioeconomico questionarioSocioeconomico);
	public void deleta(QuestionarioSocioeconomico questionarioSocioeconomico);
	public void atualiza(QuestionarioSocioeconomico questionarioSocioeconomico);
	public QuestionarioSocioeconomico pesquisaQuestionarioSocioEconomicoComId(int id);
	

}
