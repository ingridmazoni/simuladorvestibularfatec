package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.questaoVestibular.ConteudoProgramatico;

public interface ConteudoProgramaticoDao {
	
	public void insere(ConteudoProgramatico conteudoProgramatico);
	public void deleta(ConteudoProgramatico conteudoProgramatico) throws PersistenceException;
	public void atualiza(ConteudoProgramatico conteudoProgramatico);
	public ConteudoProgramatico pesquisaConteudoProgramaticoComId(int id);
	public List<ConteudoProgramatico> pesquisaConteudoProgramatico();
	public List<ConteudoProgramatico> pesquisaConteudoProgramatico(ConteudoProgramatico conteudoProgramatico);

}
