package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.RollbackException;

import dominio.entity.candidato.Candidato;
import dominio.entity.candidato.EscolhaDeCursoDoCandidato;

public interface EscolhaDeCursoDoCandidatoDao {

	public void insere(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato);
	public void deleta(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato);
	public void atualiza(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato);
	public List<Candidato> listaDeCandidatos(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato);
	public List<Candidato> listaDeCandidatosOpcaoAdmin(EscolhaDeCursoDoCandidato escolhaDeCursoDoCandidato);
	
	

}
