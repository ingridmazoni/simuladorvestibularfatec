package servicosTecnicos.dao;

import java.util.List;

import dominio.entity.entidadesUtilitarias.Cidades;

public interface CidadeDao {
	
	public List<Cidades> pesquisaCidades();
	

}
