package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.curso.NotaCorte;

public interface NotaCorteDao {
	public void insere(NotaCorte notaCorte);
	public void deleta(NotaCorte notaCorte) throws PersistenceException;
	public void atualiza(NotaCorte notaCorte);
	public NotaCorte pesquisaNotaCorteComId(int id);
	public List<NotaCorte> pesquisaNotaCorte(NotaCorte notaCorte);
	public List<NotaCorte> pesquisaNotaCorte2 (NotaCorte notaCorte);
	public List<Integer> pesquisaAnosVestibular();
	public NotaCorte pesquisaNotaCorte3(NotaCorte notaCorte);
	
}
