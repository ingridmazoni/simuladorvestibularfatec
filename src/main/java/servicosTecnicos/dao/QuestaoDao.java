package servicosTecnicos.dao;

import dominio.entity.questionarioSocioeconomico.Questao;

public interface QuestaoDao {
	
	public void insere(Questao questao);
	public void deleta(Questao questao);
	public void atualiza(Questao questao);
	public Questao pesquisaQuestaoComId(int id);
}
