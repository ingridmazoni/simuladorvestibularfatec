package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.questaoVestibular.QuestaoVestibular;


public interface QuestaoVestibularDao {
	
	public void insere(QuestaoVestibular questaoVestibular);
	public void deleta(QuestaoVestibular questaoVestibular)throws PersistenceException;
	public void atualiza(QuestaoVestibular questaoVestibular);
	public QuestaoVestibular pesquisaQuestaoVestibularComId(int id);
	public List<QuestaoVestibular> pesquisaQuestaoVestibular(QuestaoVestibular questaoVestibular);
	public QuestaoVestibular pesquisaQuestao(String disciplina);
	public List<Integer> pesquisaAnosVestibular();
	public List<QuestaoVestibular> pesquisaQuestoes(String disciplina,int numeroResultados);

}
