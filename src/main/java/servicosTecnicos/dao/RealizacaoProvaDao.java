package servicosTecnicos.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dominio.entity.ProvasDoCandidato.RealizacaoProva;
import dominio.entity.candidato.Candidato;



public interface RealizacaoProvaDao {
	
	public void insere(RealizacaoProva realizacaoProva);
	public void deleta(RealizacaoProva realizacaoProva);
	public void atualiza(RealizacaoProva realizacaoProva);
	public RealizacaoProva pesquisaRealizacaoProvaComId(int id);
	public List<RealizacaoProva> pesquisaRealizacaoProva(Candidato candidato);
	public List<Integer> pesquisaDataProva(Candidato candidato);
	public List<RealizacaoProva> pesquisaProvasCandidato(Candidato candidato,String semestre,Integer data);
	public List<RealizacaoProva> pesquisaRealizaçãoProvaPorCandidato(List<Candidato> candidatoList,Date dataInicio, Date dataFim);
	public List<Candidato> pesquisaCandidatos();
	
}
