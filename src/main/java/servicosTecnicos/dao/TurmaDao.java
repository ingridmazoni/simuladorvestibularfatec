package servicosTecnicos.dao;

import java.util.List;

import javax.persistence.PersistenceException;

import dominio.entity.curso.NotaCorte;
import dominio.entity.curso.Turma;

public interface TurmaDao {
	
	public void insere(Turma turma);
	public void deleta(Turma turma) throws PersistenceException;
	public void atualiza(Turma turma);
	public Turma pesquisaTurmaComId(int id);
	public Turma pesquisaTurma(NotaCorte notaCorte);
	public List<Turma> pesquisaTurma(Turma turma);
	public List<Turma> pesquisaTodasAsTurmas(Turma turma);
	public Turma pesquisaTurma2(Turma turma);

}
