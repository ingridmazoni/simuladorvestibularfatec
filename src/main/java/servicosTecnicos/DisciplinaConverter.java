package servicosTecnicos;


import javax.enterprise.context.ApplicationScoped;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.converter.Converter;
import dominio.entity.questaoVestibular.Disciplina;

@Convert(Disciplina.class)
@ApplicationScoped
public class DisciplinaConverter implements Converter<Disciplina>{
	


	public Disciplina convert(String arg0, Class<? extends Disciplina> arg1) {
		Disciplina disciplina = new Disciplina();
		 
		try{
			int id = Integer.parseInt(arg0);
			
			  if (!(arg0.isEmpty() || arg0 == null)) {
				  
				  
				  disciplina.setId(id);
				  
			 }
			
		}
		catch(NumberFormatException erro){
			disciplina.setId(0);
			disciplina.setNome(arg0);
		}
		
		
		  
	       
		return disciplina;
	}
}

