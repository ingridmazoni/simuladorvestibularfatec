package dominio.entity.curso;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotEmpty;

import dominio.entity.questaoVestibular.Disciplina;

@Entity
public class Curso{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotEmpty(message = "{campo.vazio}")
	@Column
	private String nome;
	
	@Column
	private String oQueOAlunoEstuda;
	
	@Column
	private String oQueOProfissionalFaz;
	
	@Column
	private String ondeTrabalhar;
	
	@ManyToOne
	private EixoTecnologico eixoTecnologico;
	
	@NotEmpty(message = "{campo.vazio}")
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Disciplina>materiasPesoDois;
	
	
	public String getoQueOAlunoEstuda() {
		return oQueOAlunoEstuda;
	}
	public void setoQueOAlunoEstuda(String oQueOAlunoEstuda) {
		this.oQueOAlunoEstuda = oQueOAlunoEstuda;
	}
		
	public List<Disciplina> getMateriasPesoDois() {
		return materiasPesoDois;
	}
	public void setMateriasPesoDois(List<Disciplina> materiasPesoDois) {
		this.materiasPesoDois = materiasPesoDois;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getoQueOAlunoEstudo() {
		return oQueOAlunoEstuda;
	}
	public void setoQueOAlunoEstudo(String oQueOAlunoEstuda) {
		this.oQueOAlunoEstuda = oQueOAlunoEstuda;
	}
	public String getoQueOProfissionalFaz() {
		return oQueOProfissionalFaz;
	}
	public void setoQueOProfissionalFaz(String oQueOProfissionalFaz) {
		this.oQueOProfissionalFaz = oQueOProfissionalFaz;
	}
	public String getOndeTrabalhar() {
		return ondeTrabalhar;
	}
	public void setOndeTrabalhar(String ondeTrabalhar) {
		this.ondeTrabalhar = ondeTrabalhar;
	}
	public EixoTecnologico getEixoTecnologico() {
		return eixoTecnologico;
	}
	public void setEixoTecnologico(EixoTecnologico eixoTecnologico) {
		this.eixoTecnologico = eixoTecnologico;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
