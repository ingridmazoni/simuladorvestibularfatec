package dominio.entity.curso;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import dominio.entity.questaoVestibular.Semestre;

@Entity
public class NotaCorte {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
		
	@ManyToOne
	private Turma turma;
	
	@Column
	private Integer numeroInscritos;
	
	@Column
	private Double demanda;
		
	@Column
	private Integer ano;
	
	@NotNull(message="{campo.vazio}")
	@Column
	@Enumerated(EnumType.STRING)
	private Semestre semestre;
	
	@NotNull(message="{campo.vazio}")
	@Column
	private Double notaCorteConvocados;
	
	@NotNull(message="{campo.vazio}")
	@Column
	private Double notaCorteSuplentes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Integer getNumeroInscritos() {
		return numeroInscritos;
	}

	public void setNumeroInscritos(Integer numeroInscritos) {
		this.numeroInscritos = numeroInscritos;
	}

	public Double getDemanda() {
		return demanda;
	}

	public void setDemanda(Double demanda) {
		this.demanda = demanda;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Semestre getSemestre() {
		return semestre;
	}

	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}

	public Double getNotaCorteConvocados() {
		return notaCorteConvocados;
	}

	public void setNotaCorteConvocados(Double notaCorteConvocados) {
		this.notaCorteConvocados = notaCorteConvocados;
	}

	public Double getNotaCorteSuplentes() {
		return notaCorteSuplentes;
	}

	public void setNotaCorteSuplentes(Double notaCorteSuplentes) {
		this.notaCorteSuplentes = notaCorteSuplentes;
	}
	
}
