package dominio.entity.curso;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import dominio.entity.candidato.EscolhaDeCursoDoCandidato;

@Entity
public class Turma{
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private Curso curso;
		
	@ManyToOne
	private Fatec fatec;
		
	@NotNull(message = "{campo.vazio}")
	@Column
	@Enumerated(EnumType.STRING)
	private Periodo periodo;
		
	@NotNull(message = "{campo.vazio}")
	@Column
	@Enumerated(EnumType.STRING)
	private TipoCurso tipoCurso;
		
	@Column
	private Integer numeroDeVagas;
	
	@OneToMany(mappedBy ="turma")
	private List<EscolhaDeCursoDoCandidato> candidatos;
			
	public Integer getNumeroDeVagas() {
		return numeroDeVagas;
	}

	public void setNumeroDeVagas(Integer numeroDeVagas) {
		this.numeroDeVagas = numeroDeVagas;
	}

	public TipoCurso getTipoCurso() {
		return tipoCurso;
	}
	
	public void setTipoCurso(TipoCurso tipoCurso) {
		this.tipoCurso = tipoCurso;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public Fatec getFatec() {
		return fatec;
	}
	public void setFatec(Fatec fatec) {
		this.fatec = fatec;
	}
	public Periodo getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<EscolhaDeCursoDoCandidato> getCandidatos() {
		return candidatos;
	}
	public void setCandidatos(List<EscolhaDeCursoDoCandidato> candidatos) {
		this.candidatos = candidatos;
	}
	
	

}
