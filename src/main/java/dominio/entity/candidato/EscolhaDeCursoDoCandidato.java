package dominio.entity.candidato;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import dominio.entity.curso.Turma;

@Entity
public class EscolhaDeCursoDoCandidato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private Candidato candidato;
	
	@ManyToOne
	private Turma turma;
	
	@Column
	@Enumerated(EnumType.STRING)
	private OpcaoCandidato opcaoCandidato;
	
	public Candidato getCandidato() {
		return candidato;
	}
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public OpcaoCandidato getOpcaoCandidato() {
		return opcaoCandidato;
	}
	public void setOpcaoCandidato(OpcaoCandidato opcaoCandidato) {
		this.opcaoCandidato = opcaoCandidato;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
