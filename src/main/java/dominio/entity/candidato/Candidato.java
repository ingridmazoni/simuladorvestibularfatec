package dominio.entity.candidato;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import dominio.entity.entidadesUtilitarias.Usuario;

@Entity
public class Candidato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
			
	@NotEmpty(message = "{campo.vazio}")
	@Column(length =50 , nullable =false)
	private String nome;
	
	@OneToOne(cascade = CascadeType.ALL)
	@Valid
	Usuario usuario;
	
	@Column
	@Temporal(TemporalType.DATE)
	@Past(message="{campo.data.passado}")
	private Date dataNascimento;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	
	@Email(message="{email}")
	@Column
	private String emailPrincipal;
	
	@Column
	@Email(message="{email}")
	private String emailSecundario;
	
	@Column
	private String telefone;
	
	@Column
	private String celular;
	
	@Column 
	private Integer anoInscricaoEnem;
	
	@Column
	@Digits(integer=3, fraction=2, message="{notaEnem}")
	private Double notaEnem;
	
	@Column
	private boolean pontuacaoAcrescidaAfrodescendente;
	
	@Column
	private boolean pontuacaoAcrescidaEscolaridadePublica;
	
	@OneToMany(mappedBy ="candidato")
	private List<EscolhaDeCursoDoCandidato> escolhaDeCurso;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getEmailPrincipal() {
		return emailPrincipal;
	}
	public void setEmailPrincipal(String emailPrincipal) {
		this.emailPrincipal = emailPrincipal;
	}
	public String getEmailSecundario() {
		return emailSecundario;
	}
	public void setEmailSecundario(String emailSecundario) {
		this.emailSecundario = emailSecundario;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Integer getAnoInscricaoEnem() {
		return anoInscricaoEnem;
	}
	public void setAnoInscricaoEnem(Integer anoInscricaoEnem) {
		this.anoInscricaoEnem = anoInscricaoEnem;
	}
	public Double getNotaEnem() {
		return notaEnem;
	}
	public void setNotaEnem(Double notaEnem){
		this.notaEnem = notaEnem;
	}
		
	public boolean isPontuacaoAcrescidaAfrodescendente() {
		return pontuacaoAcrescidaAfrodescendente;
	}
	public void setPontuacaoAcrescidaAfrodescendente(boolean pontuacaoAcrescidaAfrodescendente) {
		this.pontuacaoAcrescidaAfrodescendente = pontuacaoAcrescidaAfrodescendente;
	}
	public boolean isPontuacaoAcrescidaEscolaridadePublica() {
		return pontuacaoAcrescidaEscolaridadePublica;
	}
	public void setPontuacaoAcrescidaEscolaridadePublica(
			boolean pontuacaoAcrescidaEscolaridadePublica) {
		this.pontuacaoAcrescidaEscolaridadePublica = pontuacaoAcrescidaEscolaridadePublica;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<EscolhaDeCursoDoCandidato> getEscolhaDeCurso() {
		return escolhaDeCurso;
	}
	public void setEscolhaDeCurso(List<EscolhaDeCursoDoCandidato> escolhaDeCurso) {
		this.escolhaDeCurso = escolhaDeCurso;
	}
	
	
	
}
