package dominio.entity.entidadesUtilitarias;

public enum TipoLista {
	CONVOCADOS,
	SUPLENTES,
	GERAL
}
