package dominio.entity.entidadesUtilitarias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Usuario {
	
	@Id
	@NotEmpty(message = "{campo.vazio}")
	@Column(length =50 , nullable =false)
	private String login;
	
	@NotEmpty(message = "{campo.vazio}")
	@Column(length =50 , nullable =false)
	private String senha;
	
	@Column
	@Enumerated(EnumType.STRING)
	Perfil perfil;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	

}
