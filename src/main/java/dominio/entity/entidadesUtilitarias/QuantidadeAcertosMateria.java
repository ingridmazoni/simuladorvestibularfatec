package dominio.entity.entidadesUtilitarias;

import com.sun.prism.Material;

import dominio.entity.questaoVestibular.Disciplina;

public class QuantidadeAcertosMateria {
	
	private Long quantAcertosMateria;
	
	private Disciplina d;

	public Long getQuantAcertosMateria() {
		return quantAcertosMateria;
	}

	public void setQuantAcertosMateria(Long quantAcertosMateria) {
		this.quantAcertosMateria = quantAcertosMateria;
	}

	public Disciplina getD() {
		return d;
	}

	public void setD(Disciplina d) {
		this.d = d;
	}
	
	
	

}
