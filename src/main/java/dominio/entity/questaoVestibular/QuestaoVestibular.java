package dominio.entity.questaoVestibular;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.caelum.vraptor.observer.upload.UploadedFile;

@Entity
public class QuestaoVestibular {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(length = 9000,columnDefinition="Text")
	private String pergunta;
	
	@Column
	private String imagemPergunta;
	
	@Transient
	private UploadedFile arquivoImagemPergunta;
	
	@Column(length = 9000,columnDefinition="Text")
	private String alternativaA;
	
	@Column
	private String imagemAlternativaA;
	
	@Transient
	private UploadedFile arquivoImagemAlternativaA;
	
	@Column(length = 9000,columnDefinition="Text")
	private String alternativaB;
	
	@Column
	private String imagemAlternativaB;
	
	@Transient
	private UploadedFile arquivoImagemAlternativaB;
	
	@Column(length = 9000,columnDefinition="Text")
	private String alternativaC;
	
	@Column
	private String imagemAlternativaC;
	
	@Transient
	private UploadedFile arquivoImagemAlternativaC;
	
	@Column(length = 9000,columnDefinition="Text")
	private String alternativaD;
	
	@Column
	private String imagemAlternativaD;
	
	@Transient
	private UploadedFile arquivoImagemAlternativaD;
	
	@Column(length = 9000,columnDefinition="Text")
	private String alternativaE;
	
	@Column
	private String imagemAlternativaE;
	
	@Transient
	private UploadedFile arquivoImagemAlternativaE;
	
	@Column @NotNull(message = "{campo.vazio}")
	@Enumerated(EnumType.STRING)
	private RespostaQuestao respostaCorreta;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ConteudoProgramatico conteudoProgramatico;
	
	@Column
	private Integer anoVestibular;
	
	@Column 
	@Enumerated(EnumType.STRING)
	private Semestre semestreVestibular;
	
	public String getPergunta() {
		return pergunta;
	}
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}
	public String getAlternativaA() {
		return alternativaA;
	}
	public void setAlternativaA(String alternativaA) {
		this.alternativaA = alternativaA;
	}
	public String getAlternativaB() {
		return alternativaB;
	}
	public void setAlternativaB(String alternativaB) {
		this.alternativaB = alternativaB;
	}
	public String getAlternativaC() {
		return alternativaC;
	}
	public void setAlternativaC(String alternativaC) {
		this.alternativaC = alternativaC;
	}
	public String getAlternativaD() {
		return alternativaD;
	}
	public void setAlternativaD(String alternativaD) {
		this.alternativaD = alternativaD;
	}
	public String getAlternativaE() {
		return alternativaE;
	}
	public void setAlternativaE(String alternativaE) {
		this.alternativaE = alternativaE;
	}
	public RespostaQuestao getRespostaCorreta() {
		return respostaCorreta;
	}
	public void setRespostaCorreta(RespostaQuestao respostaCorreta) {
		this.respostaCorreta = respostaCorreta;
	}
	public ConteudoProgramatico getConteudoProgramatico() {
		return conteudoProgramatico;
	}
	public void setConteudoProgramatico(ConteudoProgramatico conteudoProgramatico) {
		this.conteudoProgramatico = conteudoProgramatico;
	}
	
	public Integer getAnoVestibular() {
		return anoVestibular;
	}
	public void setAnoVestibular(Integer anoVestibular) {
		this.anoVestibular = anoVestibular;
	}
	public Semestre getSemestreVestibular() {
		return semestreVestibular;
	}
	public void setSemestreVestibular(Semestre semestreVestibular) {
		this.semestreVestibular = semestreVestibular;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getImagemPergunta() {
		return imagemPergunta;
	}
	public void setImagemPergunta(String imagemPergunta) {
		this.imagemPergunta = imagemPergunta;
		
	}
	
	public String getImagemAlternativaA() {
		return imagemAlternativaA;
	}
	public void setImagemAlternativaA(String imagemAlternativaA) {
		this.imagemAlternativaA = imagemAlternativaA;
	}
	
	public String getImagemAlternativaB() {
		return imagemAlternativaB;
	}
	public void setImagemAlternativaB(String imagemAlternativaB) {
		this.imagemAlternativaB = imagemAlternativaB;
		
	}
	public String getImagemAlternativaC() {
		return imagemAlternativaC;
	}
	public void setImagemAlternativaC(String imagemAlternativaC) {
		this.imagemAlternativaC = imagemAlternativaC;
		
	}
	
	public String getImagemAlternativaD() {
		return imagemAlternativaD;
	}
	
	public void setImagemAlternativaD(String imagemAlternativaD) {
		this.imagemAlternativaD = imagemAlternativaD;
		
	}
	
	public String getImagemAlternativaE() {
		return imagemAlternativaE;
	}
	public void setImagemAlternativaE(String imagemAlternativaE) {
		this.imagemAlternativaE = imagemAlternativaE;
		
	}
		
	public UploadedFile getArquivoImagemPergunta() {
		return arquivoImagemPergunta;
	}
	public void setArquivoImagemPergunta(UploadedFile arquivoImagemPergunta) {
		this.imagemPergunta = arquivoImagemPergunta.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemPergunta = arquivoImagemPergunta;
		
	}
	public UploadedFile getArquivoImagemAlternativaA() {
		return arquivoImagemAlternativaA;
	}
	public void setArquivoImagemAlternativaA(UploadedFile arquivoImagemAlternativaA) {
		this.imagemAlternativaA = arquivoImagemAlternativaA.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemAlternativaA = arquivoImagemAlternativaA;
	}
	public UploadedFile getArquivoImagemAlternativaB() {
		return arquivoImagemAlternativaB;
	}
	public void setArquivoImagemAlternativaB(UploadedFile arquivoImagemAlternativaB) {
		this.imagemAlternativaB = arquivoImagemAlternativaB.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemAlternativaB = arquivoImagemAlternativaB;
	}
	public UploadedFile getArquivoImagemAlternativaC() {
		return arquivoImagemAlternativaC;
	}
	public void setArquivoImagemAlternativaC(UploadedFile arquivoImagemAlternativaC) {
		this.imagemAlternativaC = arquivoImagemAlternativaC.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemAlternativaC = arquivoImagemAlternativaC;
	}
	public UploadedFile getArquivoImagemAlternativaD() {
		return arquivoImagemAlternativaD;
	}
	public void setArquivoImagemAlternativaD(UploadedFile arquivoImagemAlternativaD) {
		this.imagemAlternativaD = arquivoImagemAlternativaD.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemAlternativaD = arquivoImagemAlternativaD;
	}
	public UploadedFile getArquivoImagemAlternativaE() {
		return arquivoImagemAlternativaE;
	}
	public void setArquivoImagemAlternativaE(UploadedFile arquivoImagemAlternativaE) {
		this.imagemAlternativaE = arquivoImagemAlternativaE.getFileName().replaceAll("[^a-zA-Z0-9|/.]", "");
		this.arquivoImagemAlternativaE = arquivoImagemAlternativaE;
	}
	
	
	
	
	

}
