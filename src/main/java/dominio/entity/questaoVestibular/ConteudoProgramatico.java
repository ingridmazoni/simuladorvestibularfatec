package dominio.entity.questaoVestibular;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class ConteudoProgramatico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotEmpty(message = "{campo.vazio}")
	@Column
	private String programaProva;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Disciplina disciplina;
	
	public String getProgramaProva() {
		return programaProva;
	}
	public void setProgramaProva(String programaProva) {
		this.programaProva = programaProva;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
