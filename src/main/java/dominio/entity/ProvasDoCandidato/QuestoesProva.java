package dominio.entity.ProvasDoCandidato;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import dominio.entity.questaoVestibular.QuestaoVestibular;
import dominio.entity.questaoVestibular.RespostaQuestao;

@Entity
public class QuestoesProva {
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private RealizacaoProva realizacaoProva;
	
	@ManyToOne
	private QuestaoVestibular questaoVestibular;
	
	@Column
	@Enumerated(EnumType.STRING)
	private RespostaQuestao respostaDoCandidato;
	
	@Column
	private int sequenciaDaQuestaoNaProva;
	
	public int getSequenciaDaQuestaoNaProva() {
		return sequenciaDaQuestaoNaProva;
	}
	public void setSequenciaDaQuestaoNaProva(int sequenciaDaQuestaoNaProva) {
		this.sequenciaDaQuestaoNaProva = sequenciaDaQuestaoNaProva;
	}
	public RealizacaoProva getRealizacaoProva() {
		return realizacaoProva;
	}
	public void setRealizacaoProva(RealizacaoProva realizacaoProva) {
		this.realizacaoProva = realizacaoProva;
	}
	public QuestaoVestibular getQuestaoVestibular() {
		return questaoVestibular;
	}
	public void setQuestaoVestibular(QuestaoVestibular questaoVestibular) {
		this.questaoVestibular = questaoVestibular;
	}
	public RespostaQuestao getRespostaDoCandidato() {
		return respostaDoCandidato;
	}
	public void setRespostaDoCandidato(RespostaQuestao respostaDoCandidato) {
		this.respostaDoCandidato = respostaDoCandidato;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
