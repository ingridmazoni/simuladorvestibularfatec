package dominio.entity.ProvasDoCandidato;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import dominio.entity.candidato.Candidato;
import servicosTecnicos.UsuarioLogado;

@Entity
public class RealizacaoProva {
	
	public RealizacaoProva() {
		Date d = new Date();
		this.setDataRealizacao(d);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private Candidato candidato;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date dataRealizacao;
	
	@Transient
	@Digits(integer=3, fraction=2, message="{notaRedacao}")
	@DecimalMax("100.00") @DecimalMin("000.00") 
	private Double notaRedacao;
	
	@Transient 
	private Double notaProva;
	
	@Transient
	private Integer npc;
	
	public Candidato getCandidato() {
		return candidato;
	}
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	public Date getDataRealizacao() {
		return dataRealizacao;
	}
	public void setDataRealizacao(Date dataRealizacao) {
		this.dataRealizacao = dataRealizacao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Double getNotaRedacao() {
		return notaRedacao;
	}
	public void setNotaRedacao(Double notaRedacao) {
		this.notaRedacao = notaRedacao;
	}
	public Double getNotaProva() {
		return notaProva;
	}
	public void setNotaProva(Double notaProva) {
		this.notaProva = notaProva;
	}
	public Integer getNpc() {
		return npc;
	}
	public void setNpc(Integer npc) {
		this.npc = npc;
	}
	
	
}
