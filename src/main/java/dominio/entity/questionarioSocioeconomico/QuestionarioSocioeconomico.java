package dominio.entity.questionarioSocioeconomico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import dominio.entity.candidato.Candidato;
import dominio.entity.questaoVestibular.RespostaQuestao;

@Entity
public class QuestionarioSocioeconomico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	private Candidato candidato;
	
	@ManyToOne
	private Questao questao;
	
	@Column
	@Enumerated(EnumType.STRING)
	private RespostaQuestao respostaDoCandidato;
	
	public Candidato getCandidato() {
		return candidato;
	}
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	public Questao getQuestao() {
		return questao;
	}
	public void setQuestao(Questao questao) {
		this.questao = questao;
	}
	public RespostaQuestao getRespostaDoCandidato() {
		return respostaDoCandidato;
	}
	public void setRespostaDoCandidato(RespostaQuestao respostaDoCandidato) {
		this.respostaDoCandidato = respostaDoCandidato;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
