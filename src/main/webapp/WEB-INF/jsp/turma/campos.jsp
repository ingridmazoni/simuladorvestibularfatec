<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
  						<div class="col-md-10">
    						<select id="fatecID" name="turma.fatec.id" class="form-control">
    										<option value=""></option>
		        							<c:forEach items="${fatecList}" var="fatec">
		        							
				        							<c:choose>
															<c:when test="${fatec.id == turma.fatec.id}">
																<c:set var="teste" value="selected"></c:set>
																			 
															</c:when>
															<c:otherwise>
																<c:set var="teste" value=""></c:set>
																			
															</c:otherwise>
													</c:choose>
				        							<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
		          							</c:forEach>
          					</select>
  						</div>
					</div>
				</div>
		</div>
	</div>	


	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="cursoID">Curso</label>
  						<div class="col-md-10">
    						<select id="cursoID" name="turma.curso.id" class="form-control">
    										<option value=""></option>
		        							<c:forEach items="${cursoList}" var="curso">
				        							<c:choose>
																	<c:when test="${curso.id == turma.curso.id}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
													</c:choose>
		        									<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
		          							</c:forEach>
          					</select>
  						</div>
					</div>
				</div>

				<div class="col-md-6">	
					<div class="form-group">
  							<label class="col-md-3 control-label" for="numeroVagas">Número de Vagas</label>  
  							<div class="col-md-8">
  								<input id="numeroVagas" name="turma.numeroDeVagas" placeholder="Digite o número de vagas" class="form-control input-md" type="text" value="${turma.numeroDeVagas}">
  							</div>
					</div>
				</div>



		</div>
	</div>	


	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="tipoCurso">Tipo</label>
  						<div class="col-md-10">
    						<select id="tipoCurso" name="turma.tipoCurso" class="form-control">
    										<option value=""></option>
		        							<c:forEach items="${tipoCursoList}" var="tipoCurso">
		        								<c:choose>
																	<c:when test="${turma.tipoCurso eq tipoCurso}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
												</c:choose>
		        								<option <c:out value="${teste}"/> value="${tipoCurso}">${tipoCurso}</option>
		          							</c:forEach>
          					</select>
  						</div>
					</div>
			</div>

				<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="periodo">Período</label>
  						<div class="col-md-10">
    						<select id="periodo" name="turma.periodo" class="form-control">
    										<option value=""></option>
		        							<c:forEach items="${periodoList}" var="periodo">
		        								<c:choose>
																	<c:when test="${turma.periodo eq periodo}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
												</c:choose>
		        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
		          							</c:forEach>
          					</select>
  						</div>
					</div>
				</div>

		</div>
	</div>	