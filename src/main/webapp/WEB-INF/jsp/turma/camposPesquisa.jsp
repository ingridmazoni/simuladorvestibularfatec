<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
  						<div class="col-md-10">
    						<select id="selectbasic" name="turma.fatec.id" class="form-control">
                  							<option value=""></option>
	        								<c:forEach items="${fatecList}" var="fatec">
		        								<c:choose>
														<c:when test="${notaCorte.turma.fatec.id == fatec.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
												</c:choose>
	        									<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
					          				
	          							  </c:forEach>
                  							
                						</select>
  						</div>
					</div>
				</div>


				<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Curso</label>
  						<div class="col-md-10">
    						<select id="selectbasic" name="turma.curso.id" class="form-control">
                  							<option value=""></option>
	        								<c:forEach items="${cursoList}" var="curso">
			        								<c:choose>
															<c:when test="${notaCorte.turma.curso.id == curso.id}">
																<c:set var="teste" value="selected"></c:set>
																			 
															</c:when>
															<c:otherwise>
																<c:set var="teste" value=""></c:set>
																			
															</c:otherwise>
													</c:choose>
	        										<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
					          				
	          							   </c:forEach>
                  							
                			</select>
  						</div>
					</div>
				</div>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Tipo</label>
  						<div class="col-md-10">
    						<select id="selectbasic" name="selectbasic" class="form-control">
      							<option value="1">Presencial</option>
      							
    						</select>
  						</div>
					</div>
			</div>

				<div class="col-md-6">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Período</label>
  						<div class="col-md-10">
    						<select id="selectbasic" name="turma.periodo" class="form-control">
                                       <option value=""></option>
	        								<c:forEach items="${periodoList}" var="periodo">
	        								<c:choose>
													<c:when test="${turma.periodo == periodo}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
					          				
	          							</c:forEach>
                            </select>
  						</div>
					</div>
				</div>

		</div>
	</div>	
	