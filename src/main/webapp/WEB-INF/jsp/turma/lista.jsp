<div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
          <thead>
                <tr>
                      <th>Fatec</th>
                      <th>Curso</th>
                      <th>Tipo</th>
                      <th>Período</th>
                      <th>N° de Vagas</th>
                      <th>Ações</th>

                </tr>
          </thead>
          <tbody>
           <c:forEach items="${turmaList}" var="turma">
                <tr class="success">
                  <td class="col-md-4">${turma.fatec.nome}</td>
                  <td class="col-md-3">${turma.curso.nome}</td>
                  <td>${turma.tipoCurso}</td>
                  <td>${turma.periodo}</td>
                  <td>${turma.numeroDeVagas}</td>
                  <td class="col-md-2">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/turma/${turma.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                       <form action="<c:url value="/turma/${turma.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>
                  </td>
                </tr>
                
                </c:forEach>
               
          </tbody>
      </table>
    </div>
  </div>