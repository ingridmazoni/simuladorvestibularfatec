<div class="row">
				<div class="col-md-4">
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Cidade</label>
  						<div class="col-md-10">
    						<select id="city" name="fatec.cidade.id" class="form-control">
	      								<option value=""></option>
	        							<c:forEach items="${cidadesList}" var="cidade">
	        								<c:choose>
													<c:when test="${fatec.cidade.id == cidade.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${cidade.id}">${cidade.nome}</option>
					          				
	          							</c:forEach>
          						</select>
  						</div>
					</div>

				</div>

				<div class="col-md-8">
					<div class="form-group">
  							<label class="col-md-2 control-label" for="textinput">Nome</label>  
 							 <div class="col-md-10">
  								<input id="textinput" name="fatec.nome" placeholder="Digite o nome da fatec" class="form-control input-md" type="text">
  							</div>
					</div>

				</div>

	</div>