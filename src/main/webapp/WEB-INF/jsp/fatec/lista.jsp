<div class="row">
    <div class="col-md-12">
      <table class="table table-hover" align="center">
          <thead>
                <tr>
                	  <th>Cidade</th>		
                      <th>Fatec</th>
                      <th>Telefone 1</th>
                      <th>E-mail </th>
                      <th>Ações</th>
                      
                </tr>
          </thead>
          <tbody>
             <c:forEach items="${fatecList}" var="fatec">
                <tr class="success">
                  <td class="col-md-2">${fatec.cidade.nome}</td>
                  <td class="col-md-3">${fatec.nome}</td>
                  <td class="col-md-2">${fatec.telefone1}</td>
                  <td class="col-md-3">${fatec.email1}</td>
                  
                                   
                  <td class="col-md-2">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/fatec/${fatec.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                      
                      <form action="<c:url value="/fatec/${fatec.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>

                  </td>
                </tr>
                </c:forEach>
               
          </tbody>
      </table>
    </div>
  </div>
	