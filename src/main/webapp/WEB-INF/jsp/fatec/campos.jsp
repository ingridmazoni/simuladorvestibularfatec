	<input id="id" type="hidden" name="fatec.id" value="${fatec.id}">


    	<div class="row">
    		<div class="col-md-12">
    				<div class="form-group">
      						<label class="col-md-1 control-label" for="">Nome</label>  
      						<div class="col-md-9">
      								<input id="nome" name="fatec.nome" type="text" placeholder="Digite o nome da fatec" value="${fatec.nome}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	

    	<div class="row">
    		<div class="col-md-12">
    				<div class="form-group">
      						<label class="col-md-1 control-label" for="">Cep</label>  
      						<div class="col-md-4">
      								<input id="cep" name="fatec.cep" type="text" placeholder="Digite o cep"  value="${fatec.cep}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	
    		

    	<div class="row">
    		<div class="col-md-12">
    				<div class="form-group">
      						<label class="col-md-1 control-label" for="">Endereço</label>  
      						<div class="col-md-5">
      								<input id="endereco" name="fatec.endereco" type="text" placeholder="Digite o endereço" value="${fatec.endereco}" class="form-control input-md">
        					</div>
    			

    		
      						<label class="col-md-1 control-label" for="">Número</label>  
      						<div class="col-md-1">
      								<input id="numero" name="fatec.numero" type="number" value="${fatec.numero}" class="form-control input-md">
        					</div>
        					
        					<label class="col-md-1 control-label" for="">Bairro</label>  
      						<div class="col-md-3">
      								<input id="bairro" name="fatec.bairro" type="text" placeholder="Digite o bairro" value="${fatec.bairro}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	

    	<div class="row">
    		<div class="col-md-12">
    				<div class="form-group">
      						<label class="col-md-1 control-label" for="selectbasic">Cidade</label>
      						<div class="col-md-4">
      							<select id="city" required name="fatec.cidade.id" class="form-control">
	      								<option value=""></option>
	        							<c:forEach items="${cidadesList}" var="cidade">
	        								<c:choose>
													<c:when test="${fatec.cidade.id == cidade.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${cidade.id}">${cidade.nome}</option>
					          				
	          							</c:forEach>
          						</select>
        						
        					</div>

    				</div>
    		</div>
    	</div>	

    	<div class="row">
    		<div class="col-md-6">
    				<div class="form-group">
      						<label class="col-md-2 control-label" for="">Telefone 1</label>  
      						<div class="col-md-8">
      								<input id="telefone1" name="fatec.telefone1" type="text" placeholder="Digite o telefone" value="${fatec.telefone1}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    		<div class="col-md-6">
    				<div class="form-group">
      						<label class="col-md-2 control-label" for="">Telefone 2</label>  
      						<div class="col-md-8">
      								<input id="telefone2" name="fatec.telefone2" type="text" placeholder="Digite o telefone" value="${fatec.telefone2}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	

    	<div class="row">
    		<div class="col-md-6">
    				<div class="form-group">
      						<label class="col-md-2 control-label" for="">E-mail 1</label>  
      						<div class="col-md-8">
      								<input id="email" name="fatec.email1" type="email" placeholder="Digite o E-mail" value="${fatec.email1}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    		<div class="col-md-6">
    				<div class="form-group">
      						<label class="col-md-2 control-label" for="">E-mail 2</label>  
      						<div class="col-md-8">
      								<input id="email" name="fatec.email2" type="email" placeholder="Digite o E-mail" value="${fatec.email2}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	

    	<div class="row">
    		<div class="col-md-12">
    				<div class="form-group">
      						<label class="col-md-1 control-label" for="">Site</label>  
      						<div class="input-prepend col-md-5">
      								<input id="site" name="fatec.site" type="url" placeholder="Digite o Site" value="${fatec.site}" class="form-control input-md">
        					</div>
    				</div>
    		</div>
    	</div>	