	
			<div class="row">
  				<div class="col-md-12">
  		            <div class="form-group">
        							<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
        							<div class="col-md-4">
          								<select id="selectbasic" name="notaCorte.turma.fatec.id" class="form-control" required>
                  							<option value=""></option>
	        								<c:forEach items="${fatecList}" var="fatec">
		        								<c:choose>
														<c:when test="${notaCorte.turma.fatec.id == fatec.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
												</c:choose>
	        									<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
					          				
	          							  </c:forEach>
                  							
                						</select>
        							</div>
      						</div>
    				</div>		
			</div>
	
			<div class="row">
				<div class="col-md-12">
		
						<div class="form-group">
  							<label class="col-md-2 control-label" for="selectbasic">Curso</label>
  							<div class="col-md-4">
    								<select id="selectbasic" name="notaCorte.turma.curso.id" class="form-control" required>
                  							<option value=""></option>
	        								<c:forEach items="${cursoList}" var="curso">
			        								<c:choose>
															<c:when test="${notaCorte.turma.curso.id == curso.id}">
																<c:set var="teste" value="selected"></c:set>
																			 
															</c:when>
															<c:otherwise>
																<c:set var="teste" value=""></c:set>
																			
															</c:otherwise>
													</c:choose>
	        										<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
					          				
	          							   </c:forEach>
                  							
                						</select>
  							</div>
						</div>
  				</div>		
			</div>
		
			<div class="row">
				<div class="col-md-12">
		
						<div class="form-group">
  							<label class="col-md-2 control-label" for="selectbasic">Período</label>
  							<div class="col-md-4">
    								<select id="selectbasic" name="notaCorte.turma.periodo" class="form-control" required>
                                       <option value=""></option>
	        								<c:forEach items="${periodoList}" var="periodo">
	        								<c:choose>
													<c:when test="${notaCorte.turma.periodo == periodo}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
					          				
	          							</c:forEach>
                                    </select>
  							</div>
						</div>
  				</div>		
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
  						<label class="col-md-2 control-label" for="textinput">Número de Inscritos</label>  
  						<div class="col-md-4">
  								<input id="textinput" name="notaCorte.numeroInscritos" placeholder="Digite o número de inscritos" class="form-control input-md" type="text" value="${notaCorte.numeroInscritos}">
  						</div>
					</div>
				</div>		
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
  						<label class="col-md-2 control-label" for="textinput">Demanda</label>  
  						<div class="col-md-4">
  								<input id="textinput" name="notaCorte.demanda" placeholder="Digite a demanda" class="form-control input-md" type="text" value="${notaCorte.demanda}">
  						</div>
					</div>
				</div>		
			</div>



	

<div class="col-md-10 col-md-offset-1">
    <div class="panel panel-success">
          <div class="panel-heading">
                <h3 align="center" class="panel-title">Digite a nota de corte do curso</h3>
          </div>

			   <div class="panel-body">
              						<div class="row">
                  							<div class="col-md-12">
                      									<div class="form-group">
                                                <label class="col-md-4 control-label" for="selectbasic">Ano</label>
                                        							<div class="col-md-2 form-group">
                                                							<div class='input-group date' id='datetimepicker1'>
                                                    								<input type="text" required class="form-control" name="notaCorte.ano" value="${notaCorte.ano}"/>
                                                        								<span class="input-group-addon">
                                                        									<span class="glyphicon glyphicon-calendar"></span>
                                                        								</span>
                                                							</div>
                                        							</div>
                                        									
                                  							
                            										<label class="col-md-1 control-label" for="selectbasic">Semestre</label>
                                  										<div class="col-md-2">
                                        										<select id="selectbasic" name="notaCorte.semestre" class="form-control" required>
												                                   <option value=""></option>
													        								<c:forEach items="${semestreList}" var="semestre">
													        								<c:choose>
																									<c:when test="${notaCorte.semestre == semestre}">
																										<c:set var="teste" value="selected"></c:set>
																													 
																									</c:when>
																									<c:otherwise>
																										<c:set var="teste" value=""></c:set>
																													
																									</c:otherwise>
																							</c:choose>
													        								<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
																	          				
													          							</c:forEach>
												                                </select>
                                  										</div>
                        								</div>
                    						</div>
                					</div>

                					<div class="row">
                							<div class="col-md-12 col-md-offset-2">
                									<div class="form-group">
                										  
                      											<label class="col-md-4 control-label" for="textinput">Nota de Corte da Lista de Convocados</label>  
                      											<div class="col-md-3">
                      													<input id="textinput" required name="notaCorte.notaCorteConvocados" placeholder="Digite a nota de corte" class="form-control input-md" type="text" value="${notaCorte.notaCorteConvocados}">
                      											</div>
                										  
                									</div>
                  						</div>
                					</div>

                				<div class="row">
              							<div class="col-md-12 col-md-offset-2">
              									<div class="form-group">
              										  
                                      <label class="col-md-4 control-label" for="textinput">Nota de Corte da Lista de Suplentes</label>  
                                      <div class="col-md-3">
                                          	<input id="textinput" required name="notaCorte.notaCorteSuplentes" placeholder="Digite a nota de corte" class="form-control input-md" type="text" value="${notaCorte.notaCorteSuplentes}">
                                      </div>
              										  
              									</div>
                						</div>
                				</div>


				  </div>
	 </div>

</div>