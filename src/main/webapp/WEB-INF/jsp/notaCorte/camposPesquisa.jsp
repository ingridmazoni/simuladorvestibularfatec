<div class="row">
		    <div class="col-md-6">	
      					<div class="form-group">
          						<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
          						<div class="col-md-10">
                						<select id="selectbasic" name="notaCorte.turma.fatec.id" class="form-control">
                  							<option value=""></option>
	        								<c:forEach items="${fatecList}" var="fatec">
		        								<c:choose>
														<c:when test="${notaCorte.turma.fatec.id == fatec.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
												</c:choose>
	        									<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
					          				
	          							  </c:forEach>
                  							
                						</select>
          						</div>
      					</div>
  			</div>
        <div class="col-md-6">
    					<div class="form-group">
          						<label class="col-md-1 control-label" for="selectbasic">Curso</label>
          						<div class="col-md-10">
                						<select id="selectbasic" name="notaCorte.turma.curso.id" class="form-control">
                  							<option value=""></option>
	        								<c:forEach items="${cursoList}" var="curso">
			        								<c:choose>
															<c:when test="${notaCorte.turma.curso.id == curso.id}">
																<c:set var="teste" value="selected"></c:set>
																			 
															</c:when>
															<c:otherwise>
																<c:set var="teste" value=""></c:set>
																			
															</c:otherwise>
													</c:choose>
	        										<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
					          				
	          							   </c:forEach>
                  							
                						</select>
          						</div>
    					</div>
        </div>
			
	</div>

    <div class="row">
		              <div class="col-md-4">
                      <div class="form-group">
                              <label class="col-md-2 col-md-offset-2 control-label" for="selectbasic">Período</label>
                              <div class="col-md-8">
                                   <select id="selectbasic" name="notaCorte.turma.periodo" class="form-control">
                                       <option value=""></option>
	        								<c:forEach items="${periodoList}" var="periodo">
	        								<c:choose>
													<c:when test="${notaCorte.turma.periodo == periodo}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
					          				
	          							</c:forEach>
                                    </select>
                              </div>
                      </div>
                  </div>

                	<div class="col-md-4">
                      <label class="col-md-3 control-label" for="selectbasic">Ano</label>
                    	<div class="col-md-6 form-group">
                        	<div class='input-group date' id='datetimepicker1'>
                            	<input type="text" readonly class="form-control" name="notaCorte.ano" />
                            	<span class="input-group-addon">
                            		<span class="glyphicon glyphicon-calendar"></span>
                            	</span>
                        	</div>
                    	</div>
              		</div>

                  <div class="col-md-4">
                        <div class="form-group">
                              <label class="col-md-3 col-md-pull-1 control-label" for="selectbasic">Semestre</label>
                              <div class="col-md-8 col-md-pull-1">
                                  <select id="selectbasic" name="notaCorte.semestre" class="form-control">
                                   <option value=""></option>
	        								<c:forEach items="${semestreList}" var="semestre">
	        								<c:choose>
													<c:when test="${notaCorte.turma.semestre == semestre}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
					          				
	          							</c:forEach>
                                </select>
                              </div>
                        </div>
                  </div>

    </div>  	