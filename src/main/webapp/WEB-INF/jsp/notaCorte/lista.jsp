<div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
          <thead>
                <tr>
                	  <th>Fatec</th>
                	  <th>Curso</th>
                	  <th>Periodo</th>			
                      <th>Nota de Corte Convocados</th>
                      <th>Nota de Corte Suplentes</th>
                      <th>Número de Inscritos</th>
                      <th>Demanda</th>
                      <th>Ano</th>
                      <th>Semestre</th>
                      <th>N° vagas</th>
                      <th>Ações</th>

                </tr>
          </thead>
          <tbody>
           <c:forEach items="${notaCorteList}" var="notaCorte">
                <tr class="success">
                  <td class="col-md-4">${notaCorte.turma.fatec.nome}</td>
                  <td class="col-md-2">${notaCorte.turma.curso.nome}</td>
                  <td>${notaCorte.turma.periodo}</td>
                  <td>${notaCorte.notaCorteConvocados}</td>
                  <td>${notaCorte.notaCorteSuplentes}</td>
                  <td>${notaCorte.numeroInscritos}</td>
                  <td>${notaCorte.demanda}</td>
                  <td>${notaCorte.ano}</td>
                  <td class="col-md-1">${notaCorte.semestre}</td>
                  <td>${notaCorte.turma.numeroDeVagas}</td>
                  <td class="col-md-3">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/notaCorte/${notaCorte.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                       <form action="<c:url value="/notaCorte/${notaCorte.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>
                  </td>
                </tr>
                
                </c:forEach>
               
          </tbody>
      </table>
    </div>
  </div>