<div class="row">
    <div class="col-md-12">
      <table class="table table-hover" align="center">
          <thead>
                <tr>
                      <th>Disciplina</th>
                      <th>Conteúdo Programático</th>
                      <th>Ações</th>
                      
                </tr>
          </thead>
          <tbody>
          <c:forEach items="${conteudoProgramaticoList}" var="conteudoProgramatico">
                <tr class="success">
                  <td>${conteudoProgramatico.disciplina.nome}</td>
                  <td>${conteudoProgramatico.programaProva}</td>
                  <td class="col-md-3">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/conteudoProgramatico/${conteudoProgramatico.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                      
                      <form action="<c:url value="/conteudoProgramatico/${conteudoProgramatico.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>

                  </td>
                </tr>
          </c:forEach>     
          </tbody>
      </table>
    </div>
 </div>