<div class="row">
		
			<div class="col-md-4">	
					<div class="form-group">
  						<label class="col-md-2 control-label" for="selectbasic">Disciplina</label>
  						<div class="col-md-10">
    						<select id="selectbasic" name="conteudoProgramatico.disciplina.id" class="form-control">
      							<option value=""></option>
	        								<c:forEach items="${disciplinasList}" var="disciplina">
	        								<c:choose>
													<c:when test="${conteudoProgramatico.disciplina.id == disciplina.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								<option <c:out value="${teste}"/> value="${disciplina.id}">${disciplina.nome}</option>
					          				
	          								</c:forEach>
    						</select>
  						</div>
					</div>
			</div>


				<div class="col-md-8">
					<div class="form-group">
  						<label class="col-md-3 control-label" for="">Conteúdo Programático</label>  
  						<div class="col-md-9">
  								<input id="" name="conteudoProgramatico.programaProva" type="text" placeholder="Digite o conteúdo programático da prova" class="form-control input-md" value="${conteudoProgramatico.programaProva}">
    					</div>
					</div>

				</div>
			
		</div>