 <div class="container-fluid">
    	
    	 <c:import url="subtitulo.jsp"></c:import>
    	   	

		<div class="row">
			<div class="col-md-3 col-md-offset-4">
					<form class="form-horizontal" role="form" method="post" action=<c:url value="/prova/novo" /> id="meu_form">	
							<input id="id" type="hidden" name="contador" value="${0}">
							<button class="btn btn-success btn-lg active" type="submit" id="btnSalvar">
                          		 Clique para iniciar uma nova simulação
                      		</button> 
		    	  	</form>
		   	</div>
		</div>		

		<br>
		<br>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-success">
						<div class="panel-heading" align="center">
							<h3 class="panel-title">Selecione o ano e o semestre para consultar a lista de todas as provas realizadas</h3>
						</div>
						<div class="panel-body">
								<div class="row">
										<form class="form-horizontal" role="form" method="post" action=<c:url value="/candidato/listaProvas" /> id="meu_form">	
												<div class="form-group">
														<div class="col-md-5">
															<label class="col-md-6 control-label" for="selectbasic">Ano</label>
							  									<div class="col-md-6">
							    										<select id="city" required name="data" class="form-control">
											      								<option value=""></option>
											        							<c:forEach items="${dataRealizacao}" var="date">
											        								
											        							
											        								<option value="${date}">${date}</option>
															          				
											          							</c:forEach>
										          						</select>
							  									</div>
							  							</div>
							  							<div class="col-md-5">
															<label class="col-md-6 control-label" for="selectbasic">Semestre</label>
							  									<div class="col-md-6">
							    										<select id="selectbasic" required name="semestre" class="form-control">
							    											<option value=""></option>
							      											<option value="1">1°</option>
							      											<option value="2">2°</option>
							    										</select>
							  									</div>
							  							</div>
							  							<button class="btn btn-default"  type="submit"  id="btnSalvar">
												               <em class="glyphicon glyphicon-search"></em>
												        </button> 
							  					</div>		
					  					</form>
					  			</div>				
			  			<br>


				<c:if test="${not empty realizacaoProvaList}">
			  					<div class="row">
			    					<div class="col-md-8 col-md-offset-2">
			      					<table class="table table-bordered">
									          <thead>
									                <tr>
									                      <th>Data de Realização da Prova</th>
									                      <th>Visualizar Respostas do Candidato</th>
									                     
									                      
									                </tr>
									          </thead>
									          <tbody>
									            <c:forEach items="${realizacaoProvaList}" var="realizacaoProva">
									                <tr class="success">
										                  <td><fmt:formatDate pattern="dd/MM/yyyy" value="${realizacaoProva.dataRealizacao}" /></td>
										                  <td>
											                  <form action=<c:url value="/prova/${realizacaoProva.id}/1"/> method="get">		
											                  	 	<button class="btn btn-default"  type="submit"  id="btnSalvar">
												                         <em class="glyphicon glyphicon-eye-open"></em> Visualizar
												                     </button> 
											                  </form>    
										                  </td>

									                  
									                </tr>
									              </c:forEach>  
									          </tbody>
			      					</table>
			    					</div>
			 					</div>
			 				</c:if>	
			 			</div>
						
				</div>
					
				</div>
		</div>
		
	</div>