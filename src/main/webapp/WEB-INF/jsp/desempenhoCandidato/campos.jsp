<div class="row">
		<div class="col-md-12">
				<div class="form-group">
								<c:choose>
									<c:when test="${escolhaDeCursoDoCandidato.opcaoCandidato == 'PRIMEIRA_OPCAO_DE_CURSO'}">
											<c:set var="primeiraOpcao" value="checked"></c:set>
																		 
									</c:when>
									<c:when test="${escolhaDeCursoDoCandidato.opcaoCandidato == 'SEGUNDA_OPCAO_DE_CURSO'}">
											<c:set var="segundaOpcao" value="checked"></c:set>
																		 
									</c:when>
									<c:when test="${escolhaDeCursoDoCandidato.opcaoCandidato == 'OPCAO_ADMINISTRADOR'}">
											<c:set var="administrador" value="checked"></c:set>
																		 
									</c:when>
									<c:otherwise>
											<c:set var="administrador" value=""></c:set>
																		
									</c:otherwise>
								</c:choose>
  								
  									<div align="center" class="col-md-4 radio">
    									<label for="radios-0">
      									<input name="escolhaDeCursoDoCandidato.opcaoCandidato" <c:out value="${primeiraOpcao}"/> id="radios-0" value="PRIMEIRA_OPCAO_DE_CURSO" type="radio">1° Opção do Candidato</label>
									</div>
  									<div align="center" class="col-md-4 radio">
    									<label for="radios-0">
      									<input name="escolhaDeCursoDoCandidato.opcaoCandidato" <c:out value="${segundaOpcao}"/> id="radios-0" value="SEGUNDA_OPCAO_DE_CURSO" type="radio">2° Opção do Candidato</label>
									</div>
									<div align="center" class="col-md-4 radio">
    									<label for="radios-0">
      									<input name="escolhaDeCursoDoCandidato.opcaoCandidato" <c:out value="${administrador}"/> id="radios-0" value="OPCAO_ADMINISTRADOR" type="radio">Opção do Administrador</label>
									</div>
  								
				</div>
		</div>
</div>

<div class="row">
		<div class="col-md-12">
			<div class="form-group">
  								<label class="col-md-1 control-label" for="selectbasic">Fatec</label>
  								<div class="col-md-3">
    								<select id="selectbasic" name="escolhaDeCursoDoCandidato.turma.fatec.id" class="form-control" required>
                  							<option selected value=""></option>
	        								<c:forEach items="${fatecList}" var="fatec">
	        								
		        								<c:choose>
														<c:when test="${escolhaDeCursoDoCandidato.turma.fatec.id == fatec.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
												</c:choose>
		        								
	        									<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
					          				
	          							  </c:forEach>
                  							
                					</select>
  								</div>

  								<label class="col-md-1 control-label" for="selectbasic">Curso</label>
  								<div class="col-md-4">
    								<select id="selectbasic" name="escolhaDeCursoDoCandidato.turma.curso.id" class="form-control" required>
                  							<option selected value=""></option>
	        								<c:forEach items="${cursoList}" var="curso">
	        										<c:choose>
														<c:when test="${escolhaDeCursoDoCandidato.turma.curso.id == curso.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
													</c:choose>
	        									
			        								
	        										<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
					          				
	          							   </c:forEach>
                  							
                						</select>
  								</div>
  								<label class="col-md-1 control-label" for="selectbasic">Período</label>
  								<div class="col-md-2">
    								<select id="selectbasic" name="escolhaDeCursoDoCandidato.turma.periodo" class="form-control" required>
                                       <option selected value=""></option>
	        								<c:forEach items="${periodoList}" var="periodo">
	        									<c:choose>
														<c:when test="${escolhaDeCursoDoCandidato.turma.periodo == periodo}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
													</c:choose>
	        								
	        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
					          				
	          							</c:forEach>
                                    </select>
  								</div>
			</div>
		</div>
	</div>
	
	<c:import url="modal.jsp"></c:import>
	
	<div class="row">

		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title" align="center">
						Selecione o ano e semestre que deseja consultar para saber quais candidatos estariam aprovados
					</h3>
				</div>
				<div class="panel-body">
						<%-- <c:choose>
									<c:when test="${tipoLista == 'GERAL'}">
											<c:set var="geral" value="checked"></c:set>
																		 
									</c:when>
									<c:when test="${tipoLista == 'CONVOCADOS'}">
											<c:set var="convocados" value="checked"></c:set>
																		 
									</c:when>
									<c:when test="${tipoLista == 'SUPLENTES'}">
											<c:set var="suplentes" value="checked"></c:set>
																		 
									</c:when>
									<c:otherwise>
											<c:set var="suplentes" value=""></c:set>
																		
									</c:otherwise>
								</c:choose> --%>
					<%-- 	<div class="row">
									<div align="center" class="col-md-4 radio">
    									<label for="radios-0">
      									<input name="tipoLista" <c:out value="${geral}"/> id="radios-0" value="GERAL" type="radio">Lista de classificação Geral</label>
									</div>
  									<div align="center" class="col-md-4 radio">
    									<label for="radios-1">
      									<input name="tipoLista" <c:out value="${convocados}"/>  id="radios-1" value="CONVOCADOS" type="radio">Lista de Convocados</label>
									</div>
									<div align="center" class="col-md-4 radio">
    									<label for="radios-1">
      									<input name="tipoLista" <c:out value="${suplentes}"/> id="radios-1" value="SUPLENTES" type="radio">Lista de Suplentes</label>
									</div>
						</div> --%>
						<br>
						<br>
						<div class="row">
							<div class="form-group">
									<div align="center" class="col-md-4">
  											<label class="col-md-4 control-label" for="selectbasic">Ano</label>
  											<div class="col-md-6">
    												<select id="selectbasic" name="notaCorte.ano" class="form-control">
      													<option selected value=""></option>
      													<c:forEach items="${anosList}" var="ano">
					        									<c:choose>
																	<c:when test="${ano == notaCorte.ano}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															   </c:choose>
					        								
					        								<option <c:out value="${teste}"/> value="${ano}">${ano}</option>
									          				
					          							</c:forEach>
    												</select>
  											</div>
  									</div>

  									<div align="center" class="col-md-4">
  										<label class="col-md-4 control-label" for="selectbasic">Semestre</label>
  										<div class="col-md-6">
    										<select id="selectbasic" name="notaCorte.semestre" class="form-control" required>
												     <option selected value=""></option>
													 <c:forEach items="${semestreList}" var="semestre">
													 		<c:choose>
																	<c:when test="${semestre == notaCorte.semestre}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															   </c:choose>
													        								
													   		<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
																	          				
													</c:forEach>
										</select>
  										</div>
  									</div>
  							
							</div>


						</div>
							

				</div>
				
			</div>
		</div>
	</div>
	
	<div class="row">
		
	        	<div class="form-group">
	        	
	             <div class="col-md-6"> 
	            	<label class="col-md-7 col-md-offset-1 control-label" for="selectbasic">Candidatos que fizeram a simulação do dia</label>
	                <div  class="col-md-3 input-group date" >
	                    <input id="data1" type="text" class="form-control" value="${datainicio}" name="datainicio" />
	                   <!--  <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-calendar"></span>
	                    </span> -->
	                </div>
	      </div>  

	      
	          	<div class="col-md-6"> 
	                
	                	<label class="col-md-2 col-md-pull-1 control-label" for="selectbasic">até o dia </label>
	                	<div  class=" col-md-3 col-md-pull-1 input-group date" >
	                    	<input id="data2" type="text" class="form-control" name="datafim" value="${datafim}"/>
	                    	<!-- <span class="input-group-addon">
	                        	<span class="glyphicon glyphicon-calendar"></span>
	                    	</span> -->
	                	</div>
	             </div>     
        	
             

        </div>
    </div>  