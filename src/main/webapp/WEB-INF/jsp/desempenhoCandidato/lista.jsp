<div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
          <thead>
                <tr>
                      <th><div align="center">Situação</div></th>
                      <th><div align="center">Nome do Candidato</div></th>
                      <th><div align="center">Nota</div></th>
                      <th><div align="center">Data de Realização da Prova</div></th>
                      <th><div align="center">Visualizar Respostas do Candidato</div></th>

                </tr>
          </thead>
          <tbody>
          <c:forEach items="${realizacaoProvaList}" var="realizacaoProva">
                <tr class="success">
                  <td align="center">
                  
                
	                  		<c:choose>
								<c:when test="${notaCorte.notaCorteConvocados !=0  and realizacaoProva.notaProva >= notaCorte.notaCorteConvocados}">
										<c:set var="teste" value="Convocados"></c:set>
																		 
								</c:when>
								<c:when test="${notaCorte.notaCorteSuplentes != 0  and realizacaoProva.notaProva >= notaCorte.notaCorteSuplentes and realizacaoProva.notaProva < notaCorte.notaCorteConvocados}">
										<c:set var="teste" value="Suplentes"></c:set>
																		 
								</c:when>
								<c:when test="${notaCorte.notaCorteConvocados == null and notaCorte.notaCorteSuplentes == null }">
										<c:set var="teste" value="Nota de Corte Inexistente"></c:set>
																		 
								</c:when>
								<c:otherwise>
										<c:set var="teste" value="Reprovado"></c:set>
																		
								</c:otherwise>
							</c:choose>
							
								 <c:out value="${teste}"/>
               
                 
                                   
                  </td>
                  <td align="center">${realizacaoProva.candidato.nome}</td>
                  <td align="center">
                  		<form action=<c:url value="/candidato/${realizacaoProva.npc}/${realizacaoProva.candidato.id}/${realizacaoProva.id}/${curso.id}"/> method="post">		
                  			<button type="submit" class="btn btn-link">${realizacaoProva.notaProva}</button>
                  		</form>    
                  </td>
                  <td align="center"><fmt:formatDate pattern="dd/MM/yyyy" value="${realizacaoProva.dataRealizacao}" /></td>
                  <td align="center">
                      <form action=<c:url value="/prova/${realizacaoProva.id}/1"/> method="get">		
							<button class="btn btn-default"  type="submit"  id="btnSalvar">
								<em class="glyphicon glyphicon-eye-open"></em> Visualizar
							</button> 
						</form>    
                     

                  </td>
                </tr>
            </c:forEach>   
          </tbody>
      </table>
    </div>
  </div>