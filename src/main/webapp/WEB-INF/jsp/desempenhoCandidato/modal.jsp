<div class="col-md-12">
			<a id="modal-497612" href="#modal-container-497612" role="button" class="btn pull-right" data-toggle="modal">
				<button class="btn btn-default" type="button">
					<em class="glyphicon glyphicon-question-sign"></em> Ajuda
				</button> 
			</a>
			
			<div class="modal fade" id="modal-container-497612" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
							<div class="modal-header">
								 
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									×
								</button>
								<h4 class="modal-title" id="myModalLabel">
									<em class="glyphicon glyphicon-question-sign"></em> Ajuda
								</h4>
							</div>
							<div class="modal-body">
								<div class="row">
										<div class="col-md-12">
												<dl>
													<dt>1° Opção dos Candidatos</dt>
														<dd>Selecione esta opção para consultar a lista de convocados, suplentes ou classificação geral dos candidatos que escolheram a fatec, curso e período selecionados abaixo como primeira opção</dd>
													<dt>2° Opção dos Candidatos</dt>
														<dd>Selecione esta opção para consultar a lista de convocados, suplentes ou classificação geral dos candidatos que escolheram a fatec, curso e período selecionados abaixo como segunda opção</dd>
														
													<dt>Opção do Administrador</dt>
														<dd>Selecione esta opção para consultar a lista de convocados, suplentes ou classificação geral da fatec, curso e período escolhidos abaixo pelo administrador</dd>
													
												</dl>
										</div>
								</div>
							</div>
							<div class="modal-footer">
								 
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button> 
								
							</div>
					</div>
					
				</div>
				
			</div>
			
</div>