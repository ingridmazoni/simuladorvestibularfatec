 <div class="col-md-4">
      <table class="table table-bordered table-condensed table-responsive">
        <thead>
          <tr>
            <th><div align="center">Disciplinas</div></th>
            <th><div align="center">Quantidade de Acertos</div></th>
            <th><div align="center">Notas</div></th>
            
          </tr>
        </thead>
        <tbody>
        
        <c:forEach items="${listaMaterias}" var="notaMateria" varStatus="d">
        	<c:set value="${d.count}" var="i" />
        	
				  <tr  class="success">
		            <td class="col-md-1" align="center">${notaMateria.d.nome} <input id="id" type="hidden" name="quantAcertos[${i}].d.nome" value="${notaMateria.d.id}"></td>
		            		
		            <td class="col-md-1"><input id="textinput" maxlength="1" name="quantAcertos[${i}].quantAcertosMateria" class="form-control input-md" type="number" value="${notaMateria.quantAcertosMateria}"></td>
				          
				       <c:if test="${curso.materiasPesoDois[0].id == notaMateria.d.id or curso.materiasPesoDois[1].id == notaMateria.d.id }">
							
							 <td class="col-md-1" align="center">${notaMateria.quantAcertosMateria*2}</td>
																	 
				       </c:if>
				        <c:if test="${curso.materiasPesoDois[0].id != notaMateria.d.id and curso.materiasPesoDois[1].id != notaMateria.d.id}">
							
							 <td class="col-md-1" align="center">${notaMateria.quantAcertosMateria}</td>
																	 
				       </c:if>
						
		            
		          </tr>					                
		</c:forEach>  
       
        
        </tbody>
      </table>
      
    </div>