<div class="modal fade" id="modal-container-954435" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
              <h4 class="modal-title" id="myModalLabel">
                Como calcular o NPC <em class="glyphicon glyphicon-question-sign"></em>
              </h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                          <dl>
                            <dt>O que é NPC ?</dt>
                                <dd>É a quantidade de respostas certas das 10 questões de peso 2 multiplicadas por 2 , somado ao número de respostas certas das 44 questões compostas por matérias de peso 1 </dd>
                            
                          </dl>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
        </div>
      </div>
      


      <div class="modal fade" id="modal-container-954436" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
              <h4 class="modal-title" id="myModalLabel">
                Como calcular o NFA <em class="glyphicon glyphicon-question-sign"></em>
              </h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                           <dl class="dl-horizontal">
                                  <dt>NF - </dt>
                                        <dd>Nota final obtida pelo candidato</dd>

                                  
                             
                                 <dt>A - </dt>
                                      <dd>( valor de 3% ) para o candidato que se declarar afrodescendente </dd>
                                  
                              
                                  <dt>P - </dt>
                                        <dd>( valor de 10% ) para o candidato que se declarar ter cursado todas as séries do ensino médio em instituições públicas</dd>
                                       
                                   <dd>* O valor máximo do NFA é 100</dd>         

                            </dl>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
        </div>
      </div>



      <div class="modal fade" id="modal-container-954437" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
              <h4 class="modal-title" id="myModalLabel">
                Como calcular a nota final da prova da fatec <em class="glyphicon glyphicon-question-sign"></em>
              </h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                          <dl class="dl-horizontal">
                                  <dt>N - </dt>
                                        <dd>Nota da parte objetiva da prova da Fatec</dd>
                                  
                             
                                 <dt>R - </dt>
                                      <dd>Nota obtida na redação ( valor máximo 100) </dd>
                                  
                              
                                  <dt>NF - </dt>
                                        <dd>Nota final da prova da Fatec </dd>
                                                 
                            </dl>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="modal-container-954438" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
              <h4 class="modal-title" id="myModalLabel">
                Como calcular a nota da parte objetiva da prova da Fatec, utilizando a nota do ENEM <em class="glyphicon glyphicon-question-sign"></em>
              </h4>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                          <dl class="dl-horizontal">
                                  <dt>Se ENEM  >  P</dt>
                                        <dd>N = ( 4 * P + 1 * ENEM ) / 5 </dd>
                                  
                             
	                               <dt>Se ENEM  &lt =  P</dt>
	                                      <dd>N = P </dd>
                                  
                              
                                 
                                                 
                            </dl>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
        </div>
      </div>
