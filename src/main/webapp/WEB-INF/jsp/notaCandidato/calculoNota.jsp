  <div class="col-md-8">
            <div class="panel panel-success">
              <div class="panel-heading">
                <h3 class="panel-title" align="center">
                  Cálculo da Nota da Prova 
                </h3>
              </div>
              <div class="panel-body">
                    <div class="row">
                         <label class="col-md-4 control-label" for="textinput">NPC</label> 
                         <div class="col-md-4">
                            <input id="textinput" name="textinput" readonly class="form-control input-md" type="text" value="${npc}">
                            <span class="help-block">Nota Ponderada do Total das Questões</span>  
                           
                         </div>
                          <a id="modal-954435" href="#modal-container-954435" role="button" class="btn active btn-default col-md-1" data-toggle="modal">
                          <em class="glyphicon glyphicon-question-sign"></em></a>
                   </div>
                   <br>
                    <div class="row">
                         <label class="col-md-4 control-label" for="textinput">P = 100 * NPC / 64</label> 
                         <div class="col-md-4">
                       <%--   <fmt:parseNumber var="npc2" integerOnly="true" type="number" value="${npc}" /> --%>
                         	<c:set var="P" value="${(npc*100)/64}"/>
                            <input id="textinput" name="textinput" class="form-control input-md" readonly type="text" value="${P}">
                            <span class="help-block">Nota das Questões Objetivas da Prova da Fatec</span>  
                           
                         </div>
                   </div>
                   <br>
                   <div class="row">
                        <div class="col-md-12">
                              <div class="page-header">
                                <h3> <small>O candidato que prestou o enem poderá utilizar a nota da parte objetiva da prova para compor a nota final da prova da fatec</small></h3>
                              </div>
                        </div>
                  </div>



                    <br>
                    <div class="row">
                         <label class="col-md-4 control-label" for="textinput">ENEM</label> 
                         <div class="col-md-4">
                            <input id="textinput" readonly name="textinput" class="form-control input-md" type="text" value="${candidato.notaEnem}">
                             <span class="help-block">Nota da parte objetiva da prova do ENEM</span>  
                           
                         </div>
                   </div>
                   <br>
                    <div class="row">
                         <label class="col-md-4 control-label" for="textinput">N = ( 4 * P + 1 * ENEM ) / 5 </label> 
                         <div class="col-md-4">
                         	<c:set var="notaEnem" value="${(candidato.notaEnem*100)/1000}"/>
                         	<c:choose>
								<c:when test="${notaEnem > P}">
										<c:set var="N" value="${((4*P)+(1*notaEnem))/5}"/>
																		 
								</c:when>
								<c:otherwise>
										<c:set var="N" value="${P}"/>
																		
								</c:otherwise>
							</c:choose>
                         
                         	 
                            <input id="textinput" readonly name="textinput" class="form-control input-md" type="text" value="${N}">
                            <span class="help-block">Nota da parte objetiva da prova da Fatec</span> 
                           
                         </div>
                        <a id="modal-954438" href="#modal-container-954438" role="button" class="btn active btn-default col-md-1" data-toggle="modal">
                        <em class="glyphicon glyphicon-question-sign"></em></a>
                   </div>
                  

                   <br>
                    <div class="row">
                         <label class="col-md-4 control-label" for="textinput">NF = (8 * N + 2 * R) / 10</label> 
                         <div class="col-md-4">
                         	<c:choose>
									<c:when test="${realizacaoProva.notaRedacao>= 0 and realizacaoProva.notaRedacao<= 100}">
											<c:set var="NF" value="${((8*N) + (2 * realizacaoProva.notaRedacao))/10}"/>
																													 
									</c:when>
									<c:otherwise>
											<c:set var="NF" value="${(8*N + 2 * 0)/10}"/>
																													
									</c:otherwise>
							</c:choose>
                        <input id="textinput" name="textinput" readonly class="form-control input-md" type="text" value="${NF}">
                             <span class="help-block">Nota final da prova da Fatec</span> 
                           
                         </div>
                         <a id="modal-954437" href="#modal-container-954437" role="button" class="btn active btn-default col-md-1" data-toggle="modal">
                         <em class="glyphicon glyphicon-question-sign"></em></a>
                   </div>
                   <div class="row">
                        <div class="col-md-12">
                              <div class="page-header">
                                <h3> <small>Para o candidato que utilizar o sistema de pontuação acrescida, a sua nota final será obtida pela fórmula abaixo</small></h3>
                              </div>
                        </div>
                  </div>
                    <br>
                    <div class="row">
                    	
                    	<c:choose>
							<c:when test="${candidato.pontuacaoAcrescidaAfrodescendente eq true}">
									<c:set var="AF" value="${NF*3/100}"/>
																	 
							</c:when>
							<c:otherwise>
									<c:set var="AF" value="0"/>
																	
							</c:otherwise>
						</c:choose>
                    	 	
                         <label class="col-md-2 col-md-offset-2 control-label" for="textinput">Afrodescendencia</label> 
                         <div class="col-md-2">
                            <input id="textinput" name="textinput" readonly class="form-control input-md" type="text" value="${AF}">
                           
                         </div>
                         
                         <c:choose>
							<c:when test="${candidato.pontuacaoAcrescidaEscolaridadePublica eq true}">
									<c:set var="EP" value="${NF*10/100}"/>
																	 
							</c:when>
							<c:otherwise>
									 <c:set var="EP" value="0"/>
																	
							</c:otherwise>
						</c:choose>
                   
                         <label class="col-md-3 control-label" for="textinput">Escolaridade Pública</label> 
                         <div class="col-md-2">
                            <input id="textinput" name="textinput" readonly class="form-control input-md" type="text" value="${EP}">
                           
                         </div>
                   </div>
                   <br>
                     <div class="row">
                         <label class="col-md-5 col-md-offset-1 control-label" for="textinput">NFA = NF ( 1 + A + P )</label> 
                         <div class="col-md-2">
                            <input id="textinput" name="textinput" readonly class="form-control input-md" type="text" value="${NF+AF+EP}">
                           
                         </div>
                          <a id="modal-954436" href="#modal-container-954436" role="button" class="btn active btn-default col-md-1" data-toggle="modal">
                         <em class="glyphicon glyphicon-question-sign"></em></a>
                         
                   </div>
               
              </div>
              
            </div>
          </div>