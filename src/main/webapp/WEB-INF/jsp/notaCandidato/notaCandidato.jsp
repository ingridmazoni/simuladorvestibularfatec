<c:import url="subtitulo.jsp"></c:import>

<form class="form-horizontal" role="form"  method="post" action=<c:url value="/candidato/calculoNotaCandidato" /> id="meu_form">

	<div class="row">
    		<div class="col-md-9">
    				<div class="form-group">
      						<label class="col-md-4 control-label" for="">Selecione a data de realização da prova</label>  
          						 <div class="col-md-3">
                              <select id="selectbasic" name="realizacaoProva.id" class="form-control" >
                  							<option value=""></option>
	        								<c:forEach items="${realizacaoProvaList}" var="realizacaoProva2">
	        									<c:choose>
													<c:when test="${realizacaoProva2.id == realizacaoProva.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
											</c:choose>
	        								
			        						<option value="${realizacaoProva2.id}" <c:out value="${teste}"/>><fmt:formatDate pattern="dd/MM/yyyy" value="${realizacaoProva2.dataRealizacao}" /></option>
					          				</c:forEach>
                  							
                			</select>
                        </div>
                        
    				</div>
            </div>
      
      
      <div class="col-md-3 pull-right">
          <button id="btnSalvar" class="btn btn-success btn-lg active btn-block" type="submit" name="_method" value="post">
           Calcular Nota do Candidato
          </button>
        </div>
      

    	</div>	

      <div class="row">
          <div class="col-md-10">
              <div class="form-group">
                    <label class="col-md-4 control-label" for="">Selecione o curso desejado para calcular a nota</label>  
                         <div class="col-md-4">
                                <select id="selectbasic" name="curso.id" class="form-control">
                  							<option value=""></option>
                  							
	        								<c:forEach items="${cursoList}" var="curso2">
		        								<c:choose>
														<c:when test="${curso2.id == curso.id}">
															<c:set var="teste" value="selected"></c:set>
																		 
														</c:when>
														<c:otherwise>
															<c:set var="teste" value=""></c:set>
																		
														</c:otherwise>
												</c:choose>
			        								<option value="${curso2.id}" <c:out value="${teste}"/>>${curso2.nome}</option>
					          				</c:forEach>
                  							
                					</select>
                          </div>
              </div>
          </div>
      </div>  
      <div class="row">
          <div class="col-md-9">
              <div class="form-group">
                    <label class="col-md-3 control-label" for="">Digite a nota da redação</label>  
                    <div class="col-md-4">
      					<input id="nome" name="realizacaoProva.notaRedacao" type="text" placeholder="Digite a nota da redação" value="<fmt:formatNumber type="number" maxIntegerDigits="3" value="${realizacaoProva.notaRedacao}" />" class="form-control input-md">
      					 <span class="help-block">A nota deve ser entre 0 e 100</span>
        			</div>
              </div>
          </div>
      </div>  
      <br>
      
       <div class="row">
       
	       	<c:import url="tabelaDeNotas.jsp"></c:import>
	       
	       	<c:import url="calculoNota.jsp"></c:import>
       </div>
       
       <c:import url="modais.jsp"></c:import>
       
       <c:import url="consultaNotasAnteriores.jsp"></c:import>
       
  </form>    
       
       
       
   