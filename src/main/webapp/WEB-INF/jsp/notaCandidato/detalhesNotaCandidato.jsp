<form class="form-horizontal" role="form" method="post" action=<c:url value="/voltarPesquisa" /> id="meu_form">
<div class="row">
       <h1 class="media-heading" align="center">Login: ${candidato.usuario.login}</h1>
       <h1 class="media-heading" align="center">Nome: ${candidato.nome}</h1>
	       	<c:import url="tabelaDeNotas.jsp"></c:import>
	       
	       	<c:import url="calculoNota.jsp"></c:import>
       </div>
       
       <c:import url="modais.jsp"></c:import>
       
       <div class="row botao-linha">
    		<div class="col-xs-4 col-sm-2 pull-right">
    			<button id="btnSalvar" class="btn btn-success btn-lg active btn-block" type="submit">
    				Voltar para página de Pesquisa
    			</button>
    		</div>
    	</div>
    	
</form>
       
       