
 <div class="row">
        <div class="col-md-12">
              <div class="panel panel-success">
                  <div class="panel-heading" align="center">
                        <h3 class="panel-title">Selecione todos os campos abaixo para consultar a nota de corte</h3>
                  </div>
                  <div class="panel-body">
                        <div class="row">
                          <div class="col-md-4">
                          		<div class="row">	
	                          			<label class="col-md-2 control-label" for="">Curso</label>  
		                         		<div class="col-md-10">
		                                <select id="selectbasic" name="notaCorte.turma.curso.id" class="form-control" >
		                  							<option selected value=""></option>
			        								<c:forEach items="${cursoList}" var="curso">
			        										<c:choose>
																	<c:when test="${curso.id == notaCorte.turma.curso.id}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															</c:choose>
			        										
					        								<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
							          				</c:forEach>
		                  							
		                					</select>
		                          	    </div>
	                          	</div>
                          			  <br>
                                 
                                        <div class="row">
                                            <label class="col-md-2 control-label" for="selectbasic">Fatec</label>
                                            <div class="col-md-10">
	                                                 <select id="selectbasic" name="notaCorte.turma.fatec.id" class="form-control">
			                  							<option selected value=""></option>
				        								<c:forEach items="${fatecList}" var="fatec">
				        									<c:choose>
																	<c:when test="${fatec.id == notaCorte.turma.fatec.id}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															</c:choose>
					        								
				        									<option <c:out value="${teste}"/>  value="${fatec.id}">${fatec.nome}</option>
								          				
				          							  </c:forEach>
			                  							
			                						</select>
                                            </div>
                                        </div>
                                      <br>
                                   
                                        <div class="row">
                                            <label class="col-md-2 control-label" for="selectbasic">Horário</label>
                                            <div class="col-md-7">
                                                 	<select id="selectbasic" name="notaCorte.turma.periodo" class="form-control">
				                                       <option selected value=""></option>
					        								<c:forEach items="${periodoList}" var="periodo">
					        									<c:choose>
																	<c:when test="${periodo == notaCorte.turma.periodo}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															</c:choose>
					        								
					        								<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
									          				
					          							</c:forEach>
				                                    </select>
                                            </div>
                                        </div>
                                     
                                      <br>
                                        <div class="row">
                                            <label class="col-md-2 control-label" for="selectbasic">Ano</label>
                                            <div class="col-md-7">
                                                 <select id="selectbasic" name="notaCorte.ano" class="form-control">
				                                       <option selected value=""></option>
					        								<c:forEach items="${anosList}" var="ano">
					        									<c:choose>
																	<c:when test="${ano == notaCorte.ano}">
																		<c:set var="teste" value="selected"></c:set>
																					 
																	</c:when>
																	<c:otherwise>
																		<c:set var="teste" value=""></c:set>
																					
																	</c:otherwise>
															   </c:choose>
					        								
					        								<option <c:out value="${teste}"/> value="${ano}">${ano}</option>
									          				
					          							</c:forEach>
				                                    </select>
                                            </div>
                                        </div>
                                         
                                        <br>
                                 
                                         <div class="row">
                                            <label class="col-md-3 control-label" for="selectbasic">Semestre</label>
                                            <div class="col-md-7">
                                                  <select id="selectbasic" name="notaCorte.semestre" class="form-control">
												             <option value=""></option>
													        	<c:forEach items="${semestreList}" var="semestre">
													        		<c:choose>
																		<c:when test="${semestre == notaCorte.semestre}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																   </c:choose>
													        								
													        		<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
																	          				
													          	</c:forEach>
												    </select>
                                            </div>
                                        </div>
                                         <br>
                                        <div class="row botao-linha">
                                          <div class="col-md-9 col-md-offset-2">
                                            <button id="btnSalvar" class="btn btn-success active" type="submit">
                                              Consultar Nota de Corte
                                            </button>
                                          </div>
                                        </div>
                                   
                                </div> 


                                	<c:import url="tabela.jsp"></c:import>

                          </div>
                        </div>    

                  </div>
                
              </div>
        </div>
        
     