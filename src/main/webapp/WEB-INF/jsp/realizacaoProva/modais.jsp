<div class="modal fade" id="modal-pergunta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          <img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemPergunta}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
   
<div class="modal fade" id="modal-alternativaA" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          	<img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaA}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
 <div class="modal fade" id="modal-alternativaB" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          	<img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaB}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
 <div class="modal fade" id="modal-alternativaC" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          	<img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaC}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
 <div class="modal fade" id="modal-alternativaD" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          	<img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaD}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
 <div class="modal fade" id="modal-alternativaE" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
               
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
              </button>
             
            </div>
            <div class="modal-body" style="height:100%; max-height:100%; width:100%; max-width:100%;">
                    <div class="row">
                        <div align="center" class="col-md-12">
                          	<img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaE}" />" class="img-thumbnail">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
               
              <button type="button" class="btn btn-default" data-dismiss="modal">
                Close
              </button> 
              
            </div>
          </div>
   </div>
 </div>
          
     