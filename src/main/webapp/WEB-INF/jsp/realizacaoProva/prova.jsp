<c:import url="subtitulo.jsp"></c:import>

<form class="form-horizontal" action="<c:url value="/questaovestibular/salva" />" method="post">

	<c:import url="alternativas.jsp"></c:import>
	
	
	<div class="row botao-linha">
		<div class="col-md-12">
			
			<div class="col-md-2 col-md-offset-8">
				<button id="btnSalvar" class="btn btn-success btn-lg active" type="submit" name="_method" value="post">
					Confirmar Resposta
					
				</button>
			</div>
		</div>	
		
	</div>

</form>