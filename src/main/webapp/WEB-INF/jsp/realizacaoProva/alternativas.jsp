<c:import url="modais.jsp"></c:import>
<input id="id" type="hidden" name="questoesprova.questaoVestibular.id" value="${questaovestibular.id}">
<div class="row">
		<div class="col-md-10 col-md-offset-1">
<h4 class="media-heading">${questaovestibular.semestreVestibular} semestre de ${questaovestibular.anoVestibular} - ${questaovestibular.conteudoProgramatico.disciplina.nome}</h4>
			
			
			<div class="media well">
						
				<div class="media-body">
		
						<div class="col-md-1">
								<input id="id" type="hidden" name="contador" value="${contador}">
								   ${contador} ) 
								   
								   	
						</div>
						<c:if test="${not empty questaovestibular.imagemPergunta}">
								<div class="col-md-4" align="center">
									<a id="modal-pergunta" href="#modal-pergunta" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemPergunta}" />" width="300" height="300" class="img-thumbnail"></a>
								</div>
						</c:if>	
						<div class="col-md-7">
							<h4>
						${questaovestibular.pergunta}
							</h4>
						</div>
						

				</div>
			</div>

			<div class="media well">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="questoesprova.respostaDoCandidato" id="radios-1" value="A" type="radio" required>A
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaA}">	
						<div class="col-md-4" >
							
								<a id="modal-alternativaA" href="#modal-alternativaA" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaA}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					<div class="col-md-7">
						<h4>
					${questaovestibular.alternativaA}
						</h4>
					</div>
						

				</div>
			</div>
			<div class="media well">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="questoesprova.respostaDoCandidato" id="radios-1" value="B" type="radio" required>B
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaB}">			
						<div class="col-md-4" align="center">
											
								<a id="modal-alternativaB" href="#modal-alternativaB" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaB}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					<div class="col-md-7">
						<h4>
					${questaovestibular.alternativaB}
						</h4>
					</div>
					

				</div>
			</div>
			<div class="media well">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="questoesprova.respostaDoCandidato" id="radios-1" value="C" type="radio" required>C
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaC}">	
						<div class="col-md-4" align="center">
						
								<a id="modal-alternativaC" href="#modal-alternativaC" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaC}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-7">
						<h4>
				${questaovestibular.alternativaC}
						</h4>
					</div>
					

				</div>
			</div>
			<div class="media well">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="questoesprova.respostaDoCandidato" id="radios-1" value="D" type="radio" required>D
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaD}">	
						<div class="col-md-4" align="center">
							
								<a id="modal-alternativaD" href="#modal-alternativaD" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaD}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					<div class="col-md-7">
						<h4>
					${questaovestibular.alternativaD}
						</h4>
					</div>
					

				</div>
			</div>
			<div class="media well">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="questoesprova.respostaDoCandidato" id="radios-1" value="E" type="radio" required>E
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaE}">	
						<div class="col-md-4" align="center">
							
								<a id="modal-alternativaE" href="#modal-alternativaE" role="button" data-toggle="modal"><img alt="Clique para ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaE}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-7">
						<h4>
					${questaovestibular.alternativaE}
						</h4>
					</div>
					

				</div>
			</div>

			


		</div>

	</div>