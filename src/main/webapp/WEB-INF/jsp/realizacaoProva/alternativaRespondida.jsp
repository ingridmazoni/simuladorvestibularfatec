<c:import url="modais.jsp"></c:import>
<div class="row">
		<div class="col-md-10 col-md-offset-1">
<h4 class="media-heading" align="center">${questaovestibular.semestreVestibular} semestre de ${questaovestibular.anoVestibular} - ${questaovestibular.conteudoProgramatico.disciplina.nome}</h4>
<h2 class="media-heading" align="center">${questaovestibular.conteudoProgramatico.programaProva}</h2>


			<div class="media well">
						
				<div class="media-body">
					
					<div class="col-md-1">
							 	<h1> ${questoesProva.sequenciaDaQuestaoNaProva}) </h1> 
					</div>
					<c:if test="${not empty questaovestibular.imagemPergunta}">
								<div class="col-md-4" align="center">
									<a id="modal-pergunta" href="#modal-pergunta" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemPergunta}" />" width="300" height="300" class="img-thumbnail"></a>
								</div>
					</c:if>	
									
					<div class="col-md-7">
						<h4>
							${questaovestibular.pergunta}
						</h4>
					</div>
					

				</div>
			</div>
			
			<c:choose>
						<c:when test="${questoesProva.respostaDoCandidato eq 'A'}">
							<c:set var="teste" value="checked"></c:set>
																							 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste" value=""></c:set>
																								
						</c:otherwise>
			</c:choose>	
			<c:choose>
						<c:when test="${questaovestibular.respostaCorreta eq 'A'}">
							<c:set var="teste2" value="background-color:#dff0d8;border-color:#3c763d;color:#3c763d"/>
							<c:set var="teste3" value="glyphicon glyphicon-ok"/>																 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste2" value="background-color:#f2dede;border-color:#a94442;color:#a94442"/>
							<c:set var="teste3" value="glyphicon glyphicon-remove"/>																		
						</c:otherwise>
			</c:choose>	


			<div class="media well" style="<c:out value="${teste2}"/>">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="radios" id="radios-1" value="2" readonly type="radio" <c:out value="${teste}"/>>A
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaA}">	
						<div class="col-md-4" >
							
							 <a id="modal-alternativaA" href="#modal-alternativaA" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaA}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
									
					<div class="col-md-6">
						<h4>
					${questaovestibular.alternativaA}
						</h4>
					</div>
					<div class="col-md-1">
						<h1>
						<strong><em class="<c:out value="${teste3}"/>"></em></strong></h1>
					</div>	

				</div>
			</div>
			
			<c:choose>
						<c:when test="${questoesProva.respostaDoCandidato eq 'B'}">
							<c:set var="teste" value="checked"></c:set>
																							 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste" value=""></c:set>
																								
						</c:otherwise>
			</c:choose>	
			<c:choose>
						<c:when test="${questaovestibular.respostaCorreta eq 'B'}">
							<c:set var="teste2" value="background-color:#dff0d8;border-color:#3c763d;color:#3c763d"/>
							<c:set var="teste3" value="glyphicon glyphicon-ok"/>																 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste2" value="background-color:#f2dede;border-color:#a94442;color:#a94442"/>
							<c:set var="teste3" value="glyphicon glyphicon-remove"/>																		
						</c:otherwise>
			</c:choose>	
			
			<div class="media well" style="<c:out value="${teste2}"/>">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="radios" id="radios-1" readonly value="2" type="radio" <c:out value="${teste}"/> >B
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaB}">			
						<div class="col-md-4" align="center">
											
								<a id="modal-alternativaB" href="#modal-alternativaB" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaB}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-6">
						<h4>
						${questaovestibular.alternativaB}
						</h4>
					</div>
					<div class="col-md-1">
						<h1>
						<strong><em class="<c:out value="${teste3}"/>"></em></strong></h1>
					</div>	

				</div>
			</div>
			<c:choose>
						<c:when test="${questoesProva.respostaDoCandidato eq 'C'}">
							<c:set var="teste" value="checked"></c:set>
																							 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste" value=""></c:set>
																								
						</c:otherwise>
			</c:choose>
			<c:choose>
						<c:when test="${questaovestibular.respostaCorreta eq 'C'}">
							<c:set var="teste2" value="background-color:#dff0d8;border-color:#3c763d;color:#3c763d"/>
							<c:set var="teste3" value="glyphicon glyphicon-ok"/>																 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste2" value="background-color:#f2dede;border-color:#a94442;color:#a94442"/>
							<c:set var="teste3" value="glyphicon glyphicon-remove"/>																		
						</c:otherwise>
			</c:choose>			
			
			<div class="media well" style="<c:out value="${teste2}"/>">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="radios" id="radios-1" readonly value="2" type="radio" <c:out value="${teste}"/>>C
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaC}">	
						<div class="col-md-4" align="center">
						
								<a id="modal-alternativaC" href="#modal-alternativaC" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaC}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-6">
						<h4>
						${questaovestibular.alternativaC}
						</h4>
					</div>
					<div class="col-md-1">
						<h1>
						<strong><em class="<c:out value="${teste3}"/>"></em></strong></h1>
					</div>	

				</div>
			</div>
			
			<c:choose>
						<c:when test="${questoesProva.respostaDoCandidato eq 'D'}">
							<c:set var="teste" value="checked"></c:set>
																							 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste" value=""></c:set>
																								
						</c:otherwise>
			</c:choose>	
			<c:choose>
						<c:when test="${questaovestibular.respostaCorreta eq 'D'}">
							<c:set var="teste2" value="background-color:#dff0d8;border-color:#3c763d;color:#3c763d"/>
							<c:set var="teste3" value="glyphicon glyphicon-ok"/>																 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste2" value="background-color:#f2dede;border-color:#a94442;color:#a94442"/>
							<c:set var="teste3" value="glyphicon glyphicon-remove"/>																		
						</c:otherwise>
			</c:choose>	
			<div class="media well" style="<c:out value="${teste2}"/>">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="radios" id="radios-1" readonly value="2" type="radio" <c:out value="${teste}"/>>D
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaD}">	
						<div class="col-md-4" align="center">
							
						<a id="modal-alternativaD" href="#modal-alternativaD" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaD}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-6">
						<h4>
						${questaovestibular.alternativaD}
						</h4>
					</div>
					<div class="col-md-1">
						<h1>
						<strong><em class="<c:out value="${teste3}"/>"></em></strong></h1>
					</div>	

				</div>
			</div>
			<c:choose>
						<c:when test="${questoesProva.respostaDoCandidato eq 'E'}">
							<c:set var="teste" value="checked"></c:set>
																							 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste" value=""></c:set>
																								
						</c:otherwise>
			</c:choose>	
			<c:choose>
						<c:when test="${questaovestibular.respostaCorreta eq 'E'}">
							<c:set var="teste2" value="background-color:#dff0d8;border-color:#3c763d;color:#3c763d"/>
							<c:set var="teste3" value="glyphicon glyphicon-ok"/>																 
						</c:when>
						<c:otherwise>
						  	<c:set var="teste2" value="background-color:#f2dede;border-color:#a94442;color:#a94442"/>
							<c:set var="teste3" value="glyphicon glyphicon-remove"/>																		
						</c:otherwise>
			</c:choose>	
			<div class="media well" style="<c:out value="${teste2}"/>">
						
				<div class="media-body">
					
					<div class="radio col-md-1">
							    <label for="radios-1">
							      <input name="radios" id="radios-1" readonly value="2" type="radio" <c:out value="${teste}"/>>E
							    </label>
					</div>
					<c:if test="${not empty questaovestibular.imagemAlternativaE}">	
						<div class="col-md-4" align="center">
							
							<a id="modal-alternativaE" href="#modal-alternativaE" role="button" data-toggle="modal"><img alt="Clique pra ampliar a imagem" src="<c:url value="/images/${questaovestibular.imagemAlternativaE}" />" width="300" height="300" class="img-thumbnail"></a>
							
						</div>
					</c:if>	
					
					<div class="col-md-6">
						<h4>
						${questaovestibular.alternativaE}
						</h4>
					</div>
					<div class="col-md-1">
						<h1>
						<strong><em class="<c:out value="${teste3}"/>"></em></strong></h1>
					</div>	

				</div>
			</div>

			


		</div>

	</div>