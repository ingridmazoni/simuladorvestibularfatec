<c:import url="subtitulo.jsp"></c:import>

<c:if test="${fn:contains(usuarioLogado.getUsuario().getPerfil(), 'ADMINISTRADOR')}">
	   <h1 class="media-heading" align="center">Login: ${candidato.usuario.login}</h1>
       <h1 class="media-heading" align="center">Nome: ${candidato.nome}</h1>						
</c:if>


	<c:import url="alternativaRespondida.jsp"></c:import>
	
	
	<div class="row botao-linha">
		<div class="col-md-12">
		<form class="form-horizontal" action="<c:url value="/prova/respondida"/>" method="get">
				<div class="form-group">
				  <label class="col-md-7 control-label" for="selectbasic">Selecione o número da questão para consultar o que foi respondido e a resposta correta</label>
					  <div class="col-md-3">
						    <select id="selectbasic" name="questoesProva.sequenciaDaQuestaoNaProva" class="form-control" required>
						    	<option/>
						     	<c:forEach items="${questoesProvaList}" var="questoesProva">
						      		<option value="${questoesProva.sequenciaDaQuestaoNaProva}">${questoesProva.sequenciaDaQuestaoNaProva} - ${questoesProva.questaoVestibular.conteudoProgramatico.disciplina.nome}</option>
						       </c:forEach>
						    </select>
					  </div>
					 <button class="btn btn-default col-md-1"  type="submit"  id="btnSalvar">
						Ir
					</button> 
					  <input id="id" type="hidden" name="questoesProva.realizacaoProva.id" value="${questoesProva.realizacaoProva.id}">  
					
				</div>
		</form>
		</div>
	</div>		
		

	<div class="row botao-linha">
		<div class="col-md-12">
			
			<c:set var="anterior" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva-1}"/>
			<c:choose>
				    <c:when test="${anterior eq 0}">
				       		<c:set var="anterior" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva+53}"/>
				    </c:when>
					<c:otherwise>
				        	<c:set var="anterior" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva-1}"/>
				    </c:otherwise>
				</c:choose>
				
			<form class="form-horizontal" action="<c:url value="/prova/${questoesProva.realizacaoProva.id}/${anterior}"/>" method="get">
				
			<div class="col-md-2">
				<button id="btnSalvar" class="btn btn-success btn-lg active" type="submit">
					<em class="glyphicon glyphicon-arrow-left"></em>
					Questão Anterior
				</button>
			</div>
			
			</form>
			
			<c:set var="prox" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva+1}"/>
			<c:choose>
				    <c:when test="${prox eq 55}">
				       		<c:set var="prox" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva - 53}"/>
				    </c:when>
					<c:otherwise>
				        	<c:set var="prox" scope="session" value="${questoesProva.sequenciaDaQuestaoNaProva + 1}"/>
				    </c:otherwise>
				</c:choose>
			<form class="form-horizontal" action="<c:url value="/prova/${questoesProva.realizacaoProva.id}/${prox}"/>" method="get">
				<div class="col-md-2 col-md-offset-8">
					<button id="btnSalvar" class="btn btn-success btn-lg active" type="submit">
						Próxima Questão
						<em class="glyphicon glyphicon-arrow-right"></em>
					</button>
				</div>
			</form>
		</div>	
		
			 
							
							
		
	</div>
	<br><br>
	<div class="row">
		<div class="col-md-12" align="center">
			<c:if test="${fn:contains(usuarioLogado.getUsuario().getPerfil(), 'ADMINISTRADOR')}">
										<form class="form-horizontal" role="form" method="post" action=<c:url value="/voltarPesquisa" /> id="meu_form">
											 
									    			<button id="btnSalvar" class="btn btn-success btn-lg active" type="submit">
									    				Voltar para página de Pesquisa
									    			</button>
									    			
										</form>	
			</c:if>
		</div>
	</div>

