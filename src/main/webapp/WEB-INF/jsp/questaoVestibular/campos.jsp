<%-- ${questaoVestibular.id} --%>
<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">
						Selecione qual será a disciplina e o conteúdo programático no qual esta questão se classifica
					</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
  								<%-- <label class="col-md-1 control-label" for="selectbasic">Disciplina</label>
  								<div class="col-md-2">
    								<select id="selectbasic" name="questaoVestibular.conteudoProgramatico.disciplina.id" class="form-control">
      										<option value=""></option>
    										<c:forEach items="${disciplinaList}" var="disciplina">
    											<c:choose>
													<c:when test="${questaoVestibular.conteudoProgramatico.disciplina.id == disciplina.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${disciplina.id}">${disciplina.nome}</option>
		          							</c:forEach>
    								</select>
  								</div> --%>
								<label class="col-md-2 control-label" for="selectbasic">Conteúdo Programático</label>
  								<div class="col-md-7">
    								<select id="selectbasic" name="questaoVestibular.conteudoProgramatico.id" class="form-control">
      										<option value=""></option>
    										<c:forEach items="${conteudoProgramaticoList}" var="conteudoProgramatico">
    											<c:choose>
													<c:when test="${questaoVestibular.conteudoProgramatico.id == conteudoProgramatico.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${conteudoProgramatico.id}">${conteudoProgramatico.disciplina.nome} - ${conteudoProgramatico.programaProva}</option>
		          							</c:forEach>
      										
    								</select>
  								</div>
					</div>
				</div>
				
	</div>




	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a pergunta">Digite a pergunta</label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a pergunta" name="questaoVestibular.pergunta">${questaoVestibular.pergunta}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da pergunta</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemPergunta" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a alternativa A">Digite a alternativa A </label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a pergunta" name="questaoVestibular.alternativaA">${questaoVestibular.alternativaA}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da alternativa A</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemAlternativaA" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a alternativa B">Digite a alternativa B</label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a alternativa B" name="questaoVestibular.alternativaB">${questaoVestibular.alternativaB}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da alternativa B</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemAlternativaB" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a alternativa C">Digite a alternativa C</label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a alternativa C" name="questaoVestibular.alternativaC">${questaoVestibular.alternativaC}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da alternativa C</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemAlternativaC" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a alternativa D">Digite a alternativa D</label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a alternativa D" name="questaoVestibular.alternativaD">${questaoVestibular.alternativaD}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da alternativa D</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemAlternativaD" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group">
  						<label class="col-md-3 control-label" for="Digite a alternativa E">Digite a alternativa E</label>
 						 	<div class="col-md-8">                     
    							<textarea class="form-control" id="Digite a alternativa E" name="questaoVestibular.alternativaE">${questaoVestibular.alternativaE}</textarea>
 					 		</div>
					</div>
					<div class="form-group">
						  <label class="col-md-4 control-label" for="filebutton">Selecione a imagem da alternativa E</label>
						  <div class="col-md-4">
						    <input id="filebutton" name="questaoVestibular.arquivoImagemAlternativaE" class="input-file" type="file">
						  </div>
					</div>
				
		</div>
	</div>
	
	<div class="form-group">
  			<label class="col-md-5 control-label" for="selectbasic">Selecione qual será a resposta correta desta questão</label>
  			<div class="col-md-2">
    			<select id="selectbasic" name="questaoVestibular.respostaCorreta" class="form-control">
      				<option value=""></option>
    						<c:forEach items="${respostaQuestaoList}" var="respostaQuestao">
    							<c:choose>
									<c:when test="${questaoVestibular.respostaCorreta == respostaQuestao}">
										<c:set var="teste" value="selected"></c:set>
																	 
									</c:when>
								    <c:otherwise>
										<c:set var="teste" value=""></c:set>
																	
								    </c:otherwise>
								</c:choose>
    								<option <c:out value="${teste}"/> value="${respostaQuestao}">${respostaQuestao}</option>
		          			</c:forEach>
      				
    			</select>
  			</div>
	</div>

	


			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">
						Selecione o ano e o semestre no qual esta questão caiu no vestibular
					</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
	  								<label class="col-md-1 control-label" for="selectYear">Ano</label>
	            					<div class="col-md-2 form-group">
	                					<div class='input-group date' id='datetimepicker1' data-date="" data-date-format="yyyy" data-link-field="selectYear" data-link-format="yyyy">
	                    						<input type="text" class="form-control" readonly name="questaoVestibular.anoVestibular" value="${questaoVestibular.anoVestibular}"/>
	                    						<span class="input-group-addon">
	                    							<span class="glyphicon glyphicon-calendar"></span>
	                    						</span>
	                					</div>
	            					</div>
	            									
	      							
									<label class="col-md-1 control-label" for="selectbasic">Semestre</label>
	  								<div class="col-md-2">
	    								<select id="selectbasic" name="questaoVestibular.semestreVestibular" class="form-control">
	      									<option value=""></option>
    											<c:forEach items="${semestreList}" var="semestre">
	    											<c:choose>
															<c:when test="${questaoVestibular.semestreVestibular == semestre}">
																<c:set var="teste" value="selected"></c:set>
																							 
															</c:when>
														    <c:otherwise>
																<c:set var="teste" value=""></c:set>
																							
														    </c:otherwise>
													</c:choose>
    											<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
		          								</c:forEach>
	      										
	    								</select>
	  								</div>
  					</div>
				</div>
				
			</div>
