<div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
          <thead>
                <tr>
                      <th>Disciplina</th>
                      <th>Conteudo Programatico</th>
                      <th>Ano </th>
                      <th>Semestre</th>
                      <th>Ações</th>

                </tr>
          </thead>
          <tbody>
           <c:forEach items="${questaoVestibularList}" var="questaoVestibular">
                <tr class="success">
                  <td>${questaoVestibular.conteudoProgramatico.disciplina.nome}</td>
                  <td>${questaoVestibular.conteudoProgramatico.programaProva}</td>
                  <td>${questaoVestibular.anoVestibular}</td>
                  <td>${questaoVestibular.semestreVestibular}</td>
                  <td class="col-md-2">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/questaoVestibular/${questaoVestibular.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                       <form action="<c:url value="/questaoVestibular/${questaoVestibular.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>
                  </td>
                </tr>
                
                </c:forEach>
               
          </tbody>
      </table>
    </div>
  </div> 