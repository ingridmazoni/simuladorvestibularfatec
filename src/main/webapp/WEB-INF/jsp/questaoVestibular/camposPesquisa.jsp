<div class="row">
		
			<div class="col-md-5">	
					<div class="form-group">
  						    <label class="col-md-3 control-label" for="selectbasic">Disciplina</label>
  						    <div class="col-md-9">
    						      <select id="selectbasic" name="questaoVestibular.conteudoProgramatico.disciplina.id" class="form-control">
      										<option value=""></option>
    										<c:forEach items="${disciplinaList}" var="disciplina">
    											<c:choose>
													<c:when test="${questaoVestibular.conteudoProgramatico.disciplina.id == disciplina.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${disciplina.id}">${disciplina.nome}</option>
		          							</c:forEach>
    								</select>
  						    </div>
					</div>
			</div>


				<div class="col-md-7">
					   <div class="form-group">
  						      <label class="col-md-4 control-label" for="selectbasic">Conteúdo Programático</label>
  						      <div class="col-md-8">
    						        <select id="selectbasic" name="questaoVestibular.conteudoProgramatico.id" class="form-control">
      										<option value=""></option>
    										<c:forEach items="${conteudoProgramaticoList}" var="conteudoProgramatico">
    											<c:choose>
													<c:when test="${questaoVestibular.conteudoProgramatico.id == conteudoProgramatico.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${conteudoProgramatico.id}">${conteudoProgramatico.programaProva}</option>
		          							</c:forEach>
      										
    								</select>
  						      </div>
					   </div>
        </div>
			
		</div>

    <div class="row">
		          <div class='col-md-8'>
                  <label class="col-md-2 control-label" for="selectbasic">Ano</label>
            	     <div class="col-md-3 form-group">
                	     <div class='input-group date' id='datetimepicker1'>
                    	     <input type='text' readonly class="form-control" name="questaoVestibular.anoVestibular" />
                    	     <span class="input-group-addon">
                    		      <span class="glyphicon glyphicon-calendar"></span>
                    	     </span>
                	     </div>
            	     </div>


                   <div class="form-group">
                        <label class="col-md-2 control-label" for="selectbasic">Semestre</label>
                        <div class="col-md-3">
                              <select id="selectbasic" name="questaoVestibular.semestreVestibular" class="form-control">
	      									<option value=""></option>
    											<c:forEach items="${semestreList}" var="semestre">
	    											<c:choose>
															<c:when test="${questaoVestibular.semestreVestibular == semestre}">
																<c:set var="teste" value="selected"></c:set>
																							 
															</c:when>
														    <c:otherwise>
																<c:set var="teste" value=""></c:set>
																							
														    </c:otherwise>
													</c:choose>
    											<option <c:out value="${teste}"/> value="${semestre}">${semestre}</option>
		          								</c:forEach>
	      										
	    						</select>
                        </div>
                  </div>
              </div>
  </div>