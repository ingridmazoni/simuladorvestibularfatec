<div class="row">
		
			<div class="col-md-6">	
					 <div class="form-group">
                <label class="col-md-5 control-label" for="selectbasic">Selecione o eixo tecnológico</label>
                <div class="col-md-7">
                      <select id="city" name="curso.eixoTecnologico.id" class="form-control">
    										<option value=""></option>
    										<c:forEach items="${eixoTecnologicoList}" var="eixoTecnologico">
    											<c:choose>
													<c:when test="${curso.eixoTecnologico.id == eixoTecnologico.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${eixoTecnologico.id}">${eixoTecnologico.nome}</option>
		          							</c:forEach>
          			 </select>
                </div>
            </div>
				</div>


				<div class="col-md-6">
					 <div class="form-group">
                  <label class="col-md-2 control-label" for="textinput">Curso</label>  
                    <div class="col-md-10">
                        <input id="textinput" name="curso.nome" placeholder="Digite o curso a ser pesquisado" class="form-control input-md" type="text">
                    </div>
            </div>
				</div>
			
		
	</div>	

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">
						Selecione as matérias que serão peso 2 para este curso no vestibular 
					</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
  								<div class="col-md-12">
  								<c:forEach items="${disciplinasList}" var="disciplina" varStatus="d">
  									<div class="col-md-2 checkbox">
    									<label for="checkboxes-0">
      										<input name="curso.materiasPesoDois[${d.index}].disciplina.id" id="checkboxes-0" value="${disciplina.id}" type="checkbox">
     											 ${disciplina.nome}
    									</label>
									</div>
  								</c:forEach>	
									
									
									
  								</div>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
	