<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">	
					<div class="form-group">
  							<label class="col-md-2 control-label" for="textinput">Nome</label>  
  							<div class="col-md-10">
  								<input id="textinput" name="curso.nome" placeholder="Digite o nome" class="form-control input-md" type="text" value="${curso.nome}">
  							</div>
					</div>
				</div>


				<div class="col-md-6">
					<div class="form-group">
  							<label class="col-md-3 control-label" for="selectbasic">Eixo Tecnologico</label>
  							<div class="col-md-9">
    								<select id="city" name="curso.eixoTecnologico.id" class="form-control">
    										<option value=""></option>
    										<c:forEach items="${eixoTecnologicoList}" var="eixoTecnologico">
    											<c:choose>
													<c:when test="${curso.eixoTecnologico.id == eixoTecnologico.id}">
														<c:set var="teste" value="selected"></c:set>
																	 
													</c:when>
													<c:otherwise>
														<c:set var="teste" value=""></c:set>
																	
													</c:otherwise>
												</c:choose>
    											<option <c:out value="${teste}"/> value="${eixoTecnologico.id}">${eixoTecnologico.nome}</option>
		          							</c:forEach>
          							</select>
  							</div>
					</div>
				</div>
			
		</div>
	</div>	

	<div class="row">

			<div class="col-md-6">
				<div class="form-group">
  						<label class="col-md-3 control-label" for="textarea">O que o aluno estuda</label>
  						<div class="col-md-9">                     
    						<textarea class="form-control" id="alunoEstuda" name="curso.oQueOAlunoEstuda">${curso.oQueOAlunoEstuda}</textarea>
  						</div>
				</div>
			</div>	

			<div class="col-md-6">
  				<div class="form-group">
  						<label class="col-md-3 control-label" for="textarea">O que o profissional faz</label>
  						<div class="col-md-9">                     
    						<textarea class="form-control" id="profissionalFaz" name="curso.oQueOProfissionalFaz">${curso.oQueOProfissionalFaz}</textarea>
  						</div>

  				</div>
			</div>	
		
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
  						<label class="col-md-3 control-label" for="textarea">Onde trabalhar</label>
  						<div class="col-md-9">                     
    						<textarea class="form-control" id="textarea" name="curso.ondeTrabalhar">${curso.ondeTrabalhar}</textarea>
  						</div>

  			</div>
  		</div>	


	</div>	


	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
    				<div class="panel-heading">
    					<h3 class="panel-title">
    						Selecione as matérias que serão peso 2 para este curso no vestibular 
    					</h3>
    				</div>
				<div class="panel-body">
					   <div class="form-group">
  								<div class="col-md-12">
  								
  			
  								
  												 
  				<c:forEach items="${disciplinasList}" var="disciplina" varStatus="d">
  					<c:set value="${d.count}" var="i" />
  							 
  												
  									 <c:forEach items="${curso.materiasPesoDois}" var="pesoDois">
  									 
  									 									 
  									 	<c:if test="${not empty pesoDois and pesoDois.id eq disciplina.id}">
  									 		<c:set var="teste" value="checked"></c:set>
  									 	
  									 	</c:if>
  									 	
  									 		
  									 	
  									</c:forEach >
  									
  						 	
  									 
  						<div class="col-md-2 checkbox">
			    					<label for="disciplina">
			      						<input type="checkbox" id="disciplina${disciplina.id}" value="${disciplina.id}"  name="curso.materiasPesoDois[${i}].disciplina.id"
			      						 <c:out value="${teste}"/> >${disciplina.nome}
			     					</label>
			    									
						</div>
						
						<c:set var="teste" value=""></c:set>
						
									
  			</c:forEach>	
  									
  								</div>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>
