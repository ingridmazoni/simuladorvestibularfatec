<div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
          <thead>
                <tr>
                      <th>Eixo Tecnologico</th>
                      <th>Curso</th>
                      <th>Matéria Peso 1 </th>
                      <th>Matéria Peso 2</th>
                      <th>Ações</th>

                </tr>
          </thead>
          <tbody>
           <c:forEach items="${cursoList}" var="curso">
                <tr class="success">
                  <td class="col-md-4">${curso.eixoTecnologico.nome}</td>
                  <td class="col-md-3">${curso.nome}</td>
                  <td>${curso.materiasPesoDois[0].nome}</td>
                  <td>${curso.materiasPesoDois[1].nome}</td>
                  <td class="col-md-2">
                      <a class="btn btn-default col-md-6" type="button" href="<c:url value="/curso/${curso.id}"/>" >
                          <em class="glyphicon glyphicon-pencil"></em> Editar
                      </a> 
                       <form action="<c:url value="/curso/${curso.id}"/>" method="post">
	                      <button class="btn btn-default col-md-6" type="submit" name="_method" value="delete">
	                          <em class="glyphicon glyphicon-remove"></em> Excluir
	                      </button> 
	                     
                      </form>
                  </td>
                </tr>
                
                </c:forEach>
               
          </tbody>
      </table>
    </div>
  </div>