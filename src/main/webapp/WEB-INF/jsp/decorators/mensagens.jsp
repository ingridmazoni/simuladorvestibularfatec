<c:if test="${not empty sucesso and empty insucesso}">
	<div class="row">
		<div class="col-md-10 col-md-offset-2">
			<div class="alert alert-success fade in" role="alert" align="center">
				<button class="close" type="button" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<p>${sucesso}</p>
			</div>
		</div>
	</div>
</c:if>

<c:if test="${not empty insucesso}">
	<div class="row">
		<div class="col-md-10 col-md-offset-2">
			<div class="alert alert-danger fade in" role="alert" align="center">
				<button class="close" type="button" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<p>${insucesso}</p>
			</div>
		</div>
	</div>
</c:if>

<c:if test="${not empty errors or not empty exception}">
	<div class="row">
		<div class="col-md-10 col-md-offset-2">
			<div class="alert alert-danger fade in" role="alert" align="center">
				<button class="close" type="button" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<c:forEach var="erro" items="${errors}">
					<p>O campo <strong>${erro.category}</strong> ${erro.message}</p>
				</c:forEach>
				<c:if test="${not empty exception}">
					O seguinte erro ocorreu: ${exception.message}
				</c:if>
			</div>
		</div>
	</div>
</c:if>

<c:if test="${not empty prova}">
	<div class="row">
		<div class="col-md-10 col-md-offset-2">
			<div class="alert alert-success fade in" role="alert" align="center">
				<button class="close" type="button" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<p>${prova}</p><br>
				 <a href="<c:url value="/candidato/notaProva"/>" class="alert-link">Para visualizar a nota da prova clique aqui</a>
			</div>
		</div>
	</div>
</c:if>
