		
				
				<div class="navbar-collapse collapse " id="bs-example-navbar-collapse-1">
				
					<div class="navbar-header">
					 					
					<a class="navbar-brand collapsed" href="<c:url value="/menuPrincipal" />"><img alt="Logotipo Fatec" src="<c:url value="/images/fateczl.jpg" />" width="160" height="80"/></a>
					</div>
				
					<ul class="nav navbar-nav">
						<li>
							<a href="https://docs.google.com/forms/d/11rczjwI94i_sjubipVEUR4uEs5jVwKQM_DSr_hed5Pc/viewform?edit_requested=true">Questionário sobre o sistema</a>
						</li>
						
						<li class="dropdown">
										 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Questionário Vestibular<strong class="caret"></strong></a>
										 <ul class="dropdown-menu">
												<li>
													<a href="<c:url value="/questaoVestibular/novo"/>">Cadastrar</a>
												</li>
												<li>
													<a href="<c:url value="/questaoVestibular/pesquisa"/>">Pesquisar</a>
												</li>
												
										 </ul>
						</li>
					</ul>
					
					
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							 	<em class="glyphicon glyphicon-user"></em> ${usuarioLogado.getUsuario().getLogin()}<strong class="caret"></strong>
							 </a>
							<ul class="dropdown-menu">
								<li>
									<a href="#"><em class="glyphicon glyphicon-cog"></em> Alterar senha</a>
								</li>
								
								<li class="divider">
								</li>
								<li>
									<a href=<c:url value="/logout"/>><em class="glyphicon glyphicon-off"></em>  Sair</a>
								</li>
							</ul>
						</li>
					</ul>
					
				</div>
				
