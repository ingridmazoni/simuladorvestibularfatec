					
  						
				
					<div class="navbar-collapse collapse" id="navbar">
					
						<div class="navbar-header">
      							<a class="navbar-brand collapsed" href=<c:url value="/menuPrincipal"/>>
        							<img alt="Logotipo Fatec" src=<c:url value="/images/fateczl.jpg" /> width="160" height="80">
     					 		</a>
    						</div>
					
					
						<ul class="nav navbar-nav">
							
									<li class="dropdown">
										 	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fatec<strong class="caret"></strong></a>
											<ul class="dropdown-menu">
												<li>
													<a href=<c:url value="/fatec/novo"/>>Cadastrar</a>
												</li>
												<li>
													<a href=<c:url value="/fatec/pesquisa"/>>Pesquisar</a>
												</li>
												
											</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Curso<strong class="caret"></strong></a>
										<ul class="dropdown-menu">
											<li>
												<a href="<c:url value="/curso/novo"/>">Cadastrar</a>
											</li>
											<li>
												<a href="<c:url value="/curso/pesquisa"/>">Pesquisar</a>
											</li>
											
										</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Turma<strong class="caret"></strong></a>
										<ul class="dropdown-menu">
											<li>
												<a href="<c:url value="/turma/novo"/>">Cadastrar</a>
											</li>
											<li>
												<a href="<c:url value="/turma/pesquisa"/>">Pesquisar</a>
											</li>
											
										</ul>
									</li>
									<li class="dropdown">
										 	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nota de Corte<strong class="caret"></strong></a>
											<ul class="dropdown-menu">
												<li>
													<a href="<c:url value="/notaCorte/novo"/>">Cadastrar</a>
												</li>
												<li>
													<a href="<c:url value="/notaCorte/pesquisa"/>">Pesquisar</a>
												</li>
												
											</ul>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Conteúdo Programático<strong class="caret"></strong></a>
										<ul class="dropdown-menu">
											<li>
												<a href="<c:url value="/conteudoProgramatico/novo"/>">Cadastrar</a>
											</li>
											<li>
												<a href="<c:url value="/conteudoProgramatico/pesquisa"/>">Pesquisar</a>
											</li>
											
										</ul>
									</li>
									<li class="dropdown">
										 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Questionário<strong class="caret"></strong></a>
										 <ul class="dropdown-menu">
												<li>
													<a href="<c:url value="/questaoVestibular/novo"/>">Cadastrar</a>
												</li>
												<li>
													<a href="<c:url value="/questaoVestibular/pesquisa"/>">Pesquisar</a>
												</li>
												
										 </ul>
									</li>
									<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Desempenho do Candidato<strong class="caret"></strong></a>
											<ul class="dropdown-menu">
												<li>
													<a href="<c:url value="/desempenhoCandidato/novo"/>">Consultar Desempenho Geral</a>
												</li>
												<li>
													<a href="#">Consultar Desempenho Individual</a>
												</li>
												
											</ul>
									</li>
						</ul>
				
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								 	<em class="glyphicon glyphicon-user"></em> ${usuarioLogado.getUsuario().getLogin()}<strong class="caret"></strong>
								 </a>
								<ul class="dropdown-menu">
									<li>
										<a href="#"><em class="glyphicon glyphicon-cog"></em> Alterar senha</a>
									</li>
									
									<li class="divider"></li>
									<li>
										<a href="<c:url value="/logout"/>"><em class="glyphicon glyphicon-off"></em>  Sair</a>
									</li>
								</ul>
							</li>
							
						</ul>
				</div>
