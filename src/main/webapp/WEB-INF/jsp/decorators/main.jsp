<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
     
<!DOCTYPE html> 
<html lang="pt_BR">
<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 
 <link href=<c:url value="/bootstrap/css/style.css"/> rel="stylesheet" type="text/css"/>
 <link href=<c:url value="/bootstrap/css/bootstrap.min.css"/> rel="stylesheet" type="text/css"/>
 <link href=<c:url value="/bootstrap/css/bootstrap-theme.min.css" /> rel="stylesheet" type="text/css"/>
 <link href=<c:url value="/bootstrap/css/https _getbootstrap.com_assets_css_ie10-viewport-bug-workaround.css" /> rel="stylesheet" type="text/css"/>
 <link href=<c:url value="/bootstrap/css/https _getbootstrap.com_examples_navbar-fixed-top_navbar-fixed-top.css" /> rel="stylesheet" type="text/css"/>
 <script src=<c:url value="/bootstrap/js/jquery-3.1.0.js"/>  type="text/javascript"></script>	
 <script src=<c:url value="/bootstrap/js/bootstrap.min.js"/> type="text/javascript"></script>	
 <script src=<c:url value="/bootstrap/js/jquery.maskedinput.js"/> type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/jquery.validate.js"/>  type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/additional-methods.js"/> type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/bootstrap-datetimepicker.js"/> type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/bootstrap-datetimepicker.min.js"/> type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/scripts.js"/> type="text/javascript"></script>
 <script src=<c:url value="/bootstrap/js/https _getbootstrap.com_assets_js_ie10-viewport-bug-workaround.js"/> type="text/javascript"></script>
 
   
	<title>
		<decorator:title default="Sistema Vestibular Fatec" /> 
	</title>
	<decorator:head/> 
</head>
<body role="document">
		  		
		  <%-- 	<c:set var="usuarioLogado" scope="session" value="${userLogado}"/>	 --%>
			  <%@include file="menuPrincipal.jsp" %>
  				 		  
    		<section>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<br>
						<br>
						<br>
						  <%@include file="mensagens.jsp" %>
						
						  <decorator:body/>
					
					</div>
				</div>
														
			</div>
			</section>
	
   

</body>
</html>
