<c:if test="${ not empty usuarioLogado.usuario}">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-sm-6">
				<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
				
						<c:set var="perfil" scope="session" value="${usuarioLogado.getUsuario().getPerfil()}"/>
		
															 
							<c:if test="${fn:contains(perfil, 'CANDIDATO')}">
								<%@include file="menuCandidato.jsp" %>					
								
							</c:if>
								 
							<c:if test="${fn:contains(perfil, 'ADMINISTRADOR')}">
								<%@include file="menuAdministrador.jsp" %>					
								
							</c:if>
							<c:if test="${fn:contains(perfil, 'TESTE')}">
								<%@include file="menuFuncionario.jsp" %>					
								
							</c:if>
				</nav>
			</div>
		</div>
	</div>
</c:if>