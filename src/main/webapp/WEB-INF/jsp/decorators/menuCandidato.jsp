				
				
				<div class="navbar-collapse collapse " id="bs-example-navbar-collapse-1">
				
					<div class="navbar-header">
					 					
					<a class="navbar-brand collapsed" href="<c:url value="/menuPrincipal" />"><img alt="Logotipo Fatec" src="<c:url value="/images/fateczl.jpg" />" width="160" height="80"/></a>
					</div>
				
					<ul class="nav navbar-nav">
						
						<li>
							<a href=<c:url value="/candidato/provas"/>>Prova</a>
						</li>
						<li>
							<a href=<c:url value="/candidato/notaProva"/>>Nota</a>
							
						</li>
						<li>
							<a href="https://docs.google.com/forms/d/1BkpC_M-Bts95xQMqUOrLyjORHMvRdaVb2byLU8_ToQE/viewform?edit_requested=true">Questionário sobre o sistema</a>
						</li>
					</ul>
					
					
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							 	<em class="glyphicon glyphicon-user"></em> ${usuarioLogado.getUsuario().getLogin()}<strong class="caret"></strong>
							 </a>
							<ul class="dropdown-menu">
								<li>
									<a href=<c:url value="/candidatoCadastro/${usuarioLogado.getUsuario().getLogin()}" />><em class="glyphicon glyphicon-cog"></em> Alterar senha</a>
								</li>
								
								<li class="divider">
								</li>
								<li>
									<a href=<c:url value="/logout"/>><em class="glyphicon glyphicon-off"></em>  Sair</a>
								</li>
							</ul>
						</li>
					</ul>
					
				</div>
				
