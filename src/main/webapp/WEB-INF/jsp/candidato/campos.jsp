<input id="idCandidaato" type="hidden" name="candidato.id" value="${candidato.id}">
<div class="row">
		<div class="col-md-8">
			<div class="row">
					<div class="col-md-12">
					
						<div class="form-group">
			  				<label class="col-md-2 control-label" for="textinput">Nome</label>  
			  				<div class="col-md-10">
			  				<input id="textinput" name="candidato.nome" placeholder="Digite o nome do candidato" class="form-control input-md" type="text" value="${candidato.nome}">
			  				</div>
						</div>
					</div>
			</div>	

				<div class="row">
						<div class="col-md-12">
									<div class="form-group">
											<div class="col-md-6">
													<label class="col-md-6 control-label" for="selectbasic">Data Nascimento</label>
								                	<div class="col-md-6 input-group date">
								                		<input type="text" id="data1" name="candidato.dataNascimento" class="form-control" value="<fmt:formatDate pattern="dd/MM/yyyy"  value="${candidato.dataNascimento}" />"/>
								                    	<!-- <span class="input-group-addon">
								                        	<span class="glyphicon glyphicon-calendar"></span>
								                    	</span> -->
								                	</div>
								            </div>	
								            <div class="col-md-6">    	
								           
								  						<label class="col-md-2 control-label" for="selectbasic">Sexo</label>
								  						<div class="col-md-7">
										    					<select id="selectbasic" name="candidato.sexo" class="form-control">
										      							<option value=""></option>
									        							<c:forEach items="${sexoList}" var="sexo">
									        								<c:choose>
																				<c:when test="${candidato.sexo == sexo}">
																					<c:set var="teste" value="selected"></c:set>
																								 
																				</c:when>
																				<c:otherwise>
																					<c:set var="teste" value=""></c:set>
																								
																				</c:otherwise>
																			</c:choose>
									        								<option <c:out value="${teste}"/> value="${sexo}">${sexo}</option>
									        							</c:forEach>
										    					</select>
								  						</div>
								  			</div>
							  		</div>
						</div>	  			
				</div>

				<div class="row">
					<div class="col-md-12">  
						<div class="form-group">
							  <label class="col-md-3 control-label" for="textinput">E-mail principal</label>  
							  <div class="col-md-9">
								  <input id="email" name="candidato.emailPrincipal" placeholder="Digite o e-mail principal" class="form-control input-md" type="email" value="${candidato.emailPrincipal}">
								  
							  </div>
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-12">  
						<div class="form-group">
							  <label class="col-md-3 control-label" for="textinput">E-mail secundário</label>  
							  <div class="col-md-9">
								  <input id="email" name="candidato.emailSecundario" placeholder="Digite o e-mail secundário" class="form-control input-md" type="email" value="${candidato.emailSecundario}">
								  
							  </div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">  
						<div class="form-group">
							  <label class="col-md-3 control-label" for="textinput">Telefone</label>  
							  <div class="col-md-9">
								  <input id="telefone1" name="candidato.telefone" placeholder="Digite o telefone" class="form-control input-md" type="text" value="${candidato.telefone}">
								  
							  </div>
						</div>
					</div>
					<div class="col-md-6">  
						<div class="form-group">
							  <label class="col-md-3 control-label" for="textinput">Celular</label>  
							  <div class="col-md-9">
								  <input id="celular" name="candidato.celular" placeholder="Digite o celular" class="form-control input-md" type="text"  value="${candidato.celular}">
								  
							  </div>
						</div>
					</div>
				</div>
						

				

		</div>

			<div class="col-md-4">
				<div align="center" class="col-md-10 col-sm-offset-2">
					<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">
									Sistema de Pontuação Acrescida
								</h3>
							</div>
							<div class="panel-body">

									<div class="form-group">
  											<div class="col-md-12">
												<div class="checkbox">
													    <label for="checkboxes-0">
													    	<c:choose>
																<c:when test="${candidato.pontuacaoAcrescidaAfrodescendente eq true}">
																	<c:set var="teste" value="checked"></c:set>
																	 
																</c:when>
																<c:otherwise>
																	<c:set var="teste" value=""></c:set>
																	
																</c:otherwise>
															</c:choose>
															<input name="candidato.pontuacaoAcrescidaAfrodescendente" id="pontuacaoAcrescida" type="checkbox" <c:out value="${teste}"/> >Afrodescendência
													    			     
													   	</label>
												</div>
												<br>
												<div class="checkbox">
															<c:choose>
																<c:when test="${candidato.pontuacaoAcrescidaEscolaridadePublica eq true}">
																	<c:set var="teste2" value="checked"></c:set>
																	 
																</c:when>
																<c:otherwise>
																	<c:set var="teste2" value=""></c:set>
																	
																</c:otherwise>
															</c:choose>
												
												    <label for="checkboxes-1">
												      	<input name="candidato.pontuacaoAcrescidaEscolaridadePublica" id="checkboxes-1" type="checkbox" <c:out value="${teste2}"/> > Escolaridade Pública
												      
												    </label>
												</div>
											</div>
									</div>





								
							</div>
							
					</div>
				</div>
			</div>	

	</div>

	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 align="center" class="panel-title">
						Enem
					</h3>
				</div>
				<div class="panel-body">

						<div class="col-md-6">
                
                			<label class="col-md-4 control-label" for="selectbasic">Ano</label>
		                	<div class="col-md-6 input-group date" id="datetimepicker1">
		                    	<input type="text" readonly name="candidato.anoInscricaoEnem" class="form-control" value="${candidato.anoInscricaoEnem}" />
		                    	<span class="input-group-addon">
		                        	<span class="glyphicon glyphicon-calendar"></span>
		                    	</span>
		                	</div>
            			</div> 


            			<div class="col-md-6 form-group">
						  <label class="col-md-5 control-label" for="textinput">Nota do Enem</label>  
						  <div class="col-md-7">
						  		<input id="notaEnem" name="candidato.notaEnem" placeholder="nota do enem" class="form-control input-md" type="text" value="<fmt:formatNumber type="number" value="${candidato.notaEnem}" />">
						  </div>
						</div> 
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
				<div align="center" class="col-md-12">
					<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">
									Login e Senha
								</h3>
							</div>
							<div class="panel-body">

									<div class="row">
											
												  <label class="col-md-2 control-label" for="textinput">Login</label>  
												  <div class="col-md-10">
												  		<input id="textinput" name="candidato.usuario.login" placeholder="login" class="form-control input-md" type="text" value="${candidato.usuario.login}">
												  		<input id="id" type="hidden" name="candidato.usuario.perfil" value="${perfil}">
												  </div>
									</div>
									<br>				  
									<div class="row">				
												  <label class="col-md-2 control-label" for="passwordinput">Senha</label>
												  <div class="col-md-10">
												    	<input id="passwordinput" name="candidato.usuario.senha" placeholder="senha" class="form-control input-md" type="password" value="${candidato.usuario.senha}">
												  </div>
											
									</div>
							
							</div>
						</div>
			</div>	

		</div>	
</div>	

  <div class="row"> 
		<div class="col-md-8">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 align="center" class="panel-title">
						Selecione a primeira opção de curso que você deseja prestar
					</h3>
				</div>
				<div class="panel-body">
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
									  <div class="col-md-9">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${0}].turma.fatec.id" class="form-control">
											    		<option value=""></option>
									        			<c:forEach items="${fatecList}" var="fatec">
										        				<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[0].turma.fatec.id == fatec.id}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        				<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
							</div>

						</div>
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Curso</label>
									  <div class="col-md-6">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${0}].turma.curso.id" class="form-control">
											      		<option value=""></option>
									        			<c:forEach items="${cursoList}" var="curso">
									        					<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[0].turma.curso.id == curso.id}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        			
									        				<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
							</div>

						</div>
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Período</label>
									  <div class="col-md-3">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${0}].turma.periodo" class="form-control">
											      		<option value=""></option>
									        			<c:forEach items="${periodoList}" var="periodo">
									        				<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[0].turma.periodo == periodo}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        				<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
									  
							</div>

						</div>


						
							
					
				</div>
				
			</div>
		</div>
	</div>


 <div class="row"> 
		<div class="col-md-8">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 align="center" class="panel-title">
						Selecione a segunda opção de curso que você deseja prestar
					</h3>
				</div>
				<div class="panel-body">
							
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Fatec</label>
									  <div class="col-md-9">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${1}].turma.fatec.id" class="form-control">
											    		<option value=""></option>
									        			<c:forEach items="${fatecList}" var="fatec">
										        				<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[1].turma.fatec.id == fatec.id}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        				<option <c:out value="${teste}"/> value="${fatec.id}">${fatec.nome}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
							</div>

						</div>
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Curso</label>
									  <div class="col-md-6">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${1}].turma.curso.id" class="form-control">
											      		<option value=""></option>
									        			<c:forEach items="${cursoList}" var="curso">
									        					<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[1].turma.curso.id == curso.id}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        			
									        				<option <c:out value="${teste}"/> value="${curso.id}">${curso.nome}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
							</div>

						</div>
						<div class="row">
							<div class="form-group">
							  		<label class="col-md-2 control-label" for="selectbasic">Período</label>
									  <div class="col-md-3">
											    <select id="selectbasic" name="escolhaDeCursoDoCandidato[${1}].turma.periodo" class="form-control">
											      		<option value=""></option>
									        			<c:forEach items="${periodoList}" var="periodo">
									        				<c:choose>
																		<c:when test="${candidato.escolhaDeCurso[1].turma.periodo == periodo}">
																			<c:set var="teste" value="selected"></c:set>
																						 
																		</c:when>
																		<c:otherwise>
																			<c:set var="teste" value=""></c:set>
																						
																		</c:otherwise>
																</c:choose>
									        			
									        				<option <c:out value="${teste}"/> value="${periodo}">${periodo}</option>
									          	  		</c:forEach>
											    </select>
									  </div>
									  
							</div>

						</div>


						
							
					
				</div>
				
			</div>
		</div>
	</div>

	
 