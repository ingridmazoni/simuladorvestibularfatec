jQuery(function($){
	   	$('#telefone1').mask('(99) 9999-9999');
    	$('#telefone2').mask('(99) 9999-9999');
    	$('#celular').mask('(99) 99999-9999');
    	$('#cep').mask('99999-999');
    	$('#data1').mask('99/99/9999');
    	
    	
    	
  
});  



$(function () {
    $('#datetimepicker1').datetimepicker({
      startView: 'decade',
      viewMode: 'years',
      todayBtn:  1,
      autoclose: 1,
      todayHighlight: 1,
      minView: 'decade',
      maxView: 'decade',
      forceParse: 0,
      format: 'yyyy'   

            
    });
});



$(function () {
    $('#datetimepicker2').datetimepicker({
    	 startView: 'decade',
         viewMode: 'years',
         todayBtn:  1,
         autoclose: 1,
         todayHighlight: 1,
         minView: 'decade',
         maxView: 'decade',
         forceParse: 0,
         format: 'dd/MM/yyyy'  
               
    });
});



jQuery(function(){
                $('#meu_form').validate({
                   rules: {
                        
                       email: {
                            required: true,
                            email: true
                        },
                        site: {
                            required: true,
                            url: true
                        },
                    },
                    messages: {
                        
                        email: {
                            required: "Campo obrigatório",
                            email: "O e-mail digitado é inválido"
                       },

                       site: {
                            required: "Campo obrigatório",
                            url: "O site digitado é inválido."
                       },
                   }
               });
           }); 
  
  